# README #

All these files are used for the machine learning routine, trying to improve the reduction ratio on some test datas. The routine is explained in the pdf file (first part of the file).
The new routines use a ksvd module too. It is available at https://github.com/hoytak/pyksvd
For those using windows, you can install the module from http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyksvd

Furthermore, to run it correctly, you will need to change the 'omp.py' file in sklearn/linear_model library and the 'dict_learning' file in sklearn/decomposition library with the new ones in this repository.


If you do not have the used datas, just ask to Etienne.