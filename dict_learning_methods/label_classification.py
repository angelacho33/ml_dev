# -*- coding: utf-8 -*-
"""
Created on Mon Aug 15 15:48:05 2016

@author: Etienne
"""
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import ARDRegression

def complete_signal(y, framesize=None):
    if framesize is not None:
        resolution = len(y)
        if (resolution%framesize>0):
           for j in range (framesize-(resolution%framesize)):
                    y=np.concatenate((y, [y[resolution-1]]))
    return y           

def training_signal_logistic(y_t, y_c, score=False, solver='liblinear', n_iter=100, tol=1e-5, class_weight=None, penalty='l2', C=1.):
    reg=LogisticRegression(penalty=penalty, solver=solver, max_iter=n_iter, multi_class='ovr', 
                           tol=tol, class_weight=class_weight, C=C)
    l_t=np.ones(len(y_t))
    l_c=-np.ones(len(y_c))
    X=np.concatenate((y_t, y_c), axis=0)
    y=np.concatenate((l_t, l_c))
    reg.fit(X,y)
#    w=np.concatenate((reg.intercept_, reg.coef_), axis=0)
    w=reg.coef_
    w_0=reg.intercept_
    pred=reg.predict(X)
    if score:
        sc=reg.score(X, y)
        print 'Accuracy score : ' + str(sc)
    else:
        sc='No score'
    return w_0, w, pred, sc

def testing_signal_logistic(y, intercept=None, coef=None):
    reg=LogisticRegression(solver='sag', max_iter=100, multi_class='ovr')
    reg.coef_=coef
    reg.intercept_=intercept
    pred=reg.predict(y)
    return pred
    
def training_signal_ard(y_t, y_c, n_iter=300, tol=0.001, score=False):
    reg=ARDRegression(n_iter=n_iter, tol=tol)
    l_t=np.ones(len(y_t))
    l_c=-np.ones(len(y_c))
    X=np.concatenate((y_t, y_c), axis=0)
    y=np.concatenate((l_t, l_c))
    reg.fit(X,y)
#    w=np.concatenate((reg.intercept_, reg.coef_), axis=0)
    coef=reg.coef_
    alpha=reg.alpha_
    lam=reg.lambda_
    sigma=reg.sigma_
    pred=reg.predict(X)
    if score:
        sc=reg.score(X, y)
        print 'Accuracy score : ' + str(sc)
    else:
        sc='No score'
    return coef, alpha, lam, sigma, pred, sc

def testing_signal_ard(y, coef=None, alpha=None, lam=None, sigma=None):
    reg=ARDRegression()
    reg.coef_=coef
    reg.alpha_=alpha
    reg.lambda_=lam
    reg.sigma_=sigma
    pred=reg.predict(y)
    return pred
    
def correlation(x_1, numberofmax=1):
    if numberofmax>x_1.shape[1]:
        print 'Numberofmax has to be less than the number of components'
        return 0
    x_1=-np.sort(-np.abs(x_1), axis=1)
    cumsum=np.cumsum(x_1, axis=1)
    x_max=cumsum[:, numberofmax-1]
    x_sum=cumsum[:, -1]
    x_sum[x_sum==0]=1e-10
    x_max[x_max==0]=1e-10
    corr=x_max/x_sum
    return corr