# -*- coding: utf-8 -*-
"""
Created on Fri Aug 19 10:14:16 2016

@author: Etienne
Implements a svd initialization method for D_0
"""

import numpy as np

def svd_init(y_1, frame_size=None, n_components=None, random=True):
    if frame_size is None:
        frame_size=y_1.shape[1]
    if n_components is None:
        n_components=frame_size
    D=np.zeros((n_components, frame_size))
    u, s, v= np.linalg.svd(y_1)
    D[:np.min((frame_size, n_components))]=v[:np.min((frame_size, n_components))]
    if random:
       D+=0.01*np.random.randn(n_components, frame_size)
    for i in range(n_components):
        norm=np.sqrt(np.sum(D[i]**2))
        if norm<1e-8:
            D[i]=np.random.randn((n_components, frame_size))
            D[i]/=np.sqrt(np.sum(D[i]**2))
        else:
            D[i]/=norm
    #calculating c
    diag=np.diag(s)
    if (u.shape[1]<frame_size):
        s=diag
        s=np.concatenate((diag, np.zeros((u.shape[1], frame_size-u.shape[1]))), axis=1)
    else:
        s=np.concatenate((diag, np.zeros((u.shape[1]-frame_size, frame_size))), axis=0)
    c=np.dot(u, s)
    return D, c