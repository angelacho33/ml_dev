import sys, os

try:
    tkdir = '/home/boursier/repositories/tk'
except Exception:
    print "ERROR: You need to set the environment variable TKDIR (path of your tk directory)"    
os.environ['LD_LIBRARY_PATH'] = tkdir+'/tklib/'
sys.path.append(tkdir+'/tkmw/packages/')
sys.path.append(tkdir+'/tksvr/packages/')

from tools import tk_databank
from tools import tk_err
from tools import tk_plot
import numpy as np
#from test_subs import *


import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib.dates as md
import matplotlib.style
import datetime
matplotlib.style.use('ggplot')



#export LD_LIBRARY_PATH=/Users/markus/Teraki//7_Programing/7.1_Development/tk/tklib/


def single_signal_rmse(O):
    """Calculate the RMSE of measurment values alone (not of difference between 2 signals)
    """
    rmse = np.sqrt( np.mean( np.array(O)**2 ) )
    return(rmse)


def read_alstom_file(filename):
    import csv
    data = []
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=';')
        for row in reader:
            [x, y, z] = row
            data.append([float(x), float(y), float(z)])
    return(data)

def scale_time_domain(in_):
    in_ = np.array(in_)
    out = (in_ * 16) / 32767.
    return(out)

def filter_time_domain(in_):
#function [ou] = first_filter(in)
#    in=(in*16)/32767;
#    in=in-median(in);
#    in=abs(in);
#    limit = 3*std(in);
#    s=size(in)
#    for i=1:s(1)
#        if abs(in(i))>limit
#            in(i)=0;
#        end;
#    end;
#    ou=in;
#end
    in_ = scale_time_domain(in_)
    in_ = in_ - np.median(in_)
    in_ = np.abs(in_)
    limit = 3 * np.std(in_, ddof=1)
    s = len(in_)   # size
    for i in range(s):
        if np.abs(in_[i]) > limit:
            in_[i] = 0
    out = in_
    return(out)

def filter_time_domain_without_scaling(in_):
    in_ = in_ - np.median(in_)
    in_ = np.abs(in_)
    limit = 3 * np.std(in_, ddof=1)
    s = len(in_)   # size
    for i in range(s):
        if np.abs(in_[i]) > limit:
            in_[i] = 0
    out = in_
    return(out)


def filter_frequency_domain(in_):
#function [ou] = complet_filter(in)
#    in=(in*16)/32767;
#    in=in-median(in); %median
#    in=abs(in);  %%%%%%% outloder start%%%%%%%
#    limit = 3*std(in); %%%% if the signal is higher of 3*std
#    s=size(in);
#    for i=1:s(1)  %%%%%%% outloder start%%%%%%%
#        if abs(in(i))>limit
#            in(i)=0;
#        end;
#    end;
#    ouu=in; %%%%%%% outloder end%%%%%%%
#    in=in*9.8;  %%%%%%% velocity integrate start%%%%%%%
#    integ=[in(1)/(2*(1/3200))];
#    for i=2:s(1)
#        so(i)=(in(i)+in(i-1))/2*(1/3200);
#    end;
#    so=so-mean(so);
#    so=so*1000;  %%%%%%% velocity integrate end%%%%%%%
#    dftData=fft(so) %%%%%%% FFT start%%%%%%%
#    lenData =size(dftData)
#    piX= ceil(lenData(2)/2)+1;
#    dftData=(2/lenData(2))*dftData(1:piX);
#    ou=dftData;  %%%%%%% FFT end%%%%%%%    
#end

#    in_ = np.array(in_)
#    in_ = (in_ * 16) / 32767.
#    in_ = in_ - np.median(in_)
#    in_ = np.abs(in_)              # outloder start
#    limit = 3 * np.std(in_, ddof=1)            # if the signal is higher of 3*std
#    s = len(in_)
#    for i in range(s):              # outloder start
#        if np.abs(in_[i]) > limit:
#            in_[i] = 0
   
    in_ = filter_time_domain(in_)

    in_ = in_ * 9.8                 # velocity integrate start
    so = []
    s = len(in_)
    for i in range(1, s, 1):        #????????????
        so.append( (in_[i] + in_[i-1]) / 2 * (1/3200.) )    #????????????
    so = np.array(so) - np.mean(so)
    so = so * 1000.                 # velocity integrate end
    dftData = np.fft.fft(so)        # FFT start
    
    lenData = dftData.shape
    piX = np.ceil(lenData[0] / 2) + 1
    dftData = (2 / lenData[0]) * dftData[1:piX]
    ou = dftData                    # FFT end
    return(ou)
              

def process_and_save():
    # Step 1: create a Databank and add data from 3 files to it.
    D = tk_databank.DataBank()
    raw_files = [
        #'TEST1/2016-04-15 15_18_24_raw_data_1_test_dataset.csv',
        #'TEST2/2016-04-18 13_50_18_raw_data_1_test_dataset.csv',
        'TEST3/2016-04-21 12_56_53_raw_data_4_test_dataset.csv'

    ]
    rec_files = [
        #'TEST1/2016-04-15 15_18_24_rec_data_1_test_dataset.csv',                 
        #'TEST2/2016-04-18 13_50_19_rec_data_1_test_dataset.csv',
        'TEST3/2016-04-21 12_56_53_rec_data_4_test_dataset.csv'
    ]
    sensor_names = ['Alstom Test1', 'Alstom Test2', 'Alstom Test3']

    for i in range(len(raw_files)):
        values = read_alstom_file(raw_files[i])

        # Apply transofrmation given by David
        values = convert(values, type='scale_time_domain')

        ts = [x * (1/3200.) for x in range(len(values))]
        S1 = D.add_sensor_data(values, ts, sensor_id=i, sensor_type='Accelerometer', sensor_name=sensor_names[i],
            value_names=['x', 'y', 'z'], quantity=['Acceleration (g)', 'Acceleration (g)', 'Acceleration (g)'])

    # Step 2: process raw data of each sensor according to process parameters
    process_cases = [[400, 0.6], [400, 0.7], [400, 0.8], [400, 0.9], [640, 0.6], [640, 0.7], [640, 0.8], [640, 0.9], [800, 0.6], [800, 0.7], [800, 0.8], [800, 0.9]]
    for sensor_id in range(len(raw_files)):
        for framesize, reduction in process_cases:
            D.sensor_data_process(sensor_id, framesize, reduction, 'cii_dct_v011', block=15)     # 'cii_dct_v011', 'aii_v010'

    D.save('Alstom_Test2_20160512.dbk')
#    D.save('Alstom_Test2_converted.dbk')

#    P = tk_plot.Plot(D)
#    for i in range(len(raw_files)):
#        P.plot_raw_inspection(sensor_id=i, process_id=0, output='screen')
#        P.plot_rec_inspection(sensor_id=i, process_id=0, output='png')
#        P.plot_rec_inspection(sensor_id=i, process_id=0, output='screen')


def subsample_process_and_save():
    # Step 1: create a Databank and add data from 3 files to it.
    D = tk_databank.DataBank()
    Dfull = tk_databank.DataBank()
    
    raw_files = [
        #'TEST1/2016-04-15 15_18_24_raw_data_1_test_dataset.csv',
        #'TEST2/2016-04-18 13_50_18_raw_data_1_test_dataset.csv',
        'TEST3/2016-04-21 12_56_53_raw_data_4_test_dataset.csv'

    ]
    rec_files = [
        #'TEST1/2016-04-15 15_18_24_rec_data_1_test_dataset.csv',                 
        #'TEST2/2016-04-18 13_50_19_rec_data_1_test_dataset.csv',
        'TEST3/2016-04-21 12_56_53_rec_data_4_test_dataset.csv'
    ]
    sensor_names = ['Alstom Test1', 'Alstom Test2', 'Alstom Test3']

    for i in range(len(raw_files)):
        values = read_alstom_file(raw_files[i])

        # Apply transofrmation given by David
#        values = convert(values)

        ts = [x * (1/3200.) for x in range(len(values))]
        ts = [x * 1e9 for x in ts]
        
        subs_values, subs_tss = sub_sampling(values, ts)
        
        Dfull.add_sensor_data(values, ts, sensor_id=i, sensor_type='Accelerometer', sensor_name=sensor_names[i],
            value_names=['x', 'y', 'z'], quantity=['Acceleration (g)', 'Acceleration (g)', 'Acceleration (g)'])
        
        S1 = D.add_sensor_data(subs_values, subs_tss, sensor_id=i, sensor_type='Accelerometer', sensor_name=sensor_names[i],
            value_names=['x', 'y', 'z'], quantity=['Acceleration (g)', 'Acceleration (g)', 'Acceleration (g)'])

    # Step 2: process raw data of each sensor according to process parameters
    process_cases = [[800, 0.6]] #[[400, 0.6], [400, 0.7], [400, 0.8], [400, 0.9], [640, 0.6], [640, 0.7], [640, 0.8], [640, 0.9], [800, 0.6], [800, 0.7], [800, 0.8], [800, 0.9]]
    process_id = 0
    for sensor_id in range(len(raw_files)):
        for framesize, reduction in process_cases:
            D.sensor_data_process(sensor_id, framesize, reduction, 'cii_dct_v011', block=10)     # 'cii_dct_v011', 'aii_v010'



            recon_data = D.get_sensor_recon_data(sensor_id, process_id)
            ups_values, ups_tss = up_sampling(recon_data['values'], recon_data['tss'])

#            D.add_processed_sensor_data(0, ups_values, ups_tss)
            Dfull.add_processed_sensor_data(0, ups_values, ups_tss, meta={'framesize': framesize, 'reduction': reduction})

            process_id = process_id + 1

    D.save('Alstom_Test2_20160509_test_step.dbk')
#    D.save('Alstom_Test2_converted.pkl')

    P = tk_plot.Plot(Dfull)
#    for i in range(len(raw_files)):
#        #P.plot_raw_inspection(sensor_id=i, output='screen')
#        P.plot_rec_inspection(sensor_id=i, process_id=0, output='png')
    P.plot_rec_inspection(sensor_id=0, process_id=0, output='png')

def step_process_and_save():
    # Step 1: create a Databank and add data from 3 files to it.
    D = tk_databank.DataBank()
    Dfull = tk_databank.DataBank()
    
    raw_files = [
        'TEST3/2016-04-21 12_56_53_raw_data_4_test_dataset.csv'
    ]
    rec_files = [
        'TEST3/2016-04-21 12_56_53_rec_data_4_test_dataset.csv'
    ]
    sensor_names = ['Alstom Test3']

    for i in range(len(raw_files)):
        values = read_alstom_file(raw_files[i])

        # Apply transofrmation given by David
#        values = convert(values)

        ts = [x * (1/3200.) for x in range(len(values))]
        ts = [x * 1e9 for x in ts]
        
#        values_hlp = [ list( np.round(np.array(x)/10.)*10 ) for x in zip(*values) ]
#        values_hlp = [ list( np.round(np.array(x)) ) for x in zip(*values) ]
#        values_out = [list(x) for x in zip(values_hlp[0], values_hlp[1], values_hlp[2])]

        D.add_sensor_data(values, ts, sensor_id=i, sensor_type='Accelerometer', sensor_name=sensor_names[i],
            value_names=['x', 'y', 'z'], quantity=['Acceleration (g)', 'Acceleration (g)', 'Acceleration (g)'])
        
    # Step 2: process raw data of each sensor according to process parameters
    process_cases = [[800, 0.8]] #[[400, 0.6], [400, 0.7], [400, 0.8], [400, 0.9], [640, 0.6], [640, 0.7], [640, 0.8], [640, 0.9], [800, 0.6], [800, 0.7], [800, 0.8], [800, 0.9]]
    process_id = 0
    for sensor_id in range(len(raw_files)):
        for framesize, reduction in process_cases:
            D.sensor_data_process(sensor_id, framesize, reduction, 'cii_dct_v012')#, block=10) # 'cii_dct_v011', 'aii_v010'

            recon_data = D.get_sensor_recon_data(sensor_id, process_id)
            ups_values, ups_tss = up_sampling(recon_data['values'], recon_data['tss'])

#            D.add_processed_sensor_data(0, ups_values, ups_tss)
            D.add_processed_sensor_data(0, ups_values, ups_tss, meta={'framesize': framesize, 'reduction': reduction})

            process_id = process_id + 1

    D.save('Alstom_Test2_20160509_test_step.dbk')
#    D.save('Alstom_Test2_converted.pkl')

    P = tk_plot.Plot(D)
#    for i in range(len(raw_files)):
#        #P.plot_raw_inspection(sensor_id=i, output='screen')
#        P.plot_rec_inspection(sensor_id=i, process_id=0, output='png')
    P.plot_rec_inspection(sensor_id=0, process_id=0, output='png')
    P.plot_rec_inspection(sensor_id=0, process_id=0, output='screen')



def process_and_save_matlab_file():
    # Step 1: create a Databank and add data from 3 files to it.
    D = tk_databank.DataBank()
    
    raw_files = ['../data/network_1_713/X_17.mat']
    sensor_names = ['network_1_713 X_17']

    for i in range(len(raw_files)):
        tss_dummy, values = read_matlab_file(raw_files[i])

        # Apply transofrmation given by David
#        values = convert(values)

        ts = [x * (1/3200.) for x in range(len(values))]
#        ts = [x * 1e9 for x in ts]
        
        D.add_sensor_data(values, ts, sensor_id=i, sensor_type='Accelerometer', sensor_name=sensor_names[i],
            value_names=['x', 'y', 'z'], quantity=['Acceleration (g)', 'Acceleration (g)', 'Acceleration (g)'])
        
    # Step 2: process raw data of each sensor according to process parameters
    process_cases = [[640, 0.6], [640, 0.7], [640, 0.8], [640, 0.9], [800, 0.6], [800, 0.7], [800, 0.8], [800, 0.9]]
    #[400, 0.6], [400, 0.7], [400, 0.8], [400, 0.9], 
    process_id = 0
    for sensor_id in range(len(raw_files)):
        for framesize, reduction in process_cases:
            print '>>>', framesize, reduction
            D.sensor_data_process(sensor_id, framesize, reduction, 'cii_dct_v011')#, block=10) # 'cii_dct_v011', 'aii_v010'

#            recon_data = D.get_sensor_recon_data(sensor_id, process_id)
#            ups_values, ups_tss = up_sampling(recon_data['values'], recon_data['tss'])

#            D.add_processed_sensor_data(0, ups_values, ups_tss)
#            D.add_processed_sensor_data(0, ups_values, ups_tss, meta={'framesize': framesize, 'reduction': reduction})

#            process_id = process_id + 1

    D.save('Alstom_X_17_20160518.dbk')

    P = tk_plot.Plot(D)
#    for i in range(len(raw_files)):
#        #P.plot_raw_inspection(sensor_id=i, output='screen')
#        P.plot_rec_inspection(sensor_id=i, process_id=0, output='png')
    P.plot_rec_inspection(sensor_id=0, process_id=0, output='png')
    P.plot_rec_inspection(sensor_id=0, process_id=0, output='screen')

    P.print_deviations_as_table(sensor_id=0, out='stdout')

def read_matlab_file(filename):
    import scipy.io
    import datetime
    
    def ts_to_dt(ts, unit='ms'):
        s = ts / 1000.0
        #dt = datetime.datetime.fromtimestamp(s).strftime('%Y-%m-%d %H:%M:%S.%f')
        dt = datetime.datetime.fromtimestamp(s).strftime('%M:%S')
        return dt
    
    values_x = scipy.io.loadmat(filename)['x']
    values_x = [[x[0]] for x in values_x]

    n = len(values_x)
    sampling_rate = 3200. #Hz
    sampling_interval = 1/sampling_rate
    tss = [ i*sampling_interval * 1000 for i in range(n)]
    tss = [ts_to_dt(elm) for elm in tss]

    return(tss, values_x)


def convert(values, type='fiter_time_domain'):
    if type == 'fiter_time_domain':
        values_hlp = [ list(filter_time_domain(x)) for x in zip(*values) ]
        values_out = [list(x) for x in zip(values_hlp[0], values_hlp[1], values_hlp[2])]
    if type == 'filter_time_domain_without_scaling':
        values_hlp = [ list(filter_time_domain_without_scaling(x)) for x in zip(*values) ]
        if len(values_hlp) == 1: 
            values_out = [list(x) for x in zip(values_hlp[0])]        
        elif len(values_hlp) == 3:
            values_out = [list(x) for x in zip(values_hlp[0], values_hlp[1], values_hlp[2])]        
    elif type == 'filter_frequency_domain':
        filter_frequency_domain(zip(*values)[0])
    elif type == 'scale_time_domain':
        values_hlp = [ list(scale_time_domain(x)) for x in zip(*values) ]
        values_out = [list(x) for x in zip(values_hlp[0], values_hlp[1], values_hlp[2])]
    
    return(values_out)


def load_and_plot(filename, sensor_id=None, suptitle='Fourier analyze plot', path=None, mpd=400):
    D = tk_databank.DataBank()
    DC = tk_databank.DataBank()
#    D.load('Alstom_Test2_20160505_b15.dbk')

#    filename = 'Alstom_X_17_20160518.dbk'
    D.load(filename)


    raw = D.get_sensor_raw_data(sensor_id)
    raw_tss = raw['tss']
    raw_values = raw['values']
    raw_values_conv = convert(raw_values, type='filter_time_domain_without_scaling')

    DC.add_sensor_data(raw_values_conv, tss=raw_tss, sensor_id=sensor_id, sensor_type='Accelerometer', value_names=['x', 'y', 'z'], quantity=['Acceleration (g)', 'Acceleration (g)', 'Acceleration (g)'], sensor_name=raw['sensor_name'])

    n_pids = len(D.get_sensor_info(sensor_id)['processed_data'])
#    n_pids = 1

    for process_id in range(n_pids):          
        rec = D.get_sensor_recon_data(sensor_id, process_id)
        rec_tss = rec['tss']
        rec_values = rec['values']
        rec_values_conv = convert(rec_values, type='filter_time_domain_without_scaling')
        DC.add_processed_sensor_data(sensor_id, rec_values_conv, tss=rec_tss, datatype='recon_data', meta={'framesize': rec['framesize'], 'reduction': rec['reduction'], 'eng_name': rec['eng_name']})

        print "-"*100
        print 'sensor_id, process_id, framesize, reductionrate:', sensor_id, process_id, rec['framesize'], rec['reduction']
        print "-"*100
        print 'Coord'.ljust(15), 'RawRMSE'.ljust(15), 'RecRMSE'.ljust(15), 'RMSE'.ljust(15), 'Diff%'.ljust(15)
        print "-"*100

        for i in range(1):
            raw_component = zip(*raw_values)[i]
            rec_component = zip(*rec_values)[i]
            rawrmse = single_signal_rmse(raw_component)
            recrmse = single_signal_rmse(rec_component)
            rmse = tk_err.errperf(raw_component, rec_component, 'rmse')
            mape = tk_err.errperf(raw_component, rec_component, 'mape')
            smape = tk_err.errperf(raw_component, rec_component, 'smape')
            ratio = rmse / rawrmse * 100.
            print str(raw['value_names'][i]).ljust(15), str(rawrmse).ljust(15), str(recrmse).ljust(15), str(rmse).ljust(15), str(ratio).ljust(15)
        print

    for process_id in range(n_pids):
#        P = tk_plot.Plot(DC)
#        P.plot_rec_inspection(sensor_id=sensor_id, process_id=process_id, output='png')
        plot_rec_inspection(D, sensor_id, process_id, output='png', suptitle=suptitle, path=path, mpd=mpd)

    P = tk_plot.Plot(D)
    P.print_deviations_as_table(sensor_id=sensor_id, out='stdout')

#    D.write_sensor_data_files(0)
    

def load_and_plot_matlab_processed_data():
    D = tk_databank.DataBank()

    filename = 'Alstom_X_17_20160518.dbk'
    D.load(filename)

    sensor_id = 0
    n_pids = len(D.get_sensor_info(sensor_id)['processed_data'])
    n_pids=[1]
    for process_id in range(n_pids):
#        P = tk_plot.Plot(D)
#        P.plot_rec_inspection(sensor_id=sensor_id, process_id=process_id, output='png')
        plot_rec_inspection(D, sensor_id, process_id, output='png', suptitle='X_17')

    P = tk_plot.Plot(D)
    P.print_deviations_as_table(sensor_id=sensor_id, out='stdout')

#    D.write_sensor_data_files(0)
    
    

class TextPlot(object):
    def __init__(self, ax=None):
        if ax == None:
            self.fig = plt.figure(figsize=(8, 6))
            self.ax = fig.add_subplot(1, 1, 1)
        else:
            self.ax = ax
        self._colors = {'raw':'g', 'rec':'b', 'red':'r', 'subsampled':'c', 'deviation':'y', 'other':'grey'}
        #ax.axes.get_xaxis().set_visible(False)
        #ax.axes.get_yaxis().set_visible(False)
        ax.set_axis_off()

    def write(self, info):
        for i, (key, value) in enumerate(info):
            string = key+':  '+str(value)
            self.ax.text(0.0, 0.5-(i*0.25), string, va='top', ha='left', transform=self.ax.transAxes, fontsize=11)


def plot_rec_inspection(db, sensor_id, process_id, output='screen', fft_yscale='linear', dt_format='%Y-%m-%d %H:%M:%S', suptitle=None, path=None, mpd=400):

    # originating from a copy of the function in tk_plot
    """Plot different graphs in one figure to get a first insight into raw data

    Args:
        sensor_id (int): Sensor ID as set when loading data into databank.
        output ([string]): Optional. Output can be 'screen' (default) or 'png'.
        
    Returns:
        ln (line): line object handle
    
    """
    raw = db.get_sensor_raw_data(sensor_id)
    raw_tss = raw['tss']
    raw_values = zip(*raw['values'])
    
    rec = db.get_sensor_recon_data(sensor_id, process_id)
    rec_tss = rec['tss']
    rec_values = zip(*rec['values'])
    
    sensor_process_info = db.get_sensor_process_info(sensor_id, process_id)
    sensor_process_info['deviations']=db.get_deviations(sensor_id, process_id)
    
    if output == 'screen':
        ratio = [16, 8] #[4, 3]
        scale = 0.95  #2.5
        figsize = [x * scale for x in ratio]
        dpi = None
    elif output == 'png':
        ratio = [16, 7] #[4, 3]
        scale = 0.8
        figsize = [x * scale for x in ratio]
        dpi = 150

    for i in range(raw['dof']):
        fig = plt.figure(figsize=figsize, dpi=dpi, facecolor='w') # figsize default: (8, 6), dpi=150
        if suptitle == None:
            suptitle = raw['sensor_name']
        fig.suptitle(suptitle, fontsize=14)

        gs = gridspec.GridSpec(4, 4)
        if isinstance(raw_tss[0], datetime.datetime):
            # x axis labels are longer and will be rotated, therefore increase space
            gs.update(hspace=0.45)
        else:
            gs.update(hspace=0.2)
        
        ax1 = plt.subplot(gs[0:3, :-2])        
        ax2 = plt.subplot(gs[0:3, -2:])
        ax3 = plt.subplot(gs[-1:, :1])

        # The signal
        sp = tk_plot.SignalPlot(ax=ax1)
        sp.set_params(title=raw['value_names'][i], quantity=raw['quantity'][i], dt_format=dt_format)
        sp.add_signal(raw_tss, raw_values[i], 'raw')
        sp.add_signal(rec_tss, rec_values[i], 'rec')
        ax1.tick_params(labelbottom='on')
#        ax1.set_ylim([-5, 5])
        
        # Deviations
        n = len(rec_values[i])
        diff = tk_err.e(raw_values[i][:n], rec_values[i])
        rel_diff = diff / raw_values[i][:n] * 100

        # Fourier spectrum
        fftp = tk_plot.FftPlot(ax2)
        fftp.plot(raw_tss, raw_values[i], 'raw', skip_zero=True, yscale=fft_yscale, sw_rmse=True, mpd=mpd)
        fftp.plot(rec_tss, rec_values[i], 'rec', skip_zero=True, yscale=fft_yscale, sw_rmse=True, mpd=mpd)
        fftp.ax.yaxis.tick_right()
        fftp.ax.yaxis.set_label_position('right')

        # Print some error measures into the plot.
        tp = TextPlot(ax3)
        info = [
#            ('frame_size', sensor_process_info['framesize']),
            ('reduction (%)', sensor_process_info['reduction']),
#            ('eng_name', sensor_process_info['eng_name']),
            ('RRMSE%', sensor_process_info['deviations']['RRMSE%'][i])
#            ('WAPE%', sensor_process_info['deviations']['WAPE%'][i])
        ]
        tp.write(info)

        if output == 'png':
            fig.savefig(path+raw['sensor_name']+'_sid'+str(rec['sensor_id'])+'_f'+str(rec['framesize'])+'_r'+str(rec['reduction'])+'_'+raw['value_names']+'.png', dpi=dpi)
            plt.close(fig)
            
    if output == 'screen':
        plt.show()






def visualize_raw():
    import matplotlib.pyplot as plt
    from tools import tk_plot
    
    raw_files = [
        #'TEST1/2016-04-15 15_18_24_raw_data_1_test_dataset.csv',
        #'TEST2/2016-04-18 13_50_18_raw_data_1_test_dataset.csv',
        'TEST3/2016-04-21 12_56_53_raw_data_4_test_dataset.csv'

    ]
    rec_files = [
        #'TEST1/2016-04-15 15_18_24_rec_data_1_test_dataset.csv',                 
        #'TEST2/2016-04-18 13_50_19_rec_data_1_test_dataset.csv',
        'TEST3/2016-04-21 12_56_53_rec_data_4_test_dataset.csv'
    ]
    sensor_names = ['Alstom Test1', 'Alstom Test2', 'Alstom Test3']

    D = tk_databank.DataBank()
    for i in range(len(raw_files)):
        values = read_alstom_file(raw_files[i])
        
        # Apply transofrmation given by David
        values = convert(values)
                
        ts = [x * (1/3200.) for x in range(len(values))]
        S1 = D.add_sensor_data(values, ts, sensor_id=i, sensor_type='Accelerometer', sensor_name=sensor_names[i],
            value_names=['x', 'y', 'z'], quantity=['Acceleration (g)', 'Acceleration (g)', 'Acceleration (g)'])

        P = tk_plot.Plot(D)
        P.plot_raw_inspection(sensor_id=i)

    
if __name__ == '__main__':
#    process_and_save()
#    process_and_save_matlab_file()
    load_and_plot()
#    load_and_plot_matlab_processed_data()
#    visualize_raw()
#    subsample_process_and_save()
#    step_process_and_save()