# -*- coding: utf-8 -*-
"""
Created on Mon Jul 25 10:28:02 2016

@author: Etienne
"""

##############################################################################
#Importation


from ksvd import KSVD
import numpy as np
from sklearn.decomposition import SparseCoder
from sklearn.decomposition import DictionaryLearning
import tk_err
import dictionaries_generation as gen
import Fourier_filter as ffl
from plot_dictionary import sup_int



def double_training(h, n_components=100, alpha=1, max_iter=2000, tol=1e-9, fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=None, transform_alpha=None, n_jobs=1, 
                    code_init=None, dict_init=None, verbose=False, split_sign=False, random_state=None, tol_2=None, tol_1=None, n_nonzero_coefs=None, factor=10):
    
    dictio = DictionaryLearning(n_components=n_components, alpha=alpha, 
                                            max_iter= max_iter, tol=tol,
                                            fit_algorithm=fit_algorithm, transform_algorithm=transform_algorithm, transform_n_nonzero_coefs=transform_n_nonzero_coefs, transform_alpha=transform_alpha, n_jobs=n_jobs, 
                                            code_init=code_init, dict_init=dict_init, verbose=verbose, 
                                            split_sign=split_sign, random_state=random_state, tol_2=tol_2 if (tol_2 is None) else tol_2*factor, tol_1=tol_1, n_nonzero_coefs=n_nonzero_coefs)
                                            
                                            #On this step is the dictionary learned, and x is then transformed in the new basis
    x=dictio.fit_transform(h)
            
    #Analyses the results        
    D=dictio.components_ #here is the final dictionary
    
    fit_algo='omp'
    
    dictio = DictionaryLearning(n_components=n_components, alpha=alpha, 
                                            max_iter= max_iter, tol=tol,
                                            fit_algorithm=fit_algo, transform_algorithm=transform_algorithm, transform_n_nonzero_coefs=transform_n_nonzero_coefs, transform_alpha=transform_alpha, n_jobs=n_jobs, 
                                            code_init=x, dict_init=D, verbose=verbose, 
                                            split_sign=split_sign, random_state=random_state, tol_2=tol_2, tol_1=tol_1, n_nonzero_coefs=n_nonzero_coefs)
                                       
    x=dictio.fit_transform(h)
    D=dictio.components_
    return D,x
    
def svd_double_training(h, n_components=100, alpha=1, max_iter=2000, tol=1e-9, fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=None, transform_alpha=None, n_jobs=1, 
                     verbose=False, split_sign=False, random_state=None, tol_2=None, tol_1=None, n_nonzero_coefs=None, factor=10, enable_threading=False):
    print 'Initialization : svd'
    frame_size=len(h[0])
    D_0,c_0=KSVD(h, n_components, frame_size/3, 5, enable_threading=enable_threading)
    D,x=double_training(h, n_components=n_components, alpha=alpha, max_iter=max_iter, tol=tol, fit_algorithm=fit_algorithm, transform_algorithm=transform_algorithm, transform_n_nonzero_coefs=transform_n_nonzero_coefs, 
                        transform_alpha=transform_alpha, n_jobs=n_jobs, code_init=c_0, dict_init=D_0, verbose=verbose, 
                        split_sign=split_sign, random_state=random_state, tol_2=tol_2, tol_1=tol_1, n_nonzero_coefs=n_nonzero_coefs, factor=factor)
    return D,x
    
def learning_omp(y_b, D, code_init=None, train_data=None, alpha=1, max_iter=2000, tol=1e-9, fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=None, transform_alpha=None, n_jobs=1, 
                     verbose=False, split_sign=False, random_state=None, tol_2=None, tol_1=None, n_nonzero_coefs=None, factor=10, enable_threading=False, 
                     sparse_tol=None, tol_1sparse=None, n_nonzero_coefs_sparse=None, enable_printing=True, enable_printing_results=True,
                     n_nonzero_limit=None, probability_limit=0.6, retrain_limit=100):
    
    n_components=len(D)
    frame_size=len(D[0])
    resolution = len(y_b);
    n_segm = sup_int((resolution+0.0)/frame_size) #is the number of segments which decompose the original signal
    if (resolution%frame_size>0):
        for j in range (frame_size-(resolution%frame_size)):
            y_b=np.concatenate((y_b, [y_b[resolution-1]]))            
    resolution = len(y_b);    
    h_b=np.zeros((n_segm, frame_size))    
    
    for l in range(n_segm):
        h_b[l]=(y_b[l*frame_size:(l+1)*frame_size])   
    variance_b=(np.mean(y_b**2)-np.mean(y_b)**2)
    
    x_1=np.zeros((n_segm, n_components))
    if train_data is None:
        h=h_b
        c_0=x_1
        start=0
    else:
        h=np.concatenate((train_data, h_b), axis=0)
        c_0=np.concatenate((code_init, x_1), axis=0)
        start=len(train_data)
    err=0
    lastretrain=0
    if n_nonzero_limit is None:
        n_nonzero_limit=frame_size
    coder = SparseCoder(dictionary=D, transform_n_nonzero_coefs=n_nonzero_coefs_sparse,
                                transform_alpha=sparse_tol*sparse_tol*variance_b*frame_size, transform_algorithm=transform_algorithm, tol_1=tol_1sparse)        
    
    for i in range(n_segm):
        x_1[i]=coder.transform(h_b[i].reshape(1, -1))
        c_0[i+start]=x_1[i]
        #check if we have too many non zeros
        if (len(np.flatnonzero(x_1[i]))>n_nonzero_limit):
            err+=1
        #if we have too often too many zeros, let's retrain our dictionary    
        if (err>(1-probability_limit)*(i-lastretrain)) and ((i-lastretrain)>=retrain_limit):
            if (enable_printing):
                print '%s errors from the last retrain (%s segments)' %(err, (i-lastretrain))
                print 'Retraining the dictionary (double training method) ...'
            D,x=double_training(h[:start+i+1], n_components=n_components, alpha=alpha, max_iter=max_iter, tol=tol, fit_algorithm=fit_algorithm, transform_algorithm=transform_algorithm, transform_n_nonzero_coefs=transform_n_nonzero_coefs, 
                        transform_alpha=transform_alpha, n_jobs=n_jobs, code_init=c_0[:start+i+1], dict_init=D, verbose=verbose, 
                        split_sign=split_sign, random_state=random_state, tol_2=tol_2, tol_1=tol_1, n_nonzero_coefs=n_nonzero_coefs, factor=factor)
            c_0[:start+i+1]=x
            x_1[:i+1]=x[start:]
            if (enable_printing_results):
                print 'Training results :'
                density=len(np.flatnonzero(x_1)) #number of non zero coefficients
                z_1=np.ravel(np.dot(x,D))
                y=np.ravel(h[:start+i+1])
                rsquared_error = tk_err.rrmse(y, z_1, norm='std') #RMSE
                maxmin_error = tk_err.rrmse(y, z_1, norm='maxmin')
                print 'RRMSE : ', rsquared_error
                print 'Maxmin_RRMSE : ', maxmin_error
                print 'Infinity norm : ', np.max(np.abs(y-z_1))
                print 'Non zero coeffs : ', density
                print 'Compression ratio : ', 100*(1-(density+0.0)/((i+1)*frame_size))
            if (enable_printing):
                print '-'*40
            lastretrain=i
            err=0
            coder.components_=D   
    return x_1,D