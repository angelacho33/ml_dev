"""
Teraki Data Process Package
Author: <author name>
Class for SignalDictionaryEncoder
"""

from tk_base_engine import BaseEngine
#from sklearn.decomposition import SparseCoder
from sklearn_library_modifications.dict_learning import SparseCoder
import numpy as np
import tk_err

def dct_iii(X):
    X = np.array(X)
    PI = np.pi
    N = X.shape[0]
    x = [None] * N
    for n in range(N):
            summation = 0.
            for k in range(N):
                    s = 0.5**0.5 if k==0 else 1.0
                    summation += s * X[k] * np.cos(PI * (n + 0.5) * k / N)
                    x[n] = summation * (2.0 / N)**0.5
    x_reform = [list(a) for a in zip(*x)]
    return x_reform  
    
def sawtooth(length, invwidth):
    """Discrete sub-sampled sawtooth wavelet"""
    import scipy.signal as scs
    x = np.linspace(0, length - 1, length)
    x = scs.sawtooth(2*np.pi*x*(invwidth), width=0.5);
    return (x+1)


class SignalDictionaryEncoder(BaseEngine):
    """Class for SignalDictionaryEncoder
    It inherits all the attributes and methods from BaseEngine, 
    and overwrite the following public methods:

    Engine Type:  Encoder
    Engine Layer: Signal
    Working Mode: Compression (filter and convertion)

                                   +-------------------------+            
                                   |                         |            
     [X0, X1, X2,... Xn] --------->| SignalDictionaryEncoder |-----------> [Y0, Y1, ..., Ym]
                                   |                         |              m < n                
                                   +-------------------------+            
                                                ^
                                                |
                                            dictionary

    Public Methods:
        single_encoding():  Single signal encoding
        frame_encoding():  Frame signals encoding
        batch_encoding():  Batch signals encoding
        thresholding(): thresholds a reduced data according to the parameters
    """
    def __init__(self, config_para={}):
        """initiate Engine instance

        Args: 
            config_para: configuration for the Engine is it a dictionary of following:
            {
                'engine_id':  <engine_id>,
                'engine_type':  <engine_type>,
                'engine_layer': <engine_layer>,
                'f': <framesize>,
                'did': <dictionary_id>,
                'd': <dictionary>
                'tol_2': <toleranceRRMSE> is the tolerance in RRMSE (maxmin norm). Watch out, it is not in 
                                          percentage, so tol_2=0.02 means 2% RRMSE. Set it to 1 if you don't want to use it.
                'tol_1': <tolerancedeviation> is the tolerance in the maximal deviation (it is not relative to the signal).
                                              set it to None if you don't want to use it
                'n_nonzero' : <maximal number of nonzero per segment> set it to None if you don't want to use it
            }

            <engine_name>, <engine_type> and <engine_layer> are string, <dictionary_id> is the id
            of the <dictionary> and <dictionary> is the dictionary. the format of <dictionary> is
            to be discussed.

            Here is an example of input config_para:
            {
                'engine_id': "encoding_0",
                'engine_type': "encoder",
                'engine_layer': "signal",
                'did': 1,
                'f': 5,
                'd': [
                    [0, 3, 9, 20, 30],
                    [1, 8, 9, 25, 23],
                    ...
                    [1, 23, 27, 33, 49]
                ],
                'tol_2':0.03,
                'tol_1':None,
                'n_nonzero':None
            }

        Returns: 
            None
        """ 
        self.engine_id=config_para['engine_id']
        self.engine_type=config_para['engine_type']
        self.engine_layer=config_para['engine_layer']
        self.f=config_para['f']
        self.did=config_para['did']
        self.d=config_para['d']
        self.tol_2=config_para['tol_2']
        self.tol_1=config_para['tol_1']
        self.n_nonzero=config_para['n_nonzero']
        BaseEngine.__init__(self, config_para)
        return


    def thresholding(self, x_1, y_b, step):
        """Thresholds the coefficients x_1 corresponding to the signal y_b. It finds the right threshold
        by increasing from step at each iteration, until we can't increase without exceeding the tolerance parameters.
        """
        maxi=np.max(np.abs(x_1))
        z_1=np.ravel(np.dot(x_1,self.d))
        deviation=np.max(np.abs(y_b-z_1))
        rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin') #RMSE
        thresh=0
        x_2=np.copy(x_1)
        #Find the right threshold to set
        while ((rsquared_error<=100*self.tol_2) and (thresh < 100) and deviation<=self.tol_1):
            thresh+=step
            x_2[np.abs(x_2)<thresh/100*maxi]=0
            z_1=np.ravel(np.dot(x_2,self.d))
            rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin') #RMSE
            deviation=np.max(np.abs(y_b-z_1))
        
        while ((rsquared_error>100*self.tol_2 or deviation>self.tol_1) and (thresh>0) ):
            x_2=np.copy(x_1)
            thresh-=step
            x_2[np.abs(x_2)<thresh/100*maxi]=0
            z_1=np.ravel(np.dot(x_2,self.d))
            rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
            deviation=np.max(np.abs(y_b-z_1))      
            #Set the low coefficients to 0 with the right threshold
        x_1[np.abs(x_1)<thresh/100*maxi]=0
        return x_1, thresh
            
            
    def single_encoding(self, value, para=None, threshold=None):
        """ Single signal encoding
        Args: 
            value : single signal value (or multiaxes signal)
            para: None
        Returns:
            ret_v : a list of encoded values for one frame (<frame_data>) if it is available, 
                    otherwise None.
                    ret_v[:, 0] are the transformed coefficients
                    ret_v[:, 1] is the corresponding index
                    NOTE: the length of <frame_data> is not a fixed value.
                          the index is not reduced here
            ret_para:  None
            
        If the signal has multiaxes, this method will concatenate window by window all the axes. In this way,
        it treats a one dimensional signal (multidimensional is not possible), but by doing this, it actually
        treats all the axes in the same time. If the axes are not totally independant, this improves the reduction results.
        The dictionary then has to have its frame size equal to number_of_axes*self.f 
        """
        
        if (len(value)%self.f!=0):
            print 'The length of the input data has to be a multiple of %s' % self.f
            return None
        maxmin=(np.max(value)-np.min(value))      
        if (maxmin<1e-10):
            return value[0], None
        
        coder = SparseCoder(dictionary=self.d, transform_n_nonzero_coefs=self.n_nonzero, transform_alpha=self.tol_2*self.tol_2*maxmin*maxmin*self.f if self.tol_2 is not None else None, 
                            transform_algorithm='omp', tol_1=self.tol_1)
                            
        red=coder.transform(value.reshape(len(value)/self.f , self.f)) #transform the data divided in several frames
       
        
#        thresholding Phase
        if threshold is not None :
           red, thresh=self.thresholding(red, value, threshold)
           print 'Threshold (%) : ', thresh 
        red=np.ravel(red)
            
        l_1=np.flatnonzero(red) #is the index
        red=red[l_1]            #is the coefficients amplitude
        
        red=np.concatenate((red.reshape(len(red), 1), l_1.reshape(len(l_1), 1)), axis=1) #we need to reshape to concatenate
        
        return red, para

    def frame_encoding(self, values, para=None):
        """ Frame signals encoding
        Args: 
            value: a list of value in one frame
            para:  None

        Returns:
            ret_v:  a list of encoded value in one frame, <frame_data>
                     ret_v[:, 0] are the transformed coefficients
                     ret_v[:, 1] is the corresponding index
                    NOTE: the length of <frame_data> is not a fixed value.
                          the index is not reduced here
            ret_para:  None
        """     
        red=[]
        for value in values:        
            maxmin=(np.max(value)-np.min(value))      
            if (maxmin<1e-10):
                return value[0], None
                
            coder = SparseCoder(dictionary=self.d, transform_n_nonzero_coefs=self.n_nonzero, transform_alpha=self.tol_2*self.tol_2*maxmin*maxmin*self.f if self.tol_2 is not None else None, 
                                transform_algorithm='omp', tol_1=self.tol_1)
                                
            red=coder.transform(value.reshape(self.f, 1)) #transform the data divided in several frames
            red=np.ravel(red)
            
            l_1=np.flatnonzero(red) #is the index
            red=red[l_1]            #is the coefficients amplitude
          
            red.append(np.concatenate((red.reshape(len(red), 1), l_1.reshape(len(l_1), 1)), axis=1)) #we need to reshape to concatenate
            
            return red, para

    def batch_encoding(self, values, para=None, threshold=None):
        """ Batch signals encoding
        Args: 
            value: a list of value to be encoded
            para:  None

        Returns:
            ret_v:  a list of all the encoded values in one or multiple <frame_data>
                    if it does not reach one frame, return None.        
                    ret_v[:, 0] are the transformed coefficients
                    ret_v[:, 1] is the corresponding index
                    NOTE: the length of <frame_data> is not a fixed value.
                          the index is not reduced here
            ret_para:  a list of data_size for each <frame_data>
        """
        red=[]
        for value in values:
            if (len(value)%self.f!=0):
                print 'The length of the input data has to be a multiple of %s' % self.f
                return None
            maxmin=(np.max(value)-np.min(value))      
            if (maxmin<1e-10):
                return value[0], None
                
            coder = SparseCoder(dictionary=self.d, transform_n_nonzero_coefs=self.n_nonzero, transform_alpha=self.tol_2*self.tol_2*maxmin*maxmin*self.f if self.tol_2 is not None else None, 
                                transform_algorithm='omp', tol_1=self.tol_1)
                                
            red=coder.transform(value.reshape(len(value)/self.f , self.f)) #transform the data divided in several frames
            #        thresholding Phase
            if threshold is not None :
                red, thresh=self.thresholding(red, value, threshold)
                print 'Threshold (%) : ', thresh
            red=np.ravel(red)
            
            l_1=np.flatnonzero(red) #is the index
            red=red[l_1]            #is the coefficients amplitude
          
            red.append(np.concatenate((red.reshape(len(red), 1), l_1.reshape(len(l_1), 1)), axis=1)) #we need to reshape to concatenate
            
        return red, para    
      
  
    def compare_single_signal(self, y_b, D=None, para=None, frame_size=None, threshold=None):   
        """Returns the reduction ratio and RRMSE corresponding to the encoding with another dictionary D (for an input dataset y_b)
        """
        if frame_size==None:
            frame_size=self.f
            
        if (len(y_b)%frame_size!=0):
            print 'The length of the input data has to be a multiple of %s' % frame_size
            return None
        maxmin=(np.max(y_b)-np.min(y_b))      
        
        coder = SparseCoder(dictionary=D, transform_n_nonzero_coefs=self.n_nonzero, transform_alpha=self.tol_2*self.tol_2*maxmin*maxmin*frame_size if self.tol_2 is not None else None, 
                            transform_algorithm='omp', tol_1=self.tol_1)
                            
        x_1=coder.transform(y_b.reshape(len(y_b)/frame_size, frame_size)) #transform the data divided in several frames
        
        
        if threshold is not None:            
            maxi=np.max(np.abs(x_1))
            z_1=np.ravel(np.dot(x_1,D))
            deviation=np.max(np.abs(y_b-z_1))
            rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin') #RMSE
            thresh=0
            x_2=np.copy(x_1)
            #Find the right threshold to set
            while ((rsquared_error<=100*self.tol_2) and (thresh < 100) and deviation<=self.tol_1):
                thresh+=threshold
                x_2[np.abs(x_2)<thresh/100*maxi]=0
                z_1=np.ravel(np.dot(x_2,D))
                rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin') #RMSE
                deviation=np.max(np.abs(y_b-z_1))
            
            while ((rsquared_error>100*self.tol_2 or deviation>self.tol_1) and (thresh>0) ):
                x_2=np.copy(x_1)
                thresh-=threshold
                x_2[np.abs(x_2)<thresh/100*maxi]=0
                z_1=np.ravel(np.dot(x_2,D))
                rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
                deviation=np.max(np.abs(y_b-z_1))      
                #Set the low coefficients to 0 with the right threshold
            x_1[np.abs(x_1)<thresh/100*maxi]=0
            
        reduct=100*(1-(len(np.flatnonzero(x_1))+0.0)/len(y_b))
        z_1=np.ravel(np.dot(x_1,D))
        rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin') #RMSE
        return reduct, rsquared_error
    
    def dct_compare_single_signal(self, y_b, para=None, frame_size=None, threshold=None):
        """
        Returns the reduction ratio and RRMSE corresponding to the DCT method (for an input dataset y_b)
        """
        if frame_size==None:
            frame_size=self.f
            
        D_dct = np.array(dct_iii(np.identity(frame_size)))
        reduct, rsquared_error = self.compare_single_signal(y_b, D=D_dct, para=para, frame_size=frame_size, threshold=threshold)
        return reduct, rsquared_error
      
    def sawtooth_compare_single_signal(self, y_b, para=None, frame_size=None, threshold=None, n_components=None):
        """
        Returns the reduction ratio and RRMSE corresponding to the sawtooth method (for an input dataset y_b)
        """
        if frame_size==None:
            frame_size=self.f
        if n_components is None:
            n_components=frame_size
        D_saw=np.zeros((n_components, frame_size))
        for i in range(n_components):
            D_saw[i]=sawtooth(frame_size, (i+0.0)/(2*frame_size))-1
            D_saw[i]/=np.sqrt(np.sum(D_saw[i]**2))
        reduct, rsquared_error = self.compare_single_signal(y_b, D=D_saw, para=para, frame_size=frame_size, threshold=threshold)
        return reduct, rsquared_error
        
    def wavelet_compare_single_signal(self, y_b, para=None, frame_size=None, threshold=None, n_components=None, wavelet='haar'):
        """
        Returns the reduction ratio and RRMSE corresponding to the wavelet method (for an input dataset y_b)
        You can chose your wavelet among the pywt package list.
        """
        if frame_size==None:
            frame_size=self.f
        if frame_size%2!=0:
            print 'Frame size has to be even'
            return None
        if n_components is None:
            n_components=frame_size
        import pywt
        D_wav=np.identity(frame_size/2)
        D_wav=np.concatenate((pywt.idwt(D_wav, None, wavelet), pywt.idwt(None, D_wav, wavelet)))
        if D_wav.shape!=(frame_size, n_components):
            print 'Problem with pywt, wrong shape of the wavelet dictionary'
            return None
        for i in range(n_components):
            D_wav[i]/=np.sqrt(np.sum(D_wav[i]**2))
        reduct, rsquared_error = self.compare_single_signal(y_b, D=D_wav, para=para, frame_size=frame_size, threshold=threshold)
        return reduct, rsquared_error
        
    
    
    def wavelet_compare_single_signal_old_version(self, y_b, para=None, frame_size=None, threshold=0.1, wavelet='haar'):
        """
        Old version of comparison with wavelet method. Not efficient enough.
        """
        import pywt
        if frame_size==None:
            frame_size=self.f
        if frame_size%2!=0:
            print 'Frame size has to be even'
            return None
        if (len(y_b)%frame_size!=0):
            print 'The length of the input data has to be a multiple of %s' % frame_size
            return None
        h=y_b.reshape(len(y_b)/frame_size, frame_size)
        thresh=0.
        (cA, cD)=(pywt.dwt(h, wavelet));
        rsquared_error=0.
        deviation=0.
        maxi=np.max(np.abs(cA))
        x_A=np.copy(cA)
        x_D=np.copy(cD)
        while ((rsquared_error<=100*self.tol_2) and (thresh < 100)and deviation<=self.tol_1):
            thresh+=threshold
            x_A[np.abs(x_A)<thresh/100*maxi]=0
            x_D[np.abs(x_D)<thresh/100*maxi]=0
            z_1=np.ravel((pywt.idwt(x_A, x_D, wavelet)))
            rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin') #RMSE
            deviation=np.max(np.abs(y_b-z_1))
            
        while ((rsquared_error>100*self.tol_2 or deviation>self.tol_1) and (thresh>0) ):
            x_A=np.copy(cA)
            x_D=np.copy(cD)
            thresh-=threshold
            x_D[np.abs(x_D)<thresh/100*maxi]=0
            x_A[np.abs(x_A)<thresh/100*maxi]=0
            z_1=np.ravel((pywt.idwt(x_A, x_D, wavelet)))
            rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
            deviation=np.max(np.abs(y_b-z_1))      
            #Set the low coefficients to 0 with the right threshold
            
        cA[np.abs(cA)<thresh/100*maxi]=0
        cD[np.abs(cD)<thresh/100*maxi]=0
        reduct=100*(1-(len(np.flatnonzero(cA))+len(np.flatnonzero(cD))+0.0)/len(y_b))
        z_1=np.ravel((pywt.idwt(cA, cD, wavelet)))
        rsquared_error = tk_err.rrmse(y_b, z_1, norm='maxmin') #RMSE
        return reduct, rsquared_error, thresh
