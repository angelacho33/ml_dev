# -*- coding: utf-8 -*-

## Scripts to process files from a folder both with RDP and the Dictionary Learning Routines
## Process a file means encode it and decode it...for the RDP this encoding-decoding is done explicity in this script
## instead of ussing the databank built-in function D.sensor_data_process(sensor_id, framesize,reduction, 'rdpi_v001', epsilon=0.001

## Note: The rdp encoding in this current implementation is performed in all the data considered..i.e not in a frame by frame way.

import time
import csv
import sys
import glob
import os

import numpy as np
import matplotlib.pyplot as plt

from dict_learning_methods.tk_dictionary_encoder import SignalDictionaryEncoder
from dict_learning_methods.tk_dictionary_decoder import SignalDictionaryDecoder

import dict_learning_methods.plot_dictionary as pd
from dict_learning_methods.dict_analysis import dict_read

import tk_gdfr
import tk_plot
import tk_err
from tk_databank import DataBank
from tk_plot import Plot

from tk_eng_rdpi_v001 import Decomposer
from tk_eng_rdpi_v001 import Reconstructor


D = DataBank()
P = Plot(D)
#folder = '../Files/'
folder='/home/angelh/Documents/DataScienceRelated/Teraki/ml_dev_tests/test4_comparisson_DL_RDP/Files/'

##Uncomment according to the file you want to process...or uncoment the sorted function below to process all the files in the folder
#filenamelist = [folder+'Audi_Sensor281.csv']
#filenamelist = [folder+'Audi_Sensor283.csv']
#filenamelist = [folder+'Audi_Sensor670.csv']
#filenamelist = [folder+'Audi_Sensor88.csv']
filenamelist = [folder+'AirBus_sensor_id6_BPTCHR.csv']
#filenamelist = [folder+'AirBus_sensor_id7_BPTCHR.csv']
#filenamelist = [folder+'AirBus_sensor_id9_BPTCHR.csv']
#filenamelist = [folder+'BMW_velocity_session_806c4190.csv']
#filenamelist = [folder+'Claas_sig20.csv']
#filenamelist = [folder+'Claas_sig27.csv']
#filenamelist = [folder+'Telefonica_160913_xAxis_v01.csv']


#filenamelist= sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)
print "filenamelist", filenamelist

##1)Add sensor data to a databank
for sensor_id,file in enumerate(filenamelist):
    sensor_name='sensor'
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file,',', 'pos', 'pos', 0, [1])
    framesize=50 ##Note1: The framesize in the current implementation of the rdp engine has no effect...
    
    if 'BMW' in file:
        sensor_name='BMW_velocity'
        framesize=30 #As used in Daniel script.
        #continue
    elif 'AirBus' in file:
        sensor_name='AirBus_BPTCHR'
        #continue
    elif 'Telefonica' in file:
        sensor_name='Telefonica'
        ##Note: Telefonica file has no timestamps in the file..so the values are in column 0
        sensor_data1 = tk_gdfr.get_ts_value_from_file(file,',', 'pos', 'pos', 0, [0])
        #continue
    elif 'Audi' in file:
        sensor_name='Audi'
        sensor_data1 = tk_gdfr.get_ts_value_from_file(file,',', 'pos', 'pos', 0, [0])
        #continue
    elif 'Claas' in file:
        sensor_name='Claas'
    ##Use all data points in the file:
    number_of_datapoints=len(sensor_data1['pos']['values']) 
    ##Or the first x points
    #number_of_datapoints=20000
    
    if 'Telefonica' in file: 
        ##Take more datapoints for the Telefonica case
        number_of_datapoints=100000
        ##Telefonica files has no time stamps..so create an array of timestamps assuming 1Hz of sampling rate
        timestamps=np.arange(0,number_of_datapoints)*1000
        #print timestamps
        #timestamps=timestamps[:number_of_datapoints]
    elif 'Audi' in file:
        timestamps=np.arange(0,number_of_datapoints)*1000
    else:
        timestamps=sensor_data1['pos']['tss'][:number_of_datapoints]
    
    if 'AirBus' or 'Claas' in file:
        print 'Converting time stamps in s to ms..'
        timestamps=np.asarray(timestamps)*1000
    
    values=sensor_data1['pos']['values'][:number_of_datapoints]
    
    #print "len(values)", len(values)
    #print 'First 10 values', values[:10]
    
    #print "len(timestamps)", len(timestamps)
    #print "First 10 timestams", timestamps[:10]

    #print 'values', values
    ##Add raw data to a DataBank...
    sensor_id=sensor_id
    S1 = D.add_sensor_data(values,timestamps, sensor_id=sensor_id, sensor_name=sensor_name)
    
    print '**********************'
    print 'Added ',number_of_datapoints, ' data points from ',sensor_name, ' to a Databank with sensor_id ', sensor_id 
    print '**********************'
    
##2) Process each sensor added...once with RDP (process_id=0) and once with DL (process_id=1)

for sensor in D.get_sensor_list():
    
    sensor_id=sensor['sensor_id']
    sensor_name=sensor['sensor_name']
    
    raw_data=D.get_sensor_raw_data(sensor_id)
    tss=np.asarray(raw_data['tss'])
    values=np.asarray(raw_data['values'])
    npoints=raw_data['count']
    
    print '*********************************************'
    print 'Processing ',npoints,' datapoints of sensor',sensor_id,' from ',sensor_name
    print '*********************************************'

    maxmin=np.max(values)-np.min(values) ##maxmin refers to the raw data amplitude
    
    values_array=np.asarray(values) #sequence of values in the shape [[1],[2],[3],[4]...]
    values=values_array.T[0] ##change to the shape [1,2,3,4,5]
    #print values[:10]
    data_for_rdp=[] ##rdp need as an input an array of tuples (x,y)
    for item,timestamp in enumerate(tss):
        data_for_rdp.append((timestamp,values[item]))
    #print "data_for_rdp", data_for_rdp 
    print "len(data_for_rdp)", len(data_for_rdp) 
    
    
    ####################################################################################
    ####################################################################################
    ##################################################################################################
    #########Process data with rdp, process_id=0
    
    #print 'Processing data with RDP...'
    epsilon=0.3
    t0 = time.time()
    print 'Starting timestamp for RDP processing', t0, 'for sensor_id:', sensor_id
    Decomposer_rdp = Decomposer(epsilon, framesize)
    rdp_data, rdp_list = Decomposer_rdp.decompose(data_for_rdp)
    #print time.time() 
    dt = time.time() - t0 
    #print 'rdp_data',rdp_data    
    rdp_data_values=np.asarray(rdp_data).T 
    
    #plt.plot(rdp_data_values[0],rdp_data_values[1],marker='o',ls='')
    #plt.plot(tss,values,ls='-')
    
    #plt.show()
    print '*************************************************************************'
    print '****************** RDP Decomposer ran in %.2f s'%dt
    print '*************************************************************************'
    #print len(rdp_data[0])
    
    print "rdp_data_values[0]",rdp_data_values[0]
    print "len(rdp_data_values[0])",len(rdp_data_values[0])
    
    reduction=(1-len(rdp_data_values[0])/float(len(values)))
    print 'reduction (RDP): ', reduction
    ##Process with rdp_v001 via sensor_data_process...
    #reduction=0.9 ##Note2: for rdp the reduction is controlled by epsilon so changing this variable does not have an effect in the results.
    sparsity=1-reduction ##Note3: sparsity has no effect in the rdp reconstruction either...
    ##Process Data with rdp
    #process_id=D.sensor_data_process(sensor_id=sensor_id, framesize=framesize, reduction=reduction, eng_name='rdpi_v001', epsilon=epsilon)

    
    ##Reconstruct data
    R = Reconstructor(sparsity, framesize)
    rec_data = R.reconstruct(rdp_data, rdp_list)
    #print "len(rec_data)",len(rec_data)
    
    #print rec_data
    rec_data=np.asarray(rec_data).T 
    rec_rdp_timestamps=rec_data[0]
    rec_rdp_values=rec_data[1]
    rec_rdp_values=rec_rdp_values.reshape(len(rec_rdp_values), 1) #Need an array with shape [[1],[2],[3]...] to add to databank
    
    #print "len(rec_rdp_values)",len(rec_rdp_values)
    #print "rec_rdp_values",rec_rdp_values
    #print "rec_rdp_timestamps",rec_rdp_timestamps
    
    
    process_id=D.add_processed_sensor_data(sensor_id, rec_rdp_values, rec_rdp_timestamps, datatype='recon_data', meta={'reduction' : reduction,'framesize' : framesize})
    
    #reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
    #count_red=reduce_info['count_red']
    #effective_reduction=reduce_info['ratio']
    
    ##Deviations 
    deviations=D.get_deviations(sensor_id,process_id)
    dof=0 ##Only one dof
    dev_max=deviations['E'][dof]['max']
    dev_mean=deviations['E'][dof]['mean']
    dev_min=deviations['E'][dof]['min']
    rrmse=deviations['RRMSE%'][dof]
    
    #print "Effective Reduction %.2d"%effective_reduction*100
    #print 'epsilon (for rdp)%d'%epsilon
    #print "rrmse %.2f"%effective_reduction*100
    #print "dev_mas %.2f"%dev_max
    
    #reduction_string=' reduction: %.2f'%reduction*100
    reduction_string=' ,reduction:'+str(round(reduction*100,2))
    suptitle='RDP, '+ sensor_name+reduction_string+'% ,rrmse:'+str(round(rrmse,2))+'%'+ ' ,dev_max:'+str(round(dev_max,5))+' ,epsilon:'+str(epsilon)+'\n Encoding done in %.2f s'%dt
    #' ,pid:'+str(process_id)+' ,sid:'+str(sensor_id)+
    #P.plot(sensor_id,process_id,deviation='difference',suptitle=suptitle,output='png',dt_format='%H:%M:%S')
    #P.plot(sensor_id,process_id,deviation='difference',suptitle=suptitle,output='screen',dt_format='%H:%M:%S')
    P.plot(sensor_id,process_id,deviation='difference',suptitle=suptitle,output='png',dt_format='%H:%M:%S')
    #P.plot(sensor_id=sensor_id,process_id=process_id,deviation='difference',suptitle=suptitle,output='png',dt_format='%Y-%m-%d %H:%M:%S')
    
    
    
    ##################################################################################################
    ##################################################################################################
    #################### Start Dictionary Learning Routine, process_id=1 #############################
    plot_dict=False
    write=True
    
    frame_size=framesize
    n_components=framesize
    
    data_tmp = values
    #print data_tmp
    #data_tmp = [x[0] for x in data_tmp]
    #        print data_tmp
    data_tmp = np.asarray(data_tmp)
    #        print data_tmp
    #       print "This is the new dataset",data_tmp
    #print "This is the dataset length",len(data_tmp)
    
    len_recon=len(data_tmp)     
    if (len_recon%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
        for j in range (frame_size-(len_recon%frame_size)):
            data_tmp=np.concatenate((data_tmp, [data_tmp[len_recon-1]]))
            
    #loadthedictionary
    if not os.path.exists('./Dictionaries'):
        os.makedirs('./Dictionaries')
    file_dict='./Dictionaries/Dictionary_'+sensor_name+'_sensorid_'+str(sensor_id)+'_omp_%s_%s.csv' % (n_components, frame_size)
    try:
        Dict=dict_read(file_dict)
        print '*************************************************************************'
        print 'Previously trained with framesize %d found.'%frame_size
    except IOError as e:
        print "Unable to load Dictionary...creating one..." #Does not exist OR no read permissions
        config_train={
            'engine_id': "encoding_0",
            'engine_type': "encoder",
            'engine_layer': "signal",
            'f' : frame_size,
            'n_components' : frame_size,
            'n_iter' : 50,
            'tol' : -1,
            'sparsity':1.0,
            'did': 1,
            'init' : 'svd',
            'tol_2': 1.0,
            #'tol_1': 0.01,
            'tol_1': 0.01*maxmin,
            'n_nonzero': None,
            'factor' : 5,
        } 
        
    
        print 'Training the dictionary ...'
        y_1=data_tmp[:len(data_tmp)/2] ##The dictionary is trainin with half of the data
        
        from dict_learning_methods.tk_dictionary_learning import DictionaryTraining
        dictio=DictionaryTraining(config_train)
        Dict=dictio.fit(y_1)
        print 'Dictionary trained.'
        
        if (plot_dict):
            if Dict is not None:
                fi4=pl.figure(4, figsize=(4.2, 4))
                fi4.suptitle('Initialization dictionary', fontsize=18)
                pd.plot_dic(Dict)
                #Writes the dictionary in a csv file
        if (write):
            #file_dict='../Dictionaries/dictionary_sensorid'+str(sensor_id)+'_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
            with open(file_dict, "wb") as f:
                csvfile = csv.writer(f, delimiter=';')
                for i in range(len(Dict[0])):
                    csvfile.writerow((Dict[:,i]))
                f.close()    
        
    config_enc={'engine_id': "encoding_0",
                'engine_type': "encoder",
                'engine_layer': "signal",
                'did': 1,
                'f': frame_size,
                'd': Dict,
                'tol_2': 1.0,
                #'tol_1': 0.01,
                'tol_1': 0.01*maxmin,
                'n_nonzero': None}    
    
    t0 = time.time()
    print 'Starting timestamp for DL', t0
    encoder=SignalDictionaryEncoder(config_enc)
    red,_=encoder.single_encoding(data_tmp)
    dt = time.time() - t0 
    print '*************************************************************************'
    print '************************** Dictionary Learning deconding ran in %.2f s'%dt
    print '*************************************************************************'
    print 'Note: This time refers only to the encoding part to compare with RDP. So the time to create the dictionary is not taken into account'
    
    reduction = (1-red.shape[0]/float(len(data_tmp)))
    print 'reduction (DL): ', reduction
    
    config_dec={'engine_id': "encoding_0",
                'engine_type': "encoder",
                'engine_layer': "signal",
                'did': 1,
                'f': frame_size,
                'd': Dict}
    
    decoder=SignalDictionaryDecoder(config_dec)
    recon,_=decoder.single_decoding(red, length=len(data_tmp))
    recon=recon[:len_recon]
    
    #print "*********recon***********",recon
    
    process_id=D.add_processed_sensor_data(sensor_id, recon, tss, datatype='recon_data', meta={'reduction' : reduction,'framesize' : frame_size})
    
    
    ##Deviations 
    deviations=D.get_deviations(sensor_id,process_id)
    dof=0 ##Only one dof
    dev_max=deviations['E'][dof]['max']
    dev_mean=deviations['E'][dof]['mean']
    dev_min=deviations['E'][dof]['min']
    rrmse=deviations['RRMSE%'][dof]
    
    #print "Effective Reduction %.2d"%effective_reduction*100
    #print 'epsilon (for rdp)%d'%epsilon
    #print "rrmse %.2f"%effective_reduction*100
    #print "dev_mas %.2f"%dev_max
    
    #reduction_string=' reduction: %.2f'%reduction*100
    #reduction_string=' ,reduction:'+str(reduction*100)
    reduction_string=' ,reduction:'+str(round(reduction*100,2))
    suptitle='DL, '+ sensor_name+reduction_string+'% ,rrmse:'+str(round(rrmse,2))+'%'+ ' ,dev_max:'+str(round(dev_max,5))+'\n Encoding done in %.2f s'%dt
    #' ,pid:'+str(process_id)+' ,sid:'+str(sensor_id)+
    P.plot(sensor_id,process_id,deviation='difference',suptitle=suptitle,output='png',dt_format='%H:%M:%S')
    #P.plot(sensor_id=sensor_id,process_id=process_id,deviation='difference',suptitle=suptitle,output='png',dt_format='%Y-%m-%d %H:%M:%S')

    
#Save dbk file with both RDP and DL processes
D.save("Databank.dbk")   
    
    
