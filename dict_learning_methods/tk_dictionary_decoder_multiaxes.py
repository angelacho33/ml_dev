"""
Teraki Data Process Package
Author: <author name>
Class for SignalDictionaryDecoder
"""

from tk_base_engine import BaseEngine
import numpy as np

def sup_int(n):
    if (int(n)==n):
        return int(n)
    else :
        return int(n)+1

        
class SignalDictionaryDecoder(BaseEngine):
    """Class for SignalDictionaryDecoder
    It inherits all the attributes and methods from BaseEngine, 
    and overwrite the following public methods:

    Engine Type:  Decoder
    Engine Layer: Signal
    Working Mode: Decompression

                                   +-------------------------+            
                                   |                         |            
     [X0, X1, X2,... Xn] <---------| SignalDictionaryDecoder |<----------- [Y0, Y1, ..., Ym]
                                   |                         |              m < n                
                                   +-------------------------+            
                                                ^
                                                |
                                            dictionary

    Public Methods:
        single_decoding():  Single signal decoding
        frame_decoding():  Frame signals decoding
        batch_decoding():  Batch signals decoding
        
    Only the single_decoding is now available as e multiaxes method.
    """
    def __init__(self, config_para={}):
        """initiate Engine instance

        Args: 
            config_para: configuration for the Engine is it a dictionary of following:
            {
                'engine_id':  <engine_id>,
                'engine_type':  <engine_type>,
                'engine_layer': <engine_layer>,
                'f': <framesize>,
                'did': <dictionary_id>,
                'd': <dictionary>
                'n_comp' : <n_components> will be automatically detected
            }

            <engine_name>, <engine_type> and <engine_layer> are string, <dictionary_id> is the id
            of the <dictionary> and <dictionary> is the dictionary. the format of <dictionary> is
            to be discussed.

            Here is an example of input config_para:
            {
                'engine_id': "decoding_0",
                'engine_type': "decoder",
                'engine_layer': "signal",
                'did': 1,
                'f': 50,
                'd': [
                    [0, 3, 9, 20, 30],
                    [1, 8, 9, 2, 25, 23],
                    ...
                    [1, 2, 3, 9, 23, 27, 33, 49]
                ]
            }

        Returns: 
            None
        """ 
        self.engine_id=config_para['engine_id']
        self.engine_type=config_para['engine_type']
        self.engine_layer=config_para['engine_layer']
        self.d=config_para['d']
        
        try:
            self.shape=config_para['shape']
        except KeyError:
            self.shape=1
        try:
            self.f=config_para['f']
            self.f*=self.shape
            if (self.f!=(self.d).shape[1]):
                print 'Error, the frame size is not set as d.shape[1] : ' + str(self.f) + '!=' + str((self.d).shape[1])
        except KeyError:
            self.f=(self.d).shape[1]
        self.did=config_para['did']        
        self.n_comp=len(config_para['d'])
        BaseEngine.__init__(self, config_para)
        return

    def single_decoding(self, value, maxmin_r=1, para=None, length=None):
        """ Single signal decoding 
        Args: 
            value : single encoded signal value
            para: None
            length : the length of the signal to reconstruct
        Returns:
            ret_v : a list of decoded values for one frame (<frame_data>) if it is available, 
                    otherwise None.
            ret_para:  None
        """
        n_components=self.n_comp #it's here the case of a square matrix
        if length is not None:
            length*=self.shape
        if (value.shape==()):
            if length is None:
                value=value*np.ones(self.f)
            else:
                value=value*np.ones(length)
                
            value=value.reshape(len(value), 1)
            return value, para
            
        x_1,l=value[:,0],value[:,1]    
        l=l.astype(np.int, copy=False)
        if (length==None):
            x=np.zeros(((sup_int(l[len(l)-1]+0.0)/n_components))*n_components)
        else:
            x=np.zeros(length)
        x[l]=x_1[:]
        x=x.reshape(len(x)/n_components, n_components)
        value=np.ravel(np.dot(x, self.d))
        value=value.reshape(len(value)/self.shape, self.shape)
        
#        rescale each axis after the reconstruction
        value*=maxmin_r
        return value, para

    def frame_decoding(self, values, para=None):
        """ Frame signals encoding
        Args: 
            value: a list of encoded value in one frame, <frame_data>
                   NOTE: the length of <frame_data> is not a fixed value.
            para:  None

        Returns:
            ret_v:  a list of decoded value in one frame, 
            ret_para:  None
        """
        ret_values=[]
        for value in values:
            n_components=self.n_comp #it's here the case of a square matrix
            if (value.shape==()):                
                val=value*np.ones(self.f)
                val=val.reshape(self.f, 1)
                ret_values.append(val)
                
            else:
                x_1,l=value[:,0],value[:,1]
                l=l.astype(np.int, copy=False)
                x=np.zeros(n_components)
                x[l]=x_1[:]
                ret_values.append((np.dot(x, self.d)).reshape(self.f, 1))
                
        return ret_values, para

    def batch_decoding(self, values, para=None, length=None):
        """ Batch signals encoding
        Args: 
            value: a list of encoded value in one or multiple frames
            para: a list of data_size for each <frame_data> 
            
            length : the length of the signal to reconstruct

        Returns:
            ret_v:  a list of decoded value in one or multiple frames
            ret_para:  None
        """
        ret_values=[]
        for value in values:
            n_components=self.n_comp #it's here the case of a square matrix
            if (value.shape==()):
                if length is None:
                    val=value*np.ones(self.f)
                else:
                    val=value*np.ones(length)
                val=val.reshape(len(val), 1)
                ret_values.append(val)
            else:                
                x_1,l=value[:,0],value[:,1]
                l=l.astype(np.int, copy=False)
                if (length==None):
                    x=np.zeros(((sup_int(l[len(l)-1]+0.0)/n_components))*n_components)
                else:
                    x=np.zeros(length)
                x[l]=x_1[:]
                x=x.reshape(len(x)/n_components, n_components)
                val=np.ravel(np.dot(x, self.d))
                val=val.reshape(len(val), 1)
                ret_values.append(val)
        return values, para


# Analyze functions
    def recover_red(self, red, l, length=None):
        """Recover the sparse matrix x from its coefficients list red and its index list l.
        """
        n_components=self.n_comp #it's here the case of a square matrix
        l=l.astype(np.int, copy=False)
        if (length is None):
            x=np.zeros(((sup_int(l[len(l)-1]+0.0)/n_components))*n_components)
        else:
            x=np.zeros(length)
        x[l]=red[:]
        x=x.reshape(len(x)/n_components, n_components)
        return x
        
    def nonzero_histplot(self, red, l, length=None, png=True, filename=None):
        """Plot the non zero coefficients per segment histogram from the coefficients list red and the index list l.
        """
        import matplotlib.pylab as pl
        n_components=self.n_comp
        x=self.recover_red(red, l, length=length)
        if (length is None):
            length=(((sup_int(l[len(l)-1]+0.0)/n_components))*n_components)
        x_s=np.nonzero(x)
        pl.figure()
        pl.title('Number of non-zero coefficients for each segment')
        n_segm=sup_int((length+0.0)/n_components)
        pl.hist(x_s[0], bins=n_segm, range=(0, n_segm))
        if png:
            pl.savefig(filename, bbox_inches='tight')
            pl.close()
        else:
            pl.show()
        return x_s
        
    def correlation_classification(self, red, l, length=None, numberofmax=1, averaged=10, limit=0.8):
        """
        Determines the correlation classification from the coefficients list red and the index list l.
        Parameters:
            red : dense list of the reduced coefficients
            l : corresponding index list
            length : length of the signal to reconstruct
            numberofmax : number of maxima taken into account for the sum in the calculation of the correlation
            averaged : width of the windows on which the correlation is averaged
            limit : limit determining the classification (1 or -1)
        Returns:
            list of labels (1 or -1) determining if the signal corresponds to the patterns (1) or the dictionary or not (-1)
        """
        from label_classification import correlation
        from label_classification import averaging
        x=self.recover_red(red, l, length=length)
        corr=correlation(x, numberofmax=numberofmax)
        corr=averaging(x, frame=averaged)
        classif=-np.ones(corr.shape)
        classif[corr>=limit]=1
        return classif