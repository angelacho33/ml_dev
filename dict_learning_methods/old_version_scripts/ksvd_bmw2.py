# -*- coding: utf-8 -*-
"""
===========================================
Dictionary learning
===========================================

Train a dictionary on an input datasets and then sparecode an other input dataset with this dictionary.
Steam temperature dataset
"""


##############################################################################
#Importation

import time
from ksvd import KSVD
import numpy as np
import matplotlib.pylab as pl
from sklearn.decomposition import SparseCoder
from tk_gdfr import get_ts_value_from_client_sensor_file #reading routine for alstom datasets
import plot_dictionary as pd
import csv
from dict_analysis import dict_read
import tk_err
import dictionaries_generation as gen
import Fourier_filter as ffl
import tk_plot
import os
import read_bmw

def wavelet_matrix(width, resolution, n_components):
    """Dictionary of selected wavelets"""
    D=gen.sawtooth_matrix(width, resolution, n_components)
    return D
    
    
if __name__ == '__main__':
    
    ##############################################################################
    #Generation of the signal
    print(__doc__)
     
    y_left=[]
    y_right=[]
    
    n_sessions=25
    typeoflane='overland_lanes'
    
    #Generates a signal
    for i in range(n_sessions):        
        filename = "Data/BMW/%s/file_%s.csv"%(typeoflane, i+1) #smallest file             
        y_left.append(read_bmw.leftlane_from_file(filename))            
        y_right.append(read_bmw.rightlane_from_file(filename))            
    
    y_b=np.concatenate(y_right)#testing dataset
    y_1=np.concatenate(y_left) #training dataset
    

    
        
    ##############################################################################
    #Different parameters
    
#    Options
    plot=True
    plot_dict=True
    write=True
    restart=False
    notrainrestart=False
    
    #adjustable parameters
    frame_size=10
    n_components = 10 #number of basic functions in the dictionary
    sparse_tol=np.sqrt(0.02)
    max_thresh=100.0
    min_thresh=0.0
    max_deviation=0.05
    tol_1sparse=max_deviation
    n_nonzero_coefs_sparse=None
    sparsity_parameter=3
    n_iter_ksvd=5000
    
    #Other 
    
    resolution = len(y_1);
    n_segm = pd.sup_int((resolution+0.0)/frame_size); #is the number of segments which decompose the original signal (which is here the concatenation of 2 signals)
    
    if (max_deviation==None):
        max_deviation=np.infty
    #avoid the edges problems
    
    if (resolution%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
        for j in range (frame_size-(resolution%frame_size)):
            y_1=np.concatenate((y_1, [y_1[resolution-1]]))
           
    
    variance_1=(np.mean(y_1**2)-np.mean(y_1)**2) 
    
    
     
    ##############################################################################
    #Printing
           
    print 'Frame size : ', frame_size
    print 'Number of segments : ', n_segm
    print 'Number of components : ', n_components
    print 'Sparse coding Tolerance : ', sparse_tol
    print 'Maximum Threshold (in percentage) : %s' % max_thresh
    print 'Minimum Threshold (in percentage) : %s' % min_thresh
    print 'Allowed testing deviation : %s (omp) & %s (thresholding)' %(tol_1sparse, max_deviation)
    print 'Non zero coefficients allowed during the sparse coding phase: ', n_nonzero_coefs_sparse
    print 'Non zero coefficients allowed during the training phase (ksvd) :', sparsity_parameter
    print 'Number of iterations (ksvd) :', n_iter_ksvd
    
    resolution = len(y_1);          
    t0 = time.time()
    
    ##############################################################################
    #Defines the useful functions/methods
     
    #Filters the signal
    y=y_1
     
    #a list of the different methods we can choose (omp seems to be the best at the moment)
    estimators = [('OMP', 'omp', sparse_tol*sparse_tol*variance_1*resolution/n_segm, n_nonzero_coefs_sparse), 
                  ('LASSO CD', 'lasso_cd', 1., None)]
    
            
            
    h=np.zeros((n_segm, int(len(y)/n_segm))) #will represent the different segments of y, is initialized further
    for l in range(n_segm):
        h[l]=(y[l*resolution/n_segm:(l+1)*resolution/n_segm]) #h represents now the n_segm segments dividing the y signal
    
    
    
    
    ##############################################################################
    #Dictionary initialization step
    
    if (restart):
        #last result
        title, algo, alpha, n_nonzero = estimators[0]  
        filename_2='Dictionaries/ksvd_bmw2_%s_%s_%s.csv' % (typeoflane, n_components, frame_size)
        try:
            D_0=dict_read(filename_2)             
            print 'Restart initialization : ', filename_2
        except IOError as e:
            print "Unable to open file" #Does not exist OR no read permissions
            print 'Initialization : Svd'
            D_0="svd"
            restart=False
            notrainrestart=False
    else:
        #No initialization
        print 'Initialization : None'
        D_0="svd"
    
    ##############################################################################
    
    
    
    
    #Start of the training phase
    if (plot):
        fi1=pl.figure(1, figsize=(13, 6))
        pl.title('Training dataset')
        pl.plot(y_1, ls='dotted', label='Original signal')
    
    #Setting the final parameters
    title, algo, alpha, n_nonzero = estimators[0]   
    print 'KSVD'
    
    if (restart and notrainrestart):
        D=D_0
        coder_0 = SparseCoder(dictionary=D_0, transform_n_nonzero_coefs=n_nonzero,
                            transform_alpha=alpha, transform_algorithm=algo, tol_1=tol_1sparse)
        x=coder_0.transform(h)
        print '-'*40
        print 'Training phase : loaded the previous dictionary'
        print 'Variance : ', variance_1 
        
    else:         
        #Dictionary Learning step
        print '-'*40
        print 'Training phase :'
        print "Resolution : ", resolution
        print 'Variance : ', variance_1 
        D,x=KSVD(h, n_components, sparsity_parameter, n_iter_ksvd, D_init=D_0, enable_threading=True,
                         print_interval = 25)
                                    #On this step is the dictionary learned, and x is then transformed in the new basis
        
                
        
    density = len(np.flatnonzero(x)) #the number of non zero coeffs    
    z = np.ravel(np.dot(x, D)) #x is then the reconstructed signal in the original basis
    rsquared_error = tk_err.rrmse(y, z, norm='std') #RRMSE
    maxmin_error = tk_err.rrmse(y, z, norm='maxmin') 
    
    #Printing
    print 'RRMSE : ', rsquared_error
    print 'Maxmin RRMSE : ', maxmin_error
    print 'Non zero coeffs : ', density
    
    #Ploting
    if (plot):
        pl.plot(z, label='%s: %s nonzero coefs,\n%.2f error'
                    % (title, density, maxmin_error))
        pl.axis('tight')
        pl.legend()
    
        
    dt = time.time() - t0
    print('done in %.2fs.' % dt)
    #plots the original and reconstructed signal
    if (plot):
        pl.subplots_adjust(.04, .07, .97, .90, .09, .2)
    
    
    ###############################################################################
    # Plot the generated dictionary
    if (plot_dict):
        fi2=pl.figure(2, figsize=(4.2, 4))
        fi2.suptitle("Generated dictionary", fontsize=18)
        pd.plot_dic(D)
    
    ###############################################################################
    
    ##############################################################################
    #Generates the signal to test
    
    t1=time.time()
    
    
    resolution = len(y_b);
    n_segm=pd.sup_int((resolution+0.0)/frame_size); #is the number of segments which decompose the original signal
    if (resolution%frame_size>0):
        for j in range (frame_size-(resolution%frame_size)):
            y_b=np.concatenate((y_b, [y_b[resolution-1]]))
            
    resolution = len(y_b); 
    
    h_b=np.zeros((n_segm, frame_size))
    
    
    
    for l in range(n_segm):
        h_b[l]=(y_b[l*frame_size:(l+1)*frame_size])
    
    
    variance_b=(np.mean(y_b**2)-np.mean(y_b)**2)
    
    ##############################################################################
    #Start of the testing phase
    
    #Defines the method
    coder = SparseCoder(dictionary=D, transform_n_nonzero_coefs=n_nonzero,
                        transform_alpha=sparse_tol*sparse_tol*variance_b*resolution/n_segm, transform_algorithm=algo, tol_1=tol_1sparse)
                                            
     
    print '-'*40
    print 'Testing phase :'        
    print 'Variance : ', variance_b
    
    if (plot):
        fi3=pl.figure(3, figsize=(13, 6))
        pl.title('Testing dataset')
        print "Resolution : ", resolution
        pl.plot(y_b, ls='dotted', label='Original signal')
    
    
    #on this line, x is the determined to minimize ||h-Dx|| with the condition that
    #every array of x has less than n_z non zero coefficients (for the omp method)
    t2=time.time()
    x_1=coder.transform(h_b) 
    
    #Thresholding phase
    t3 = time.time()
    #Intialize
    maxi=np.max(np.abs(x_1))
    z_1=np.ravel(np.dot(x_1,D))
    rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
    deviation=np.max(np.abs(y_b-z_1))
    thresh=min_thresh
    x_2=np.copy(x_1)
    step=1.0
    #Find the right threshold to set
    while ((rsquared_error<=100*sparse_tol) and (thresh < max_thresh) and deviation<=max_deviation):
        thresh+=step
        x_2[np.abs(x_2)<thresh/100*maxi]=0
        z_2=np.ravel(np.dot(x_2,D))
        rsquared_error = tk_err.rrmse(y_b, z_2, norm='std') #RMSE
        deviation=np.max(np.abs(y_b-z_2))
    
    while ((rsquared_error>100*sparse_tol or deviation>max_deviation) and (thresh>0) ):
        x_2=np.copy(x_1)
        thresh-=step
        x_2[np.abs(x_2)<thresh/100*maxi]=0
        z_2=np.ravel(np.dot(x_2,D))
        rsquared_error = tk_err.rrmse(y_b, z_2, norm='std')
        deviation=np.max(np.abs(y_b-z_2))
        
    #Set the low coefficients to 0 with the right threshold
    x_1[np.abs(x_1)<thresh/100*maxi]=0
    
    t4=time.time()-t3 
    t5=time.time()-t2
    print('Thresholding phase done in %.2fs.' % t4)        
    print ('Sparsecoding phase done in %.2fs.' % t5)
    
    ##############################################################################
        
    
    #Analyses the results
    density=len(np.flatnonzero(x_1)) #number of non zero coefficients
    z_1=np.ravel(np.dot(x_1,D))
    rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
    maxmin_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
    print 'Actual threshold (in percentage) : ', thresh    
    print 'RRMSE : ', rsquared_error
    print 'Maxmin_RRMSE : ', maxmin_error
    print 'Infinity norm : ', np.max(np.abs(y_b-z_1))
    print 'Non zero coeffs : ', density
    print 'Compression ratio : ', 100*(1-(density+0.0)/resolution)
    
    

    #plots the original and reconstructed signal
    if (plot):
        pl.plot(z_1, label='%s: %.1f reduction ratio,\n%.2f error'
                    % (title, 100*(1-(density+0.0)/resolution), maxmin_error))
           
##############################################################################

       
    y_d=y_b
    z_dict=np.copy(z_1)           
    
###################################################
                    
    if (plot):                
        pl.axis('tight')
        pl.legend()
        
    dt = time.time() - t1
    print('done in %.2fs.' % dt)
    
    ###############################################################################
    
    
    #Writes the dictionary in a csv file
    if (write):
        folder_write='Dictionaries'
        file_path = folder_write+"/ksvd_bmw2_%s_%s_%s.csv" %(typeoflane, n_components, frame_size)
        if not os.path.exists(folder_write):
           os.makedirs(folder_write)#
        csvfile = csv.writer(open(file_path, "wb"), delimiter=';')
        for i in range(len(D[0])):
            csvfile.writerow((D[:,i]))
    
        folder_write='Reconstructed signals'
        file_path = folder_write+"/reconstructed_signal_ksvd_bmw2_%s_%s_%s.csv" %(typeoflane, n_components, frame_size)
        if not os.path.exists(folder_write):
           os.makedirs(folder_write)#
        csvfile = csv.writer(open(file_path, "wb"), delimiter=';')
        for i in range(len(z_dict)):
            csvfile.writerow(['%.2f' % z_dict[i]])
    
    ###############################################################################
    
    
    ###############################################################################
    #Plot the initialization dictionary
    if (plot_dict):
        if D_0 is not "svd":
            fi4=pl.figure(4, figsize=(4.2, 4))
            fi4.suptitle('Initialization dictionary', fontsize=18)
            pd.plot_dic(D_0)
    ###############################################################################
    
    ###############################################################################
    #Plot the representation of the initial signal in the new basis
    if (plot_dict):
        fi5=pl.figure(5, figsize=(4.2, 4))
        fi5.suptitle("Coefficients of the signal in the new basis", fontsize=18)
        pd.plot_dic(x_1.T)
    
    ###############################################################################
    
    ###############################################################################
    #Plot an histogram of the appearance of the basic functions
    if (plot):
        x_s=np.nonzero(x_1)
        fi6=pl.figure(6)
        pl.title('Number of appearances of each basic function')
        pl.hist(x_s[1], bins=n_components+1, range=(0, n_components))
        fi7=pl.figure(7)
        pl.title('Number of non-zero coefficients for each segment')
        pl.hist(x_s[0], bins=n_segm, range=(0, n_segm))
        ################################################################################
        fi8=pl.figure(8)
        pl.title('Boxplot of the deviation')
        ax = fi8.add_subplot(111)
        tk_plot.plot_boxplot_residuals(ax, y_d,z_dict)
    
    
    
    if (plot):
        pl.show()

