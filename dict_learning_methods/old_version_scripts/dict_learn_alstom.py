# -*- coding: utf-8 -*-


"""
===========================================
Dictionary learning
===========================================

Train a dictionary on an input datasets and then sparecode an other input dataset with this dictionary.
Alstom Dataset
"""


##############################################################################
#Importation

import time
import os
import numpy as np
from sklearn.decomposition import SparseCoder
from sklearn.decomposition import DictionaryLearning
import plot_dictionary as pd
import csv
from dict_analysis import dict_read
import Fourier_filter as ffl
import dictionaries_generation as gen
from tools import tk_err
import tk_gdfr
from tk_databank import DataBank
from tk_plot import Plot
from compression import compr_float
from ksvd import KSVD
import glob
    
if __name__ == '__main__':
    
    ##############################################################################
    #Generation of the signal
    print(__doc__)
    
    Dat = DataBank()  #call it Dat because D represents the Dictionary in what follows
    P = Plot(Dat)
    
    y_l=[]

    trainsensor='test'
    selectedsensor='test'
    numberofsensors= np.arange(1,20)
    typeofnetwork='network_1_713'
    
#    sensor_name = str(selectedsensor)


    #    Options
    plot=True
    write=True
    restart=True
    notrainrestart=True
    comparewithdct=False
    svd_init=True
    
    

    folder = './Data/AlstomData/%s' % typeofnetwork


    filenamelist_alstom = []
    for i in numberofsensors:
        filenamelist_alstom.append(folder+('/Alstom_Data_X_%s.csv'%i))

    

    ##Generates a signal

    #for i in range(n_sessions):        

    #    filename =  filenamelist_lane[i] #smallest file             

    #    y_left.append(read_bmw.leftlane_from_file(filename))            

    #    y_right.append(read_bmw.rightlane_from_file(filename))            

    

    #y_b=np.concatenate(y_right)#testing dataset

    #y_1=np.concatenate(y_left) #training dataset

    



    #data_tmp = np.loadtxt('../Audi_Sensor835.csv',delimiter=';')#,names=['tss','value'])#,index_col='t'

    

    

#    file = filenamelist_alstom[selectedsensor-1]

    file='./Data/AlstomData/2016-04-21 12_56_53_raw_data_4_test_dataset.csv'
    print 'File : ', file
    

    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ';', 'pos', 'pos', 0, [1])

    

    value_names = 'x'

    

    sensor_id=0

    ##Add data to a databank
    ts = [x * (1./3200) for x in range(12799)]
    S1 = Dat.add_sensor_data(sensor_data1['pos']['values'][1:],

                           ts, sensor_id, "velocity", "pos",value_names=value_names)

					  

    Dat.sensor_raw_data_clean(sensor_id, 1./3200)

    

    #D.write_sensor_data_files(sensor_id)

    

    

    ####################   Interpolation Daniel implemented

    #Raw data info

    raw_data=Dat.get_sensor_raw_data(sensor_id)

    tss=np.asarray(raw_data['tss'])

    values=np.asarray(raw_data['values'])

    npoints=raw_data['count']

    

    

    data_tmp = values

    #print data_tmp

    data_tmp = [x[0] for x in data_tmp]


    data_tmp = np.asarray(data_tmp)

    

    #data_tmp = data_tmp[0:,1]#.tolist()



    #data_tmp = []

    #for row in data.value:

    #    data_tmp.append(row)

    ##    data_codename_all.append(row)

    ##        if row not in CodeNames_All:

    ##           CodeNames_All.append(row)

    

    print "This is the dataset length",len(data_tmp)

    

    y_1 = data_tmp[0:len(data_tmp)/2]

    y_b = data_tmp


     
    
    len_recon=len(y_b)
    maxmin_1=(np.max(y_1)-np.min(y_1))   
    maxmin_b=(np.max(y_b)-np.min(y_b))      
     
    ##############################################################################
    #Different parameters
    

    #adjustable parameters
    frame_size=50
    compare_frame=200
    n_components = frame_size #number of basic functions in the dictionary
    tol=0.03
    sparse_tol=0.03
    sparsity_controlling_parameter=1.
    num_error=-1
    max_thresh=10.0
    min_thresh=7.0
    max_deviation=None
    tol_1train=None
    tol_1sparse=max_deviation
    fit_algo = 'omp'
    n_iter=50
    n_nonzero_coefs_dict=None
    n_nonzero_coefs_sparse=None
    factor=5   
    numberofdigits=0
    
    #Other parameters
    resolution = len(y_1);
    n_segm = pd.sup_int((resolution+0.0)/frame_size); #is the number of segments which decompose the original signal (which is here the concatenation of 2 signals)
    
    if (max_deviation==None):
        max_deviation=np.infty
    
    #avoid the edges problems
    
    if (resolution%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
        for j in range (frame_size-(resolution%frame_size)):
            y_1=np.concatenate((y_1, [y_1[resolution-1]]))
           
    
    variance_1=(np.mean(y_1**2)-np.mean(y_1)**2) 
    
    
     
    ##############################################################################
    #Printing
           
    print 'Frame size : ', frame_size
    print 'Number of segments : ', n_segm
    print 'Number of components : ', n_components
    print 'Training Tolerance : ', tol  #maximal authorized RMSE 
    print 'Sparse coding Tolerance : ', sparse_tol
    print 'Sparsity controlling parameter : ', sparsity_controlling_parameter
    print 'Numerical Error Tolerance : ', num_error
    print 'Maximal number of iterations : ', n_iter
    print 'Maximum Threshold (in percentage) : %s' % max_thresh
    print 'Minimum Threshold (in percentage) : %s' % min_thresh
    print 'Allowed testing deviation : %s (omp) & %s (thresholding)' %(tol_1sparse, max_deviation)
    print 'Allowed training deviation : %s' %(tol_1train)
    print 'Non zero coefficients allowed during the training phase : ', n_nonzero_coefs_dict
    print 'Non zero coefficients allowed during the sparse coding phase: ', n_nonzero_coefs_sparse
   
    
    resolution = len(y_1);          
    t0 = time.time()
    
    ##############################################################################
    #Defines the useful functions/methods
     
    #Filters the signal
    y=y_1
     
    #a list of the different methods we can choose (omp seems to be the best at the moment)
    estimators = [('OMP', 'omp', tol*tol*maxmin_1*maxmin_1*frame_size, n_nonzero_coefs_sparse), ('LASSO_Lars', 'lasso_lars', 0.5, None),
                  ('LASSO_cd', 'lasso_cd', 0.5, None), ('Threshold', 'threshold', 5, None),
                  ('Lars', 'lars', 0.5, None)]
    
            
            
    h=np.zeros((n_segm, int(len(y)/n_segm))) #will represent the different segments of y, is initialized further
    for l in range(n_segm):
        h[l]=(y[l*resolution/n_segm:(l+1)*resolution/n_segm]) #h represents now the n_segm segments dividing the y signal
    
    
    
    
    
    
    ##############################################################################
    #Dictionary initialization step
    
    if (restart):
        #last result
        title, algo, alpha, n_nonzero = estimators[0]  
        filename_2='Dictionaries/dictionary_alstom_%s_%s_%s_%s_%s.csv' % (trainsensor, fit_algo, n_components, frame_size, typeofnetwork)
        try:
            D_0=dict_read(filename_2)
            coder_0 = SparseCoder(dictionary=D_0, transform_n_nonzero_coefs=n_nonzero,
                            transform_alpha=alpha, transform_algorithm=algo, tol_1=tol_1train)
            c_0=coder_0.transform(h) 
            print 'Restart initialization : ', filename_2
        except IOError as e:
            print "Unable to open file" #Does not exist OR no read permissions
            print 'Initialization : None'
            D_0=None
            c_0=None  
            restart=False
            notrainrestart=False
    elif(svd_init):
        print 'Initialization : svd'
        D_0,c_0=KSVD(h, n_components, frame_size/3, 5, enable_threading=False)
    else:
        #No initialization
        print 'Initialization : None'
        D_0=None
        c_0=None
    
    ##############################################################################
    
    
    
    
    #Start of the training phase
    
    #Setting the final parameters
    title, algo, alpha, n_nonzero = estimators[0]   
    
    
    print 'Fit algorithm : ', fit_algo   
    print title 
    
    if (restart and notrainrestart):
        D=D_0
        x=c_0
        print '-'*40
        print 'Training phase : loaded the previous dictionary'
        print 'Variance : ', variance_1 
        
    else:         
        #Dictionary Learning step
        print '-'*40
        print 'Training phase :'
        print "Resolution : ", resolution
        print 'Variance : ', variance_1 
        dictio = DictionaryLearning(n_components=n_components, alpha=sparsity_controlling_parameter*np.sqrt(variance_1), 
                                    max_iter=n_iter, tol=num_error,
                                    fit_algorithm=fit_algo, transform_algorithm=algo, transform_n_nonzero_coefs=n_nonzero, transform_alpha=alpha, n_jobs=1, 
                                    code_init=c_0, dict_init=D_0, verbose=False, 
                                    split_sign=False, random_state=None, tol_2=alpha*factor, tol_1=tol_1train, n_nonzero_coefs=n_nonzero_coefs_dict)
                                    
                                    
        #On this step is the dictionary learned, and x is then transformed in the new basis
        x=dictio.fit_transform(h)
                
        #Analyses the results        
        D=dictio.components_ #here is the final dictionary
        
        fit_algo='omp'
        
        dictio = DictionaryLearning(n_components=n_components, alpha=sparsity_controlling_parameter*np.sqrt(variance_1), 
                                    max_iter=n_iter, tol=num_error,
                                    fit_algorithm=fit_algo, transform_algorithm=algo, transform_n_nonzero_coefs=n_nonzero, transform_alpha=alpha, n_jobs=1, 
                                    code_init=x, dict_init=D, verbose=False, 
                                    split_sign=False, random_state=None, tol_2=alpha, tol_1=tol_1train)
                                    
                                    
        #On this step is the dictionary learned, and x is then transformed in the new basis
        x=dictio.fit_transform(h)
                
        #Analyses the results        
        D=dictio.components_ #here is the final dictionary        
        
    density = len(np.flatnonzero(x)) #the number of non zero coeffs    
    z = np.ravel(np.dot(x, D)) #x is then the reconstructed signal in the original basis
    rsquared_error = tk_err.rrmse(y, z, norm='std') #RRMSE
    maxmin_error = tk_err.rrmse(y, z, norm='maxmin') 
    
    #Printing
    print 'RRMSE : ', rsquared_error
    print 'Maxmin RRMSE : ', maxmin_error
    print 'Non zero coeffs : ', density
    
  
    
        
    dt = time.time() - t0
    print('done in %.2fs.' % dt)
   
    
    ###############################################################################
 
    
    ###############################################################################
    
    ##############################################################################
    #Generates the signal to test
    
    t1=time.time()
    
    if (compare_frame!=frame_size):
        y_0=np.copy(y_b)
    
    resolution = len(y_b);
    n_segm=pd.sup_int((resolution+0.0)/frame_size); #is the number of segments which decompose the original signal
    if (resolution%frame_size>0):
        for j in range (frame_size-(resolution%frame_size)):
            y_b=np.concatenate((y_b, [y_b[resolution-1]]))
            
    resolution = len(y_b); 
    
    h_b=np.zeros((n_segm, frame_size))
    h=np.zeros((n_segm, frame_size))
    
    
    
    for l in range(n_segm):
        h_b[l]=(y_b[l*frame_size:(l+1)*frame_size])
    
    
    variance_b=(np.mean(y_b**2)-np.mean(y_b)**2)
    
    ##############################################################################
    #Start of the testing phase
    
    #Defines the method
    coder = SparseCoder(dictionary=D, transform_n_nonzero_coefs=n_nonzero,
                        transform_alpha=sparse_tol*sparse_tol*maxmin_b*maxmin_b*frame_size, transform_algorithm=algo, tol_1=tol_1sparse)
                                            
     
    print '-'*40
    print 'Testing phase :'        
    print 'Variance : ', variance_b
    
  
    
    #on this line, x is the determined to minimize ||h-Dx|| with the condition that
    #every array of x has less than n_z non zero coefficients (for the omp method)
    t2=time.time()
    x_1=coder.transform(h_b) 
    
    #Thresholding phase
    
    #Intialize
    maxi=np.max(np.abs(x_1))
    z_2=np.ravel(np.dot(x_1,D))
    rsquared_error = tk_err.rrmse(y_b, z_2, norm='std') #RMSE
    if (max_deviation<np.infty):
        deviation=np.max(np.abs(y_b-z_2))
    else:
        deviation=0
    thresh=min_thresh
    x_2=np.copy(x_1)
    step=1.0
    t3 = time.time()
    #Find the right threshold to set
    while ((rsquared_error<=100*sparse_tol) and (thresh < max_thresh) and deviation<=max_deviation):
        thresh+=step
        x_2[(np.abs(x_2)<thresh/100*maxi)]=0
        z_2=np.ravel(np.dot(x_2,D))
        rsquared_error = tk_err.rrmse(y_b, z_2, norm='std') #RMSE
        if (max_deviation<np.infty):
            deviation=np.max(np.abs(y_b-z_2))

    
    while ((rsquared_error>100*sparse_tol or deviation>max_deviation) and (thresh>0) ):
        x_2=np.copy(x_1)
        thresh-=step
        x_2[(np.abs(x_2)<thresh/100*maxi)]=0
        z_2=np.ravel(np.dot(x_2,D))
        rsquared_error = tk_err.rrmse(y_b, z_2, norm='std')
        if (max_deviation<np.infty):
            deviation=np.max(np.abs(y_b-z_2))
        
        
    #Set the low coefficients to 0 with the right threshold
    x_1[np.abs(x_1)<thresh/100*maxi]=0
    
    for i in range(len(x_1)):
        for j in range(len(x_1[i])):
            x_1[i,j]=compr_float(x_1[i,j], numberofdigits)
    t4=time.time()-t3 
    t5=(t4+t3-t2)
    print('Thresholding phase done in %.2fs.' % t4)
    print ('Sparseconding phase done in %.2fs.' % t5)
    
    ##############################################################################
        
    
    #Analyses the results
    density=len(np.flatnonzero(x_1)) #number of non zero coefficients
    z_1=np.ravel(np.dot(x_1,D))
    rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
    maxmin_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
    print 'Actual threshold (in percentage) : ', thresh
    print 'RRMSE : ', rsquared_error
    print 'Maxmin_RRMSE : ', maxmin_error
    print 'Infinity norm : ', np.max(np.abs(y_b-z_1))
    print 'Non zero coeffs : ', density
    print 'Compression ratio : ', 100*(1-(density+0.0)/resolution)
    
       
    recon=z_1.reshape(len(z_1), 1)
    recon=recon[:len_recon]
    
    print 'This is the length of the reconstructed signal : ', len(recon)
    x_2=np.ravel(x_1)
    #times_stamps=np.concatenate((times_stamps, np.zeros(len(x_1)-len(times_stamps))))
    
    l_1=np.flatnonzero(x_2)
    x_2=x_2[l_1]
    
    for i in range(len(l_1)-1):
        l_1[len(l_1)-i-1]-=l_1[len(l_1)-i-2]
   
  
    l_1=l_1.reshape(len(l_1), 1)
    x_2=x_2.reshape(len(x_2), 1)
      
    red=np.concatenate((x_2, l_1), axis=1)
    if (write):
        folder_write='Reduced_Data'
        file_path = folder_write+'/reduced_alstom_%s_%s_%s_%s_%s.csv' %(selectedsensor, fit_algo, n_components, frame_size, typeofnetwork)
        if not os.path.exists(folder_write):
           os.makedirs(folder_write)#
        with open(file_path, "wb") as f:
            csvfile = csv.writer(f, delimiter=';')
            for i in range(len(red)):
                csvfile.writerow([('%.'+str(numberofdigits)+'f') % red[i,0], '%.0f' % red[i,1]])
        f.close()
    
#   Adding the reconstructed and reduced datas to the databank
    process_id=Dat.add_processed_sensor_data(0, recon, tss, datatype='recon_data', meta={})
   
    
#    Dat.add_processed_red_data(0, file_path)

        
##############################################################################

 #Writes the dictionary in a csv file
    
    
    if (write and not (notrainrestart)):
        folder_write='Dictionaries'
        file_path = folder_write+"/dictionary_alstom_%s_%s_%s_%s_%s.csv" %(trainsensor, fit_algo, n_components, frame_size, typeofnetwork)
        if not os.path.exists(folder_write):
           os.makedirs(folder_write)#
        with open(file_path, "wb") as f_c:   
            csvfile = csv.writer(f_c, delimiter=';')
            for i in range(len(D[0])):
                csvfile.writerow((D[:,i]))
        f_c.close()
        
    Dat.save("./Saved_Databank/Alstom_Test_%s_%s.dbk" %(typeofnetwork, selectedsensor))   
    
    if (plot):
        P.plot(0, process_id=process_id, deviation='difference', save_png=True)
   
    
    ###############################################################################
    
       
       
    if (comparewithdct):
        
        
        if (compare_frame!=frame_size):
            frame_size=compare_frame
            y_b=y_0
            resolution = len(y_b);
            n_segm=pd.sup_int((resolution+0.0)/frame_size); #is the number of segments which decompose the original signal
            if (resolution%frame_size>0):
                for j in range (frame_size-(resolution%frame_size)):
                    y_b=np.concatenate((y_b, [y_b[resolution-1]]))
                    
            resolution = len(y_b); 
            
            h_b=np.zeros((n_segm, frame_size))
            h=np.zeros((n_segm, frame_size))
            
            
            
            for l in range(n_segm):
                h_b[l]=(y_b[l*frame_size:(l+1)*frame_size])
            
            
            variance_b=(np.mean(y_b**2)-np.mean(y_b)**2)
            
        #Fixed dictionaries
    
        #DCT
        D_dct = np.array(gen.dct_iii(np.identity(frame_size)))
        
      
          
        print '-'*40
        print 'Comparison'
        D_1=D_dct
        title='DCT'
        coder = SparseCoder(dictionary=D_1, transform_n_nonzero_coefs=n_nonzero,
            transform_alpha=sparse_tol*sparse_tol*variance_b*resolution/n_segm, transform_algorithm=algo, tol_1=tol_1sparse)
        print '%s : ' %title
        print '%s x %s' %(len(D_1), len(D_1[0]))
        x_3=coder.transform(h_b)   

        
        maxi=np.max(np.abs(x_3))
        z_1=np.ravel(np.dot(x_3,D_1))
        rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
        deviation=np.max(np.abs(y_b-z_1))
        thresh=min_thresh
        x_2=np.copy(x_3)
        step=1.0
        
        #Find the right threshold to set
        while ((rsquared_error<=100*sparse_tol) and (thresh < max_thresh) and deviation<=max_deviation):
            thresh+=step
            x_2[np.abs(x_2)<thresh/100*maxi]=0
            z_2=np.ravel(np.dot(x_2,D_1))
            rsquared_error = tk_err.rrmse(y_b, z_2, norm='std') #RMSE
            deviation=np.max(np.abs(y_b-z_2))
        
        while ((rsquared_error>100*sparse_tol or deviation>max_deviation) and (thresh>0) ):
            x_2=np.copy(x_3)
            thresh-=step
            x_2[np.abs(x_2)<thresh/100*maxi]=0
            z_2=np.ravel(np.dot(x_2,D_1))
            rsquared_error = tk_err.rrmse(y_b, z_2, norm='std')
            deviation=np.max(np.abs(y_b-z_2))
    
            
        #Set the low coefficients to 0 with the right threshold
        x_3[np.abs(x_3)<thresh/100*maxi]=0    
            
            #Analyses the results
        density=len(np.flatnonzero(x_3)) #number of non zero coefficients
        z_1=np.ravel(np.dot(x_3,D_1))
        rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
        maxmin_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
        print 'Actual threshold (in percentage) : ', thresh
        print 'RRMSE : ', rsquared_error
        print 'Maxmin_RRMSE : ', maxmin_error
        print 'Infinity norm : ', np.max(np.abs(y_b-z_1))
        print 'Non zero coeffs : ', density
        print 'Compression ratio : ', 100*(1-(density+0.0)/resolution)
        print '-'*40

