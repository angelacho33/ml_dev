# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 16:05:25 2016

@author: Etienne
"""

import matplotlib.pylab as pl
import numpy as np

def sup_int(n):
    if (int(n)==n):
        return int(n)
    else :
        return int(n)+1

def plot_dic(D):
    n_components=int(D.shape[0])
    for i in range(n_components):
        pl.subplot(sup_int(np.sqrt(n_components)), sup_int(np.sqrt(n_components)), i + 1)
        pl.plot(D[i])
        
        