# -*- coding: utf-8 -*-
"""
Filters a signal with the Fourier decomposition
"""
import numpy as np
from tk_gdfr import get_ts_value_from_client_sensor_file #reading routine for alstom datasets
import matplotlib.pylab as pl
import scipy.optimize as opt
import read_airbus_sar_data as read  #reading routine for airbus datasets


def filter_rule(x,freq, band, f_signal):
    if abs(freq)>f_signal+band or abs(freq)<f_signal-band:
        return 0
    else:
        return x

def filtered_signal(f, band):
    F=np.fft.fft(f-np.mean(f))
    freq=np.fft.fftfreq(len(F))
    f_signal=np.abs(freq[np.argmax(np.abs(F-F[0]))])
    F_filtered = np.array([filter_rule(x,frequ, band, f_signal) for x,frequ in zip(F,freq)])
    s_filtered = np.real(np.fft.ifft(F_filtered) + np.mean(f))
    return s_filtered
    

    
def gauss(x, p): # p[0]==mean, p[1]==stdev
    return 1.0/(p[1]*np.sqrt(2*np.pi))*np.exp(-(x-p[0])**2/(2*p[1]**2))    
    
def fwhm(X,Y):
    # Fit a gaussian
    p0 = [0,1] # Inital guess is a normal distribution
    errfunc = lambda p, x, y: gauss(x, p) - y # Distance to the target function
    p1, success = opt.leastsq(errfunc, p0[:], args=(X, Y))

    fit_mu, fit_stdev = p1

    return([fit_mu, 2*np.sqrt(2*np.log(2))*fit_stdev])
    
    
def fr_siz1(y_b): #determines the frame size to set with the fwhm function
    freq=np.fft.fftfreq(len(y_b))
    y_b-=np.mean(y_b)
    y_b/=np.sqrt(np.mean(y_b**2))   
    y_b=filtered_signal(y_b, 0.05)
    y_c=np.abs(np.real(np.fft.fft(y_b)))
    y_c/=np.mean(y_c)
    FWHM=fwhm(freq, y_c)[1]/2
    return(np.int(1./FWHM))


def inv(perm): #inverse a permutation
    inverse = np.zeros(len(perm), dtype=int)
    for i in range(len(perm)):
        inverse[perm[i]]=i
    return inverse


    
def max_local(x, h):
    y=np.zeros(len(x))
    for i in range(len(x)-h):
        if (x[i+h/2]==np.max(x[i:i+h])):
            y[i+h/2]=x[i+h/2];
    return y

def enveloppe(y):
    y_c=np.copy(y)
    nz=np.flatnonzero(y_c)
    for i in range(len(nz)-1):
        y_c[nz[i]:nz[i+1]]=np.linspace(y_c[nz[i]],y_c[nz[i+1]], nz[i+1]-nz[i])
    return y_c
    
def fr_siz2(y_1): #frame size based on the main frequency
    y_1=map(float, y_1)
    freq=np.fft.fftfreq(len(y_1))
    step=len(y_1)/500
    y_1-=np.mean(y_1)
    y_1/=np.sqrt(np.mean(y_1**2))   
    y_1=filtered_signal(y_1, 0.05)
    y_c=np.abs(np.real(np.fft.fft(y_1)))
    maxi=np.max(np.abs(y_c))
    y_c[np.abs(y_c)<1./100*maxi]=0
    x=np.zeros(len(freq), dtype=int)
    for i in range(len(x)):
        x[i]=np.round((freq[i]+np.abs(np.min(freq)))*len(freq))
    y_c=y_c[x]
    y_c=max_local(y_c, step)
    y_c=enveloppe(y_c)
    y_c=y_c[inv(x)]
    y_c/=np.mean(y_c)
    FWHM=fwhm(freq, y_c)[1]/2
    return(np.int(1./FWHM))
    
def bary(x,y):
    return (np.sum(y*x)/np.sum(y))
    
        
def fr_siz3(y, thresh): #based on the barycentre of the frequencies over the threshold
    freq=np.fft.fftfreq(len(y))
    x=freq[:len(y)/2]
    y-=np.mean(y)
    y/=np.sqrt(np.mean(y**2)) 
    F=np.abs(np.real(np.fft.fft(y))[:len(y)/2])
    maxi=np.max(np.abs(F))
    F[np.abs(F)<thresh*1./100*maxi]=0
    return(np.int(1./bary(x, F)))
    
def fr_siz4(y, thresh): #based on the average of the frequencies over the threshold
    freq=np.fft.fftfreq(len(y))
    x=freq[:len(y)/2]
    y-=np.mean(y)
    y/=np.sqrt(np.mean(y**2)) 
    F=np.abs(np.real(np.fft.fft(y))[:len(y)/2])
    maxi=np.max(np.abs(F))
    x[np.abs(F)<thresh*1./100*maxi]=0
    return(np.int(1./np.mean(x)))
    
def filter_test():    
#Generates a signal
    filename = "Data/BMW/velocity_data.csv" #smallest file
    sensor_name = 'sin'
    H = get_ts_value_from_client_sensor_file(filename, ';', 'cid:0', 
                                             sensor_name, 0, [1])
    values = H['cid:0'][sensor_name]['values']
    y=np.concatenate(values)
    y_b=y
    #y_1=y[0:len(y)/2]        
    #y=y[0:len(y)/2]
    freq=np.fft.fftfreq(len(y_b))
    y_b-=np.mean(y_b)
    y_b/=np.sqrt(np.mean(y_b**2))
    
    band = 0.02
    s=filtered_signal(y_b, band)
    rmse = np.mean((s-y_b)**2)
    print 'RMSE : ', rmse
    fi1=pl.figure(1)
    pl.plot(y_b, ls='dotted', label='Original signal')
    pl.plot(s, label='Filtered signal, RMSE = %s' %rmse)
    pl.legend()
    
    F=(np.fft.fft(y_b)) 
    F_s=(np.fft.fft(s)) 
    freq=np.fft.fftfreq(len(y_b))
    pl.figure()
    pl.plot(freq, F, label='Original Spectrum')
    pl.plot(freq, F_s, label='Filtered Spectrum')
    
    pl.show()

def frame_test():    
#Generates a signal
    filename = "Data/selected_sensors/sid_3_ASMod_dvolPFltEG.csv" #smallest file
    sensor_name = 'sin'
    H = get_ts_value_from_client_sensor_file(filename, ';', 'cid:0', 
                                             sensor_name, 0, [0])
    values = H['cid:0'][sensor_name]['values']
    y=np.concatenate(values)
    
    frame=fr_siz2(y)
    frequ=(1./frame)
    print 'Frequence : ', frequ
    F=np.real(np.fft.fft(y))
    freq=np.fft.fftfreq(len(y))
    pl.figure()
    pl.plot(freq, F, label='Fourier Spectrum')
    pl.vlines(frequ, np.min(np.real(F)), np.max(np.real(F)))
    pl.legend()
    pl.show()
    
def test():
    y_l=[]

    
    n_sessions=19
    typeofnetwork='network_1_713'
    sensor_name = 'sin'
    
    #Generates a signal
    for i in range(n_sessions):        
        filename = "Data/AlstomData/%s/Alstom_Data_X_%s.csv"%(typeofnetwork, i+1) #smallest file             
        H = get_ts_value_from_client_sensor_file(filename, ';', 'cid:0', 
                                             sensor_name, 0, [0])                                        
        values = H['cid:0'][sensor_name]['values']
        y_l.append(np.concatenate(values))            
    
    y=np.concatenate(y_l)   
    y_1=y[0:len(y)/2]
    
    pl.figure()
    pl.title('Training dataset')
    
    y_1-=np.mean(y_1)
    y_1/=np.sqrt(np.mean(y_1**2))
    pl.plot(y_1, label='Original Signal')
    pl.figure()
    pl.title('Filtered Training dataset')
    y_1=filtered_signal(y_1, 0.05)
    pl.plot(y_1, label='Filtered Signal') 
    
    y_1=map(float, y_1)
    freq=np.fft.fftfreq(len(y_1))
    step=len(y_1)/500

    y_c=np.abs(np.real(np.fft.fft(y_1)))
    
    pl.figure()
    pl.title('Fourier Spectrum in absolute values')
    pl.plot(freq, y_c)
    
    maxi=np.max(np.abs(y_c))
    y_c[np.abs(y_c)<1./100*maxi]=0
    x=np.zeros(len(freq), dtype=int)
    for i in range(len(x)):
        x[i]=np.round((freq[i]+np.abs(np.min(freq)))*len(freq))
    y_c=y_c[x]
    y_c=max_local(y_c, step)
    y_c=enveloppe(y_c)
    y_c=y_c[inv(x)]
    
    pl.figure()
    pl.title('Approximation of the enveloppe')
    pl.plot(freq, y_c)    
    
    y_c/=np.mean(y_c)
    FWHM=fwhm(freq, y_c)[1]/2
    
        # Fit a gaussian
    p0 = [0,1] # Inital guess is a normal distribution
    errfunc = lambda p, x, y: gauss(x, p) - y # Distance to the target function
    p1, success = opt.leastsq(errfunc, p0[:], args=(freq, y_c))
    fit_mu, fit_stdev = p1
    
    pl.figure()
    pl.title('FWHM')
    pl.plot(freq, y_c, ls='dotted', label='Approximation of the enveloppe')
    pl.plot(freq, gauss(freq, [fit_mu, fit_stdev]), label='Gaussian approximation')
    pl.vlines(FWHM, 0, np.max(y_c))
    pl.legend()
    pl.show()
if __name__ == '__main__':
        
   filter_test()