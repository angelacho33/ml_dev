"""
Project Teraki Data Process APIs
Author: qingzhou

it provides the APIs for raw_data file, tka data file, and other files.
"""

import os, sys, time

def str_to_int_float(string): 
    try:
        return int(string)
    except ValueError:
        pass
    try:
        return float(string)
    except ValueError:
        return string

def get_ts_value_from_client_sensor_file(filename, sep, client, sensor, pos_ts, pos_value, pos_para=[]):
    """get timestamp and values from a general file, with one or multiple client and sensors.
    
    Args:
        filename: the file to read
        sep: the seperation character
        client: the position of client_name, set to string if do not read from file
        sensor: the position of sensor_name, set to string if do not read from file
        pos_ts: the position of timestamp
        pos_value: the position of values
        pos_para: the position of to parameters to read
    
    Returns:
        Dictionary with tss and values under sensor_name key, as following format:
        {
            client_name : {
                sensor_name: {
                    "tss":   <tss_read>,
                    "values": <values_read>,
                    "para": <parameter_read>
                }
            }
        }

    Example1 data format:  <ts>,<lat>,<long>
        >>> sensor_data = get_ts_value_from_client_sensor_file(file, ',', 'client', 'sensor_name', 0, [1, 2])
        >>> tss = sensor_data['sensor_name']['tss']
        >>> values = sensor_data['sensor_name']['values']

    Example2 data format:  <ts>,<senor_name>,<lat>,<long>
        >>> sensor_data = get_ts_value_from_client_sensor_file(file, ',', 'client', 1, 0, [2, 3])
        >>> sensor_name = sensor_data.keys()
        >>> tss = sensor_data[sensor_name[0]]['tss']
        >>> values = sensor_data[sensor_name[0]]['values']

    Example3 data format:  <ts>,<client_name>,<senor_name>,<unit>,<lat>,<long>
        >>> sensor_data = get_ts_value_from_client_sensor_file(file, ',', 1, 2, 0, [4, 5], [3])
        >>> client_name = sensor_data.keys()
        >>> client_0 = client_name[0]
        >>> sensor_name = sensor_data[client_0].keys()
        >>> sensor_0 = sensor_name[0]
        >>> tss = sensor_data[client_0][sensor_0]['tss']
        >>> values = sensor_data[client_0][sensor_0]['values'] 
    """

    result = {}
    if isinstance(client,int):
        read_client = True
    elif isinstance(client, basestring):
        read_client = False
        client_id = client
    else:
        return result
    if isinstance(sensor,int):
        read_sensor = True
    elif isinstance(sensor, basestring):
        read_sensor = False
        sensor_id = sensor
    else:
        return result
    f = open(filename, 'r')
    for line in f:
        text = line.rstrip()
        items = text.split(sep)
        if read_client:
            client_id = items[client]
        if read_sensor:
            sensor_id = items[sensor]
        if client_id not in result.keys():
            result[client_id] = {}
        if sensor_id not in result[client_id].keys():
            result[client_id][sensor_id] = {}
            result[client_id][sensor_id]['tss'] = []
            result[client_id][sensor_id]['values'] = []
            result[client_id][sensor_id]['para'] = []
        ts = str_to_int_float(items[pos_ts])
        value = [str_to_int_float(items[x]) for x in pos_value]
        para = [items[x] for x in pos_para]
        result[client_id][sensor_id]['tss'].append(ts)
        result[client_id][sensor_id]['values'].append(value)
        result[client_id][sensor_id]['para'].append(para)
    f.close()
    return result


def get_ts_value_from_file(filename, sep=';', pos_sid=0, pos_mt=2, pos_ts=0, pos_value=[1]):
    """get timestamp and values from a file, specific for BMW project 
    
    Args:
        filename: the file to read
        sep: the seperation character
        pos_sid: the position of session id, only for information, set to string if do not read from file
        pos_mt: the position of message type, used as sensor_name, set to string if do not read from file
        pos_ts: the position of timestamp
        pos_value: the position of values
    
    Returns:
        Dictionary with tss and values under sensor_name key, as following format:
        {
            sensor_name : {
                "tss": <tss_read>,
                "values": <values_read>
            }
        }

    Example1 data format:  <ts>,<lat>,<long>
        >>> sensor_data = get_ts_value_from_file(file, ',', 'client', 'sensor_name', 0, [1, 2])
        >>> tss = sensor_data['sensor_name']['tss']
        >>> values = sensor_data['sensor_name']['values']

    Example2 data format:  <ts>,<senor_name>,<lat>,<long>
        >>> sensor_data = get_ts_value_from_file(file, ',', 'client', 1, 0, [2, 3])
        >>> sensor_name = sensor_data.keys()
        >>> tss = sensor_data[sensor_name[0]]['tss']
        >>> values = sensor_data[sensor_name[0]]['values']

    If the data file include multiple clients, we need to use get_ts_value_from_client_sensor_file()
    """
    result = {}
    if isinstance(pos_sid,int):
        read_sid = True
    elif isinstance(pos_sid, basestring):
        read_sid = False
        sid = pos_sid
    else:
        return result
    if isinstance(pos_mt,int):
        read_mt = True
    elif isinstance(pos_mt, basestring):
        read_mt = False
        key = pos_mt
    else:
        return result
    f = open(filename, 'r')
    for line in f:
        try:
            text = line.rstrip()
            items = text.split(sep)
            if read_sid:
                sid = items[pos_sid]
            if read_mt:
                key = items[pos_mt]
            ts = str_to_int_float(items[pos_ts])
            value = [str_to_int_float(items[x]) for x in pos_value]
            if key not in result.keys():
                result[key] = {}
                result[key]['sid'] = sid
                result[key]['mt'] = key
                result[key]['tss'] = []
                result[key]['values'] = []
            result[key]['tss'].append(ts)
            result[key]['values'].append(value)
        except:
            pass
    f.close()
    return result

def get_format_data_string(sep, order, ts, value, sid=0, mt=2):
	texts = []
	for item in order:
		if item == 'sid':
			texts.append(str(sid))
		elif item == 'mt':
			texts.append(str(mt))
		elif item == 'ts':
			texts.append(str(ts))
		elif item == 'values':
			texts.append(sep.join([str(x) for x in value]))
		elif item == 'value':
			texts.append(value)
	result = sep.join(texts)
	return result

def write_ts_value_to_file(filename, sep=';', order=[], tss=[], values=[], sid=0, mt=2):
    if len(tss) > len(values):
    	count = len(values)
    else:
        count = len(tss)
    f = open(filename, 'w')
    for i in range(count):
        try:
        	text = get_format_data_string(sep, order, tss[i], values[i], sid, mt) + '\n'
        	f.write(text)
        except:
            pass
    f.close()
    return count

def write_bmv_data_file(filename, tss, values, sid, mt):
	sep = ','
	order = ['sid', 'ts', 'mt', 'values']
	write_ts_value_to_file(filename, sep, order, tss, values, sid, mt)

#--------------------------------------------------------------------
def test1():
    from tk_databank import DataBank
    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [3,4])
    D = DataBank()
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 8, "gps", "bmw_dataset_pos")
    pre_process = {
        "multiple": [100000, 100000]
    }
    post_process = {
        "divide": [100000.0, 100000.0]
    }
    D.sensor_data_process(8, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 5)
    print "*********** MIN_VALUES ***************"
    print D.get_sensor_raw_data(8)['min_v']
    print "*********** MAX_VALUES ***************"
    print D.get_sensor_raw_data(8)['max_v']
    print "********** REDUCATION INFO ***********"
    print D.get_sensor_process_info(8, 0)['reduce_info']
    print "********** COMPRESS, REDUCATION RATIO ***********"
    print D.get_zip_reduction_ratio(8, 500)    
    print "********** End of SENSOR 8 ***********"
    #print D.get_sensor_red_data(8, 0)
    #D.write_sensor_data_files(8)

    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [3, 4, 5, 6])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 9, "heading", "bmw_dataset_pos")
    pre_process = {
        "multiple": [100000, 100000, 1000, 1000],
        "value_exceptions": [
            [],
            [],
            [
                {
                    "e_name": "modulus",
                    "e_id":  1,
                    "v_types": ["red", "rest"],
                    "condition": "(abs(delta_v)>180000)",
                    "para":  "(delta_v/abs(delta_v)*360000)"
                },
                {
                    "e_name": "red_peak_points",
                    "e_id":  2,
                    "v_types": ["red"],
                    "condition": "((abs(delta_v)>30000) and (abs(delta_v)<330000))",
                    "para": "v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  3,
                    "v_types": ["rest"],
                    "condition": "((abs(delta_v)>30000) and (abs(delta_v)<330000))",
                    "para": "v"
                }
            ], 
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  2,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>8000)",
                    "para": "v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  3,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>8000)",
                    "para": "v"
                }
            ] 
        ]
    }
    post_process = {
        "divide": [100000.0, 100000.0, 1000.0, 1000.0]
    }
    D.sensor_data_process(9, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    D.sensor_data_process(9, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 5)
    D.write_sensor_data_files(9)
    recon_data = D.get_sensor_recon_data(9, 0)
    write_bmv_data_file("bmw1_500_09.csv", recon_data['tss'], recon_data['values'], sensor_data['pos']['sid'], sensor_data['pos']['mt'])
    print "*********** MIN_VALUES ***************"
    print D.get_sensor_raw_data(9)['min_v']
    print "*********** MAX_VALUES ***************"
    print D.get_sensor_raw_data(9)['max_v']
    print "********** REDUCATION INFO ***********"
    print D.get_sensor_process_info(9, 0)['reduce_info']
    print D.get_sensor_process_info(9, 1)['reduce_info']
    print "********** COMPRESS, REDUCATION RATIO ***********"
    print D.get_zip_reduction_ratio(9, 200)    
    print D.get_zip_reduction_ratio(9, 500)    
    print "********** End of SENSOR 9 ***********"

    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [6])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 10, "speed", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  2,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>8000)",
                    "para": "v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  3,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>8000)",
                    "para": "v"
                }
            ] 
        ]
    }
    post_process = {
        "divide": [1000.0]
    }
    D.sensor_data_process(10, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    D.sensor_data_process(10, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 5)
    D.write_sensor_data_files(10)
    print "*********** MIN_VALUES ***************"
    print D.get_sensor_raw_data(10)['min_v']
    print "*********** MAX_VALUES ***************"
    print D.get_sensor_raw_data(10)['max_v']
    print "********** REDUCATION INFO ***********"
    print D.get_sensor_process_info(10, 0)['reduce_info']
    print D.get_sensor_process_info(10, 1)['reduce_info']
    print "********** COMPRESS, REDUCATION RATIO ***********"
    print D.get_zip_reduction_ratio(10, 200)    
    print D.get_zip_reduction_ratio(10, 500)    
    print "********** End of SENSOR 10 ***********"

def test2():
    from tk_databank import DataBank
    D = DataBank()
    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [5])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
    sensor_data['pos']['tss'], 10, "speed", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>30000)",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>30000)",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ] 
        ]
    }
    post_process = {
        "divide": [1000.0]
    }
    D.sensor_data_process(10, 200, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    print D.get_sensor_raw_data(10)['dof']
    print "*"*100
    print D.get_sensor_red_data(10, 0)['dof']
    print "+"*100
    print D.get_sensor_recon_data(10, 0)['dof']

    D.save("test.dbk")

    E = DataBank()
    E.load("test.dbk")
    print("E"*50)
    print E.get_sensor_raw_data(10)['dof']
    print E.get_sensor_object(10)
    print E.get_sensor_object(10, 0)

def test3():
    from tk_databank import DataBank
    D = DataBank()    
    sensor_data = get_ts_value_from_file("bmw1.csv", ',', 0, 2, 1, [3,4])
    S = D.add_sensor_data(sensor_data['pos']['values'], 
        sensor_data['pos']['tss'], 8, "gps", "bmw_dataset_pos")
    pre_process = {
        "multiple": [1000000, 1000000]
    }
    post_process = {
        "divide": [1000000.0, 1000000.0]
    }
    D.sensor_data_process(8, 500, 0.9, 'cii_dct_v011', pre_process, post_process, 5)

    pre_process['integer'] = [1, 1]
    D.sensor_data_process(8, 500, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    D.write_sensor_data_files(8)

    pre_process['min_delta'] = [10, 10]
    D.sensor_data_process(8, 500, 0.8, 'cii_dct_v011', pre_process, post_process, 5)
    D.write_sensor_data_files(8)

    recon_data = get_ts_value_from_file("recon_data_8_0.csv", ';', '8', 'test', 0, [1,2])
    D.add_processed_sensor_data(8, recon_data['test']['values'], recon_data['test']['tss'])

    print "*" * 50
    print "gps distance devition process 0"
    gps_deviation = D.get_gps_deviations(8, 0)
    print "edge distance : " + str(gps_deviation['pe_deviations'])
    print "point distance : " + str(gps_deviation['pp_deviations'])
    print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    print gps_deviation['pe_distances'][3]
    print gps_deviation['pp_distances'][3]
    print gps_deviation['wgs84_pp_distances'][3]

    print "*" * 50
    print "gps distance devition process 1"
    gps_deviation = D.get_gps_deviations(8, 1)
    print "edge distance : " + str(gps_deviation['pe_deviations'])
    print "point distance : " + str(gps_deviation['pp_deviations'])
    print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    print gps_deviation['pe_distances'][3]
    print gps_deviation['pp_distances'][3]
    print gps_deviation['wgs84_pp_distances'][3]

    print "*" * 50
    print "gps distance devition process 2"
    gps_deviation = D.get_gps_deviations(8, 2)
    print "edge distance : " + str(gps_deviation['pe_deviations'])
    print "point distance : " + str(gps_deviation['pp_deviations'])
    print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    print gps_deviation['pe_distances'][3]
    print gps_deviation['pp_distances'][3]
    print gps_deviation['wgs84_pp_distances'][3]

    #print D.get_sensor_process_info(8, 2)
    D.get_deviations(8, 2)
    D.get_sensor_recon_data(8, 2)

    post_process_para = {
        "method": "savgol_filter",
        "window_length": 19,
        "polyorder": 2
    }
    D.sensor_data_post_process(8,1, post_process_para)
    print "*" * 50
    print "gps distance devition process 4"
    gps_deviation = D.get_gps_deviations(8, 4)
    print "edge distance : " + str(gps_deviation['pe_deviations'])
    print "point distance : " + str(gps_deviation['pp_deviations'])
    print "wgs point distance : " + str(gps_deviation['wgs84_pp_deviations'])
    print gps_deviation['pe_distances'][3]
    print gps_deviation['pp_distances'][3]
    print gps_deviation['wgs84_pp_distances'][3]

def test4():
    import json
    dataset = []
    client_sensor_data = get_ts_value_from_client_sensor_file('data_long.csv', ';', 5, 6, 3, [7], [1, 8])
    index = 0
    for c in client_sensor_data.keys():
        for s in client_sensor_data[c].keys():
            index += 1
            file_name = 'sensor_data1/' + '{0:04}'.format(index) + '__' + str(c) + '__' + str(s) + '.csv'
            print(file_name)
            fp = open(file_name, 'w')
            for i in range(len(client_sensor_data[c][s]['tss'])):
                str_para = ';'.join(client_sensor_data[c][s]['para'][i])
                str_values = str(client_sensor_data[c][s]['values'][i][0])
                text = str(client_sensor_data[c][s]['tss'][i]) + ';' + str_values + ';' + str_para + '\n'
                fp.write(text)
            fp.close()
            sensor = {}
            sensor['sensor_id'] = index
            sensor['client_name'] = str(c)
            sensor['sensor_name'] = str(s)
            sensor['file'] = file_name
            sensor['dof'] = 1
            sensor['unit'] = client_sensor_data[c][s]['para'][0][1]
            dataset.append(sensor)
    #print(dataset)
    print json.dumps(dataset,sort_keys=True, indent=4)

if __name__ == '__main__':
    test4()
