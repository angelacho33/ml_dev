"""
Teraki tools
AUTHOR: 
   Markus

DESCRIPTION:
   Various functions for evaluating the reconstruction accuracy by means of 
   deviation from the original signal

MODIFICATION HISTORY:
   Written, MK, 15.10.2015
   Renaming of functions, MK, 17.10.2015
   Change of filename. Rewritten for modularity, consistent nameing.
       Integrated common generic function errperf. MK, 14.01.2015

DEPENDANCIES:
    numpy

TODO:
    Review Chi2 and Chi2_red calculation, as well as snr, snr_db
    check inf/nan handling in mpe/mape, maybe remove the exclusion of inf/nan

"""
import numpy as np

# Pointwise errors
def e(O, R):
    """Calculating pointwise difference between original and reconstructed signal

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data
 
    Returns:
        Ve (numpy array): Vecotr of differences: O-R        
    """
    Ve = np.array(O) - np.array(R)
    return Ve
    
def ae(O, R):
    """Calculating absolute pointwise difference
    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data
 
    Returns:
        Vae (numpy array): Vecotr of differences: O-R
    """
    Ve = e(O, R)
    Vae = np.abs(Ve)
    return Vae

def se(O, R):
    """Calculating the Squared Error (SE)
    
    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vae (numpy array): Error meassre: squared error
    """
    Ve = e(O, R)
    Vse = np.square(Ve)
    return Vse

# Pointwise relative errors
def re(O, R, norm='orig'):
    """Calculate relative errors (RE) 

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vae: (numpy array): Error measure: (O-R)/O

    Note:
        1./0. = inf
        0./0. = nan
    """
    if norm == 'orig':
        denominator = np.array(O)
    elif norm == 'avg':     #average between original and reconstructed
        denominator = np.mean([O, R], axis=0)

    Ve = e(O, R)
    Vre = Ve / denominator
    return Vre

def are(O, R):
    """Calculate absolute relative errors (ARE) 

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vae: (numpy array): Error measure: |(O-R)/O|

    Note:
        1./0. = inf
        0./0. = nan
    """
    Vre = re(O, R)
    Vare = np.abs(Vre)
    return Vare

def sre(O, R):
    """Calculate squared relative errors (SRE) 

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vae: (numpy array): Error measure: ((O-R)/O)^2

    Note:
        1./0. = inf
        0./0. = nan
    """
    Vre = re(O, R)
    Vsre = (Vre)**2
    return(Vsre)

# Pointwise percentage errors (relative errors * 100)
def pe(O, R, norm='orig'):
    Vre = re(O, R, norm=norm)
    Vpe = Vre * 100.
    return(Vpe)

def ape(O, R):
    """Calculate Percentage Error (PE) 

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vae: (numpy array): Error measure: |(O-R)/O| * 100

    Note:
        1./0. = inf
        0./0. = nan
    """
    Vpe = pe(O, R)
    Vape = np.abs(Vpe)
    return Vape

def spe(O, R):
    """Squared Pecentage Error (SPE)
    """
    Vpe = pe(O, R)
    Vspe = np.squared(Vpe)
    return(Vspe)

# Performance metrics - Errors
def mae(O, R):
    """Mean Absolute Error (MAE)
    
    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data
 
    Returns:
        Vae (numpy array): Vecotr of mean absolute differences
    """
    Vae = ae(O, R)
    Vmae = np.mean(Vae)
    return Vmae

def mse(O, R):
    """Mean Squared Error (MSE)
    
    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vae (numpy array): error meassre: mean squared error
    """
    Vse = se(O, R)
    Vmse = np.mean(Vse)
    return(Vmse)

def rmse(O, R):
    """Root Mean Square Error (RMSE)

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vae (numpy array): Error meassre: root mean square error
    """
    Vmse = mse(O, R)
    Vrmse = np.sqrt(Vmse)
    return(Vrmse)

# Performance metrics - Relative errors
def rrmse(O, R, norm='maxmin'):
    """Relative Root Mean Square Error (RRMSE)
    
    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data
        norm (str): Specifies which normalization to use:
            'maxmin': range, given by difference between max and min value of the 
                original data. This is the default setting.
            'mean': Mean of the original data. When normalizing by the mean, often the 
                term "Coefficient of variation of the RMSE (CvRMSE) is used.
            'std: Standard deviation with the norm convention n - 1.
    Returns:
        Vrrmse (numpy array): error meassre
    """
    if norm == 'maxmin':
        denominator = np.max(O) - np.min(O)
    elif norm == 'mean':
        denominator = np.mean(O)
    elif norm == 'std':
        denominator = np.std(O, ddof=1)
    else:
        print 'Normalization not suported'
        return(None)

    Vrmse = rmse(O, R)
    Vrrmse = Vrmse / denominator * 100.
    return(Vrrmse)
    
# Performance Metrics - Percentage errors
def mpe(O, R):
    """Calculating the Mean Percentage Deviation (MPD) 

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vmape (float): error measure
    """
    Vpe = pe(O, R)
    # Vpe might contain inf or nan corrsponding to devisions by zero. exclude them
#    n_infnan = np.sum(np.isinf(Vpe)+np.isnan(Vpe))
#    if n_infnan > 0:
#        print('In mpe() there are {0} entires with inf/nan').format(n_infnan)
#    Vmpe = np.mean(Vpe[~( np.isnan(Vpe) + np.isinf(Vpe) ) ])
    Vmpe = np.mean(Vpe)
    return(Vmpe)

def mape(O, R):
    """Calculating the Mean Absolute Percentage Error (MAPE) 

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vmape (float): Error measure
    """
    Vpe = pe(O, R)
#    n_infnan = np.sum(np.isinf(Vpe)+np.isnan(Vpe))
#    if n_infnan > 0:
#        print('In mape() there are {0} entires with inf/nan').format(np.sum(np.isinf(Vpe)+np.isnan(Vpe)))
#    Vmape = np.mean(np.abs(Vpe[~( np.isnan(Vpe) + np.isinf(Vpe) )]))
    Vmape = np.mean(np.abs(Vpe))
    return(Vmape)

def smape(O, R):
    """Calculating the Symetric Mean Absolute Percentage Error (SMAPE) 

    In comparison to MAPE, it uses the averade of orig and reconstructed points in the denominator.
    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vsmape (float): Error measure
    """
    Vae = ae(O, R)
    denominator = (np.abs(O) + np.abs(R)) / 2.
    Vsmape = np.mean(Vae / denominator) * 100.

    return(Vsmape)

#-----
def chi2(O, R, norm='expected_value'):
    """Calculating Chi^2 error

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vchi2: Error meassre: Chi^2 error
    """
    # to be veryfied !!!
    Vse = se(O, R)

    if norm == 'std':
        denominator = np.std(O)**2
    elif norm == 'expected_value':
        denominator = O

    Vchi2 = np.sum(Vse / denominator)
    return(Vchi2)

def chi2_red(O, R, nfp=0):
    """Calculating Reduced Chi^2 error

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vchi2_red: Error meassre: Reduced Chi^2 error

    Annotation:
    Wikipedia: As a rule of thumb (again valid only when the variance of the measurement error is known a priori rather 
    than estimated from the data), a \chi_\mathrm{red}^2 \gg 1 indicates a poor model fit. A \chi_\mathrm{red}^2 > 1 
    indicates that the fit has not fully captured the data (or that the error variance has been underestimated). 
    In principle, a value of \chi_\mathrm{red}^2 = 1 indicates that the extent of the match between observations and 
    estimates is in accord with the error variance. A \chi_\mathrm{red}^2 < 1 indicates that the model is 'over-fitting' 
    the data: either the model is improperly fitting noise, or the error variance has been overestimated
    """
    # to be veryied !!!
    # nfp is basically: framesize*sparsity (=m)
    Vchi2 = chi2(O, R)
    degrees_of_freedom = len(O) - nfp - 1    # N-n-1; N=number of observations, nfp=number of fitted parameters
    Vchi2_red = Vchi2 / degrees_of_freedom
    return(Vchi2_red)

def snr(O, R):
    """Calculating the Signal to Noise Ratio (SNR)
    
    Args:
    
    Returns:
    """
    Vmse = mse(O, R)
    sigma_square = np.mean(np.square(np.array(O) - np.mean(O)))
    Vsnr = sigma_square / Vmse
    return(Vsnr)
    
def snr_db(O, R):
    """Calculating the Signal to Noise Ratio (SNR) expressed in deci bell
    
    Args:
    
    Returns:
    """
    Vsnrdb = 10 * np.log10(tk_snr(O, R))
    return(Vsnrdb)

    

def wape(O, R):
    """Calculating the Weighted Absolute Percentage Error (WAPE)

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vmape (float): error measure
    """
    Ve = e(O, R)
    Vwape = np.sum(np.abs(Ve)) / np.sum(np.abs(O)) * 100.
    return Vwape


def wpe(O, R):
    # Remove, not really practical
    """Calculating the Weighted Percentage Error (wpd). Note that there is 
    no absolute taken of the difference

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data

    Returns:
        Vmape (float): error measure
    """
    Ve = e(O, R)
    Vwpe = np.abs(np.sum(Ve)) / np.sum(O) * 100.
    return Vwpe


def relerr(data, data_rec):
    # Fuction used by Daniel, es equivalent to (differs from WAPE by the abs that is taken after the sum):
    # Ve = np.array(O) - np.array(R)
    # Vwape = np.abs(np.sum(Ve)) / np.sum(R) * 100.
    # Output is in percent!
    n=np.arange(len(data))
    data_sum=0
    data_sum_norm=0
    for i in n:
        data_sum = data_sum+pow((data[i] - data_rec[i]),1)
        data_sum_norm = data_sum_norm + pow((data[i]),1)
    data_sum=abs(data_sum)/data_sum_norm * 100.
    return data_sum


#Add Euclidian norm 
#d = |O-R|^2 / (|O||R|)

def std(O, ddof=1):
    """Calculate standarddeviation for input signal
    
    Numpy's standard deviation function is used, with ddor=1, which corresponds
    to the division by (n - 1) and thus to the following fomular:
    mystd = np.sqrt(np.sum((O - np.mean(O))**2) / (n - 1))
    
    Args:
        O (list): Signal for which to calculate the standar deviation
    
    Returns:
        Vstd (float): Standard deviation for input signal
    """
    Vstd = np.std(O, ddof=ddof)
    return(Vstd)

def interpolate(Ox, Rx, Ry):
    """Interpolate...
    
    Args:
    
    Returns:
    """
    Ry_interpol = np.interp(Ox, Rx, Ry)
    return Ry_interpol

def errperf(O, R, M):
    """Calculating various error related performance metrics
    Calculation tree:

    e
    |
    |-ae-et-mae
    |
    |-se-mse-rmse
    |
    |-re-pe
      |  |
      |  |-ape-mape
      |  |
      |  |-spe-mspe-rmspe
      |
      |-are-mare
      |
      |-sre-msre-rmsre

    Args:
        O (list or numpy array): Original (raw) data
        R (list or numpy array): Reconstructed data
        M (str): Name of Error Meassure which can be one of the following:

        mae (mean absolute error)
        mse (mean squared error)
        rmse (root mean squared error)      to be implemented... 
        mare (mean absolute relative error)     to be implemented... 
        rmse (relative mean squared error)
        rrmse (relative root mean squared error)
        mape (mean absolute percentage error)
        mspe (mean squared percentage error)        to be implemented... 
        rmspe (root mean squared percentage error)  to be implemented... 

    Returns:
        Vmape (float): error measure
    """
    def get_function(name):
        switcher = {
            'e': e,
            'ae': ae,
            'mae': mae,
            'ape': ape,
            'se': se,
            'mse': mse,
            'rmse': rmse,
            'rrmse': rrmse,     # ! common measure
            'mape': mape,       # ! for signals != 0
            'smape': smape,     # ! alternative to mape
        }
        # Get the function from switcher dictionary
        func = switcher.get(name, lambda: "nothing")
        # Execute the function
        return(func)
    fct = get_function(M)
    error = fct(O, R)
    return error

def test():
    import random
    np.set_printoptions(edgeitems=2)
    
    #O = np.random.randn(2000)
    #R = np.random.randn(2000)
    
    mu, sigma = 0, 0.1
    O = np.random.normal(mu, sigma, 2000)     # original
    R = np.random.normal(mu, sigma, 2000)     # reconstructed
    print min(O), max(O)
    print
    print '=== Pointwise Errors ==='
    print '----- Errors --------------------------'
    print 'Error (E):                    ', errperf(O, R, 'e')
    print 'Absolute Error (AE):          ', errperf(O, R, 'ae')
    print 'Squared Error (SE):           ', errperf(O, R, 'se')
    print 
    print '----- Percentage Errors (pointw.) -----'
    print 'Absoulte Percent. Error (APE):', errperf(O, R, 'ape')
    print
    print '=== Performance mesures ==='
    print '----- Errors --------------------------'
    print 'Mean Absoulte Error (MAE):        ', errperf(O, R, 'mae')
    print 'Mean Square Error (MSE):          ', errperf(O, R, 'mse')
    print 'Root Mean Squared Err. (RMSE):    ', errperf(O, R, 'rmse')
    print 
    print '----- Relative errors -----------------'
    print
    print '----- Percentage errors ---------------'
    print 'Mean Abs. Percent. Er. (MAPE):    ', errperf(O, R, 'mape')
    print 'Sym. Mean Abs. Per. Er. (SMAPE):  ', errperf(O, R, 'smape')
    print
    print
    print 'RRMSE (maxmin):                   ', rrmse(O, R, norm='maxmin')
    print 'RRMSE (mean):                     ', rrmse(O, R, norm='mean')
    print 'RRMSE (std):                      ', rrmse(O, R, norm='std')
    print 'Standard deviation of O:          ', std(O)
    print 'Chi^2 (norm: exp. value):         ', chi2(O, R, norm='expected_value')
    print 'Chi^2 (norm: std):                ', chi2(O, R, norm='std')

if __name__ == '__main__':
    test()
