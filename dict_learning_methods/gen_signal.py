"""
Project Machine Learning
Author: Etienne
Functions for generating signals from sawtoothes, sines and/or Haar wavelets
"""

import numpy as np
import csv

import scipy.signal as scs

def sawtooth(length, width):
    """Discrete sub-sampled sawtooth wavelet"""
    x = np.linspace(0, length - 1, length)
    x = scs.sawtooth(2*np.pi*x/((width)), width=0.5);
    return (x+1)

def Haar(width, length, amplitude, start):
    x=np.zeros(length)
    if (start+width<length):
        x[start:start+width]=amplitude
    return x

def mid_Haar(width, length, amplitude):
    return (Haar(width, length, amplitude, (length-width)/2))

def mid_Haar_period(width, length, amplitude, period):
    n_segm=length/period
    x=np.zeros(length)
    for i in range(np.int(n_segm)):
        x[i*period:(i+1)*period]=mid_Haar(width, period, amplitude)
    return x

def write_csv(filename, t, y):
    data = [[a, b] for a, b in zip(t, y)]
    with open(filename, 'wb') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerows(data)

def read_csv(filename): #wrong for the previous write function (have to write the header)
    data = []
    with open(filename, 'rb') as f:
        reader = csv.reader(f, delimiter=';')
        for row in reader:
            data.append(row)
    header = data[0]
    if len(header) == 2:
        t = [int(element[0]) for element in data[1:]]
        value = [float(element[1]) for element in data[1:]]
        return(t, value)
    elif len(header) == 4:
        t = [int(element[0]) for element in data[1:]]
        value_x = [float(element[1]) for element in data[1:]]
        value_y = [float(element[2]) for element in data[1:]]
        value_z = [float(element[3]) for element in data[1:]]
        return(t, value_x, value_y, value_z)

def diff(a, b):
    b = set(b)
    return [aa for aa in a if aa not in b]
	   

def sin_randocc(SNR_Noise,period,M,k,length, ch):
    #sample_length = sample_length      # in s
    length        = length

    ch=ch
    SNR_Noise     = SNR_Noise
    period        = period
    M             = M
    k             = k       # Set number of k frequency components

    ftest=1./period
    N = length#sample_length/dt


    
    k_random = np.random.normal(0, 1., (k, np.int(N/period)))

    u=np.zeros(N)
    k_entry=np.zeros((k, N))
    for j in range(N):
        k_entry[:,j]=k_random[:,np.int(j/period)]
    choice_2=np.random.binomial(1, ch, (len(k_entry), np.int(N/period)))
    choice=np.zeros((len(k_entry), N))
    for j in range(N):
        choice[:,j]=choice_2[:,np.int(j/period)]
        
    for i in range(len(k_entry)):
        u += M*choice[i]*k_entry[i]*np.sin(2*np.pi*ftest*np.arange(N)*i)

    if (SNR_Noise==np.infty):
        u_noise=u
    else:
        noise = np.random.normal(0, 0.5*M/SNR_Noise, N)
        u_noise = u + noise
    return u_noise
    
def gen_sin_randocc(SNR_Noise,period,M,k,length, ch):
    u_noise=sin_randocc(SNR_Noise,period,M,k,length, ch)
    t = np.linspace(0, length, length, dtype=np.int)
    write_csv('Generated signals/generated_sin_SNR_'+str(SNR_Noise)+'_period_'+str(period)+'_M_'+str(M)+'_k_'+str(k)+'_ch_'+str(ch)+'.csv', t, u_noise)
      
def sawtooth_randocc(SNR_Noise,period,M,k,length, ch):
    #sample_length = sample_length      # in s
    length        = length

    ch=ch
    SNR_Noise     = SNR_Noise
    period        = period
    M             = M
    k             = k       # Set number of k frequency components

    
    N = length#sample_length/dt


    
    k_random = np.random.normal(0, 1., (k, np.int(N/period)))

    u=np.zeros(N)
    k_entry=np.zeros((k, N))
    for j in range(N):
        k_entry[:,j]=k_random[:,np.int(j/period)]
    choice_2=np.random.binomial(1, ch, (len(k_entry), np.int(N/period)))
    choice=np.zeros((len(k_entry), N))
    for j in range(N):
        choice[:,j]=choice_2[:,np.int(j/period)]
        
    for i in range(len(k_entry)-1):
        u += M*choice[i]*k_entry[i]*sawtooth(length, (period+0.0)/(i+1))

    if (SNR_Noise==np.infty):
        u_noise=u
    else:
        noise = np.random.normal(0, 0.5*M/SNR_Noise, N)
        u_noise = u + noise

    return u_noise
 
def gen_sawtooth_randocc(SNR_Noise,period,M,k,length, ch):
    u_noise=sawtooth_randocc(SNR_Noise,period,M,k,length, ch)
    t = np.linspace(0, length, length, dtype=np.int)
    write_csv('Generated signals/generated_sawtooth_SNR_'+str(SNR_Noise)+'_period_'+str(period)+'_M_'+str(M)+'_k_'+str(k)+'_ch_'+str(ch)+'.csv', t, u_noise)

def gen_sin_sawtooth_randocc(SNR_Noise,period,M,k,length, ch):
    u_noise=sawtooth_randocc(SNR_Noise,period,M,k,length, ch)+sin_randocc(SNR_Noise,period,M,k,length, ch)
    t = np.linspace(0, length, length, dtype=np.int)
    write_csv('Generated signals/generated_sin_sawtooth_SNR_'+str(SNR_Noise)+'_period_'+str(period)+'_M_'+str(M)+'_k_'+str(k)+'_ch_'+str(ch)+'.csv', t, u_noise)
  
      
def gen_Haar_randocc(SNR_Noise,period,M,k,length, ch):
    #sample_length = sample_length      # in s
    length        = length

    ch=ch
    SNR_Noise     = SNR_Noise
    period        = period
    M             = M
    k             = k       # Set number of k frequency components

   
    N = length#sample_length/dt


    t = np.linspace(0, length, length, dtype=np.int)
    k_random = np.random.normal(0, 1., (k, np.int(N/period)))

    u=np.zeros(N)
    k_entry=np.zeros((k, N))
    for j in range(N):
        k_entry[:,j]=k_random[:,np.int(j/period)]
    choice_2=np.random.binomial(1, ch, (len(k_entry), np.int(N/period)))
    choice=np.zeros((len(k_entry), N))
    for j in range(N):
        choice[:,j]=choice_2[:,np.int(j/period)]
        
    for i in range(len(k_entry)):
        u += M*choice[i]*k_entry[i]*mid_Haar_period(2*i, length, 1.0, period)

    if (SNR_Noise==np.infty):
        u_noise=u
    else:
        noise = np.random.normal(0, 0.5*M/SNR_Noise, N)
        u_noise = u + noise

    write_csv('Generated signals/generated_Haar_SNR_'+str(SNR_Noise)+'_period_'+str(period)+'_M_'+str(M)+'_k_'+str(k)+'_ch_'+str(ch)+'.csv', t, u_noise)
   
    
if __name__ == '__main__':
    #test_sin_quant_noise()
    #k_sparse_signal(k,psi_type)
    #gen_sin_quant_noise(1024,2,256,True,10)
    #print "Nothing going on here"
    SNR_Noise=5
    period = 50.
    M = 1
    k=25
    Sample_size=100000
    ch=0.3
    
    gen_sin_sawtooth_randocc(SNR_Noise,period,M,k,Sample_size, ch)
    