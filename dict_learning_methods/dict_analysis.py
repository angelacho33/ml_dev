# -*- coding: utf-8 -*-
"""
Here we analyse the dictionary which has been saved in the filename file.
"""
import csv
import numpy as np
import matplotlib.pylab as pl


def dict_read(filename):
    dicto=[]
    
    cr = csv.reader(open(filename,"rb"), delimiter=';')
    for row in cr:
        dicto.append(row)
    
    D_a=np.array(dicto).T
    D=np.zeros((len(D_a), len(D_a[0])))
    for i in range(len(D)):
        for j in range(len(D[0])):
            D[i,j]=np.float(D_a[i,j])
    return D

if __name__ == '__main__':
   
    filename='Dictionaries/dictionary_steam1_omp_600_75.csv'
    D=dict_read(filename)
    print '%s x %s' %(len(D), len(D[0]))