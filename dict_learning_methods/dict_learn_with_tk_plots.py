# -*- coding: utf-8 -*-


"""
===========================================
Dictionary learning
===========================================
"""


##############################################################################
#Importation

import time
import os
import numpy as np
import matplotlib.pylab as pl
#from sklearn.decomposition import SparseCoder
from sklearn_library_modifications.dict_learning import SparseCoder
#from sklearn.decomposition import DictionaryLearning
from sklearn_library_modifications.dict_learning import DictionaryLearning
import dict_learning_methods.plot_dictionary as pd
import csv
from dict_learning_methods.dict_analysis import dict_read
import tk_err
import dict_learning_methods.Fourier_filter as ffl
#import read_bmw
#import dictionaries_generation as gen

from tk_eng_rdpi_v001 import Decomposer
from tk_eng_rdpi_v001 import Reconstructor

    
if __name__ == '__main__':
    
    ##############################################################################
    #Generation of the signal
    print(__doc__)
    import sys
    import glob
    import os
    import tk_gdfr
    import tk_plot
    from tk_databank import DataBank
    from tk_plot import Plot
    
    folder = '/home/angelh/Documents/DataScienceRelated/Teraki/Geeny/Files/'
    filenamelist=[folder+"tig1day_posix_acc_interp.csv"]
    #filenamelist_claas = sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=True)
    
    
    for item,file in enumerate(filenamelist):
        Dat = DataBank() ##One databank for each file
        P = Plot(Dat)
        sensor_id=item        
        
        sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1])
        sensor_name='sensor_'+str(item+1)
        print 'Processing sensor:',sensor_id+1,' ',sensor_name
        
        number_of_datapoints=200000
        starting_datapoint=300000
        values= sensor_data1['pos']['values'][starting_datapoint:starting_datapoint+number_of_datapoints]
        timestamps= sensor_data1['pos']['tss'][starting_datapoint:starting_datapoint+number_of_datapoints]
        #timestamps = np.arange(0,len(values))*1000 #In case there are no timestamps in the csv file: create an array of timestamps here with 1Hz sampling rate
        
        ##All data to databank: later the second half of this data is used for the dictionary training
        ##and the first half is decoded in terms of this dictionary
        S1 = Dat.add_sensor_data(values,timestamps, sensor_id, sensor_name)
        
        ##To compare apples to apples here I add only the first half of the data to make the raw-rec comparison where rec is the reconstructed data z_1 which is an output of the dictionary learning decoding 
        first_half_values=values[:len(values)/2]
        first_half_timestamps=timestamps[:len(values)/2]
        S2 = Dat.add_sensor_data(first_half_values,first_half_timestamps, sensor_id+1, sensor_name)
        
        
        ##WTF the next line is needed to the plot routines to get the 'value_names' of entry of the sensor info dictionary???
        print Dat.get_sensor_raw_data(sensor_id +1 )['value_names'] 
        print Dat.get_sensor_raw_data(sensor_id)['value_names'] 

        P.plot_raw_inspection(sensor_id+1, 'png')
        P.plot_raw_inspection(sensor_id, 'png')
        
        ##############################################################################
        ###### Process Data via Dictionary Learning (Ettiene Old form routines)....second half for training...first half for testing..
    
        ####################
        #Raw data info
        raw_data=Dat.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss']) 
        values=np.asarray(raw_data['values']) ##
        npoints=raw_data['count']
        
        data_tmp = values
        data_tmp = [x[0] for x in data_tmp]
        data_tmp = np.asarray(data_tmp)
        
        y_b = data_tmp[0:len(data_tmp)/2] ##AngelH Note: First half of the data is the Testing Dataset
        y_1 = data_tmp[len(data_tmp)/2:len(data_tmp)] ##AngelH Note: Second half of the data is used for the Dictionary generation
    
        maxmin=np.max(y_b)-np.min(y_b) ##maxmin refers to the raw data amplitude
        
        ##Parameters for Dictionary Learnning
        plot=False
        plot_dict=False
        write=True
        restart=True
        notrainrestart=True
        comparewithothermethods=False
    
        #adjustable parameters
        frame_size=64
        n_components =frame_size #number of basic functions in the dictionary
        tol=np.sqrt(0.03)
        sparse_tol=np.sqrt(0.02)
        sparsity_controlling_parameter=0.001
        num_error=1e-9
        max_thresh=10.
        min_thresh=5.
        max_deviation=0.01*maxmin    #imposed max deviation during the thresholding phase
        tol_1train=None      #imposed max deviation during the training phase
        tol_1sparse=max_deviation #imposed max deviation during the testing sparse coding phase
    
        #Other parameters
        resolution = len(y_1);
        true_resolution = len(np.flatnonzero(y_b))
        n_segm = pd.sup_int((resolution+0.0)/frame_size); #is the number of segments which decompose the original signal (which is here the concatenation of 2 signals)
    
    
        #avoid the edges problems
    
        if (resolution%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
            for j in range (frame_size-(resolution%frame_size)):
                y_1=np.concatenate((y_1, [y_1[resolution-1]]))
           
    
        variance_1=(np.mean(y_1**2)-np.mean(y_1)**2) 
    
    
     
        ##############################################################################
        #Printing
           
        print 'Frame size : ', frame_size
        print 'Number of segments : ', n_segm
        print 'Number of components : ', n_components
        print "Resolution : ", resolution
        print 'Training Tolerance : ', tol  #maximal authorized RMSE 
        print 'Sparse coding Tolerance : ', sparse_tol
        print 'Sparsity controlling parameter : ', sparsity_controlling_parameter
        print 'Numerical Error Tolerance : ', num_error
        print 'Maximum Threshold (in percentage) : %s' % max_thresh
        print 'Minimum Threshold (in percentage) : %s' % min_thresh
    
        resolution = len(y_1);          
        t0 = time.time()
    
        ##############################################################################
        #Defines the useful functions/methods
        
        #Filters the signal
        y=y_1
     
        #a list of the different methods we can choose (omp seems to be the best at the moment)
        estimators = [('OMP', 'omp', tol*tol*variance_1*resolution/n_segm, None), ('LASSO_Lars', 'lasso_lars', 0.5, None),
                      ('LASSO_cd', 'lasso_cd', 0.5, None), ('Threshold', 'threshold', 5, None),
                      ('Lars', 'lars', 0.5, None)]
    
            
            
        h=np.zeros((n_segm, int(len(y)/n_segm))) #will represent the different segments of y, is initialized further
        for l in range(n_segm):
            h[l]=(y[l*resolution/n_segm:(l+1)*resolution/n_segm]) #h represents now the n_segm segments dividing the y signal
    
        ##############################################################################
        #Dictionary initialization step
    
        if (restart):
        #last result
            title, algo, alpha, n_nonzero = estimators[0]
            
            filename_2="Dictionaries/dictionary_%s_sensor_id_%s_ncomp_%s_framesize_%s.csv" %(sensor_name,sensor_id,n_components, frame_size)
            D_0=dict_read(filename_2)
            coder_0 = SparseCoder(dictionary=D_0, transform_n_nonzero_coefs=n_nonzero,
                                transform_alpha=alpha, transform_algorithm=algo, tol_1=tol_1train)
            c_0=coder_0.transform(h) 
            print 'Restart initialization : ', filename_2
        else:
            #No initialization
            print 'Initialization : None'
            D_0=None
            c_0=None
    
        ##############################################################################
    
    
    
    
        #Start of the training phase
        if (plot):
            fi1=pl.figure(1, figsize=(13, 6))
            pl.title('Training dataset')
            pl.plot(y_1, ls='dotted', label='Original signal')
    
        #Setting the final parameters
        title, algo, alpha, n_nonzero = estimators[0]   
        fit_algo = 'cd'
    
        print 'Fit algorithm : ', fit_algo   
        print title 
    
        if (restart and notrainrestart):
            D=D_0
            x=c_0
            print '-'*40
            print 'Training phase : loaded the previous dictionary'
            print 'Variance : ', variance_1 
        
        else:         
            #Dictionary Learning step
            print '-'*40
            print 'Training phase :'
            print 'Variance : ', variance_1 
            dictio = DictionaryLearning(n_components=n_components, alpha=sparsity_controlling_parameter*np.sqrt(variance_1), max_iter=2000, tol=num_error, fit_algorithm=fit_algo, transform_algorithm=algo, transform_n_nonzero_coefs=n_nonzero, transform_alpha=alpha, n_jobs=1, code_init=c_0, dict_init=D_0, verbose=False, split_sign=False, random_state=None, tol_1=tol_1train)
            #On this step is the dictionary learned, and x is then transformed in the new basis
            x=dictio.fit_transform(h)
                 
        #Analyses the results        
            D=dictio.components_ #here is the final dictionary
        
        density = len(np.flatnonzero(x)) #the number of non zero coeffs    
        z = np.ravel(np.dot(x, D)) #x is then the reconstructed signal in the original basis
        rsquared_error = tk_err.rrmse(y, z, norm='std') #RRMSE
        maxmin_error = tk_err.rrmse(y, z, norm='maxmin') 
    
    #Printing
        print 'RRMSE : ', rsquared_error
        print 'Maxmin RRMSE : ', maxmin_error
        print 'Non zero coeffs : ', density
    
    #Ploting
        if (plot):
            pl.plot(z, label='%s: %s nonzero coefs,\n%.2f error'
                        % (title, density, maxmin_error))
            pl.axis('tight')
            pl.legend()
    
        
        dt = time.time() - t0
        print('done in %.2fs.' % dt)
        #plots the original and reconstructed signal
        if (plot):
            pl.subplots_adjust(.04, .07, .97, .90, .09, .2)
    
    
    ###############################################################################
    # Plot the generated dictionary
        if (plot_dict):
            fi2=pl.figure(2, figsize=(4.2, 4))
            fi2.suptitle("Generated dictionary", fontsize=18)
            pd.plot_dic(D)
    
    ###############################################################################
    
    ##############################################################################
    #Generates the signal to test
    
        t1=time.time()
    
    
    
    
        resolution = len(y_b);
        n_segm = pd.sup_int((resolution+0.0)/frame_size) #is the number of segments which decompose the original signal
        if (resolution%frame_size>0):
            for j in range (frame_size-(resolution%frame_size)):
                y_b=np.concatenate((y_b, [y_b[resolution-1]]))
            
        resolution = len(y_b); 
    
        h_b=np.zeros((n_segm, frame_size))
        h=np.zeros((n_segm, frame_size))
    
    
    
        for l in range(n_segm):
            h_b[l]=(y_b[l*frame_size:(l+1)*frame_size])
    
    
        variance_b=(np.mean(y_b**2)-np.mean(y_b)**2)
    
    ##############################################################################
    #Start of the testing phase
    
    #Defines the method
        coder = SparseCoder(dictionary=D, transform_n_nonzero_coefs=n_nonzero,
                            transform_alpha=sparse_tol*sparse_tol*variance_b*resolution/n_segm, transform_algorithm=algo, tol_1=tol_1sparse)
                                            
     
        print '-'*40
        print 'Testing phase :'        
        print 'Variance : ', variance_b
    
        if (plot):
            fi3=pl.figure(3, figsize=(13, 6))
            pl.title('Testing dataset')
            #P.plot(sensor_id,process_id,deviation='difference',output = 'png',suptitle='Claas_Sensor_'+str(sensor_n)+'_Reduction_'+str(red_info_ratio)+'_Frame_'+str(framesize)+'_CoreRed_'+str(reduction)+'_Block_'+str(block)+'_RRMSE_'+str(rrmse))
            pl.plot(y_b, ls='dotted', label='Original signal')
    
    
    #on this line, x is the determined to minimize ||h-Dx|| with the condition that
    #every array of x has less than n_z non zero coefficients (for the omp method)
        t0_coder = time.time()
        print 'Starting timestamp for DL encoding', t0_coder, 'for sensor_id:', sensor_id
        x_1=coder.transform(h_b) 

    #Thresholding phase
        t3 = time.time()
    #Intialize
        maxi=np.max(np.abs(x_1))
        z_1=np.ravel(np.dot(x_1,D))
        rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
        deviation=np.max(np.abs(y_b-z_1))
        thresh=min_thresh
        x_2=np.copy(x_1)
        step=1.0
    #Find the right threshold to set
        while ((rsquared_error<100*sparse_tol) and (thresh < max_thresh) and deviation<max_deviation):
            thresh+=step
            x_2[np.abs(x_2)<thresh/100*maxi]=0
            z_2=np.ravel(np.dot(x_2,D))
            rsquared_error = tk_err.rrmse(y_b, z_2, norm='std') #RMSE
            deviation=np.max(np.abs(y_b-z_2))
    
        while ((rsquared_error>100*sparse_tol or deviation>max_deviation) and (thresh>0) ):
            x_2=np.copy(x_1)
            thresh-=step
            x_2[np.abs(x_2)<thresh/100*maxi]=0
            z_2=np.ravel(np.dot(x_2,D))
            rsquared_error = tk_err.rrmse(y_b, z_2, norm='std')
            deviation=np.max(np.abs(y_b-z_2))      
    #Set the low coefficients to 0 with the right threshold
        for i in range(n_components):    
            x_1[np.abs(x_1)<thresh/100*maxi]=0
    
        t4=time.time()-t3 
        print('Thresholding phase done in %.2fs.' % t4)
    
        dt_DL_encoding=time.time()-t0_coder
        
    ##############################################################################
        
    
    #Analyses the results
        density=len(np.flatnonzero(x_1)) #number of non zero coefficients
        z_1=np.ravel(np.dot(x_1,D))
        rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
        maxmin_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
        print 'Actual threshold (in percentage) : ', thresh
        print 'RRMSE : ', rsquared_error
        print 'Maxmin_RRMSE : ', maxmin_error
        print 'Infinite norm : ', np.max(np.abs(y_b-z_1))
        print 'Non zero coeffs : ', density
        print 'Compression ratio : ', 100*(1-(density+0.0)/true_resolution)
    
    
        
    #plots the original and reconstructed signal
        if (plot):
            pl.plot(z_1, label='%s: %.1f reduction ratio,\n%.2f error'
                        % (title, 100*(1-(density+0.0)/resolution), maxmin_error))
           
            
            
        #print "*********recon***********",recon
        ########################################################
        ##########################Add DL reconstructed data to a Databank for deviation calculation and plots...
        reduction_DL=(1-(density+0.0)/resolution) ##Deffinition taken from the label of the plot above...
        recon_values_from_DL=z_1.reshape(len(z_1), 1) ####AngelH: This line is needed to add data to a databank z_1=[0,1,2,3,4...] in the form [[1],[2],[3]...].
        
        recon_values_from_DL=recon_values_from_DL[:len(first_half_timestamps)] ##Earlier some data points were added to the data to be able to divided in equal size segments for the dictionary learning....:S
        
        print "len(recon_values_from_DL)",len(recon_values_from_DL)
        print "len(timestamps_for_rdp)",len(first_half_timestamps)
        
        process_id=Dat.add_processed_sensor_data(sensor_id+1,recon_values_from_DL,first_half_timestamps , datatype='recon_data', meta={'reduction' : reduction_DL,'framesize' : frame_size})
        
        print "process_id",process_id
        ##Deviations 
        deviations=Dat.get_deviations(sensor_id+1,process_id)
        dof=0
        dev_max=deviations['E'][dof]['max']
        dev_mean=deviations['E'][dof]['mean']
        dev_min=deviations['E'][dof]['min']
        rrmse=deviations['RRMSE%'][dof]
        
        reduction_string=' ,reduction:'+str(round(reduction_DL*100,2))
        suptitle='DL, '+ sensor_name+reduction_string+'% ,rrmse:'+str(round(rrmse,2))+'%'+ ' ,dev_max:'+str(round(dev_max,5))+'\n Encoding done in %.2f s'%dt_DL_encoding+' npoints: '+str(len(recon_values_from_DL))
        #' ,pid:'+str(process_id)+' ,sid:'+str(sensor_id)+
        P.plot(sensor_id+1,process_id,deviation='difference',suptitle=suptitle,output='png',dt_format='%H:%M:%S')
        P.plot(sensor_id+1,process_id,deviation='difference',suptitle=suptitle,output='screen',dt_format='%H:%M:%S')
        P.plot(sensor_id+1,process_id,deviation='rel_error',suptitle=suptitle,output='png',dt_format='%H:%M:%S')
        
        Dat.write_sensor_data_files(sensor_id+1)
        Dat.save("Databank_sensor_id_%d.dbk"%sensor_id)   
        ##############################################################################

       
            
        if (comparewithothermethods):
        
            z_dict=np.copy(z_1)
            rate_dict=100*(1-(density+0.0)/resolution)
        
            compare_tol=sparse_tol
        
        #Fixed dictionaries
    
        #DCT
            D_dct = np.array(gen.dct_iii(np.identity(frame_size)))
        
        #Sawtooth
            factor=1e-5 #width factor
            subsampling=100
            n_comp=resolution/subsampling
            D_sawtooth = np.r_[tuple(wavelet_matrix(width=w, resolution=resolution/n_segm,
                                            n_components=np.floor(n_comp / 5))
                        for w in (factor*10, factor*50, factor*250, factor*5*250, factor*25*250))]        
          
          
            print '-'*40
            print 'Comparison'
            for (D_1,title) in zip((D_dct, D_sawtooth), ('DCT', 'Sawtooth')):
                coder = SparseCoder(dictionary=D_1, transform_n_nonzero_coefs=n_nonzero,
                    transform_alpha=compare_tol*compare_tol*variance_b*resolution/n_segm, transform_algorithm=algo, tol_1=tol_1sparse)
                print '%s : ' %title
                print '%s x %s' %(len(D_1), len(D_1[0]))
                x_3=coder.transform(h_b)   
                maxi=np.max(np.abs(x_3))
                z_1=np.ravel(np.dot(x_3,D_1))
                rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
            
                thresh=min_thresh
                x_2=np.copy(x_3)
            
                step=1.0
            
            #Find the right threshold to set
                while ((rsquared_error<100*sparse_tol) and (thresh < max_thresh)):
                    thresh+=step
                    x_2[np.abs(x_2)<thresh/100*maxi]=0
                    z_2=np.ravel(np.dot(x_2,D_1))
                    rsquared_error = tk_err.rrmse(y_b, z_2, norm='std') #RMSE    
            
                while (not(rsquared_error<100*sparse_tol) and (thresh>0)):
                    x_2=np.copy(x_3)
                    thresh-=step
                    x_2[np.abs(x_2)<thresh/100*maxi]=0
                    z_2=np.ravel(np.dot(x_2,D_1))
                    rsquared_error = tk_err.rrmse(y_b, z_2, norm='std')
        
                
                #Set the low coefficients to 0 with the right threshold
                for i in range(n_components):    
                    x_3[np.abs(x_3)<thresh/100*maxi]=0    
                
                #Analyses the results
                density=len(np.flatnonzero(x_3)) #number of non zero coefficients
                z_1=np.ravel(np.dot(x_3,D_1))
                rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
                maxmin_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
                print 'Actual threshold (in percentage) : ', thresh
                print 'RRMSE : ', rsquared_error
                print 'Maxmin_RRMSE : ', maxmin_error
                print 'Infinite norm : ', np.max(np.abs(y_b-z_1))
                print 'Non zero coeffs : ', density
                print 'Compression ratio : ', 100*(1-(density+0.0)/resolution)
                print '-'*40
                if (plot):
                    pl.plot(z_1, label='%s: %.1f reduction ratio,\n%.2f error'
                                % (title, 100*(1-(density+0.0)/resolution), maxmin_error))
                        
                if (title=='DCT'):
                    z_dct=np.copy(z_1)
                    rate_dct=100*(1-(density+0.0)/resolution)
                elif (title=='Sawtooth'):
                    z_sawtooth=np.copy(z_1)
                    rate_sawtooth=100*(1-(density+0.0)/resolution)                    
                    

##############################################################################
                    
        if (plot):                
            pl.axis('tight')
            pl.legend()    
        
        dt = time.time() - t1
        print('done in %.2fs.' % dt)
    
    ###############################################################################
    
    
    #Writes the dictionary in a csv file
        if (write):
            if not os.path.exists('./Dictionaries'):
                os.makedirs('./Dictionaries')
            csvfile = csv.writer(open("Dictionaries/dictionary_%s_sensor_id_%s_ncomp_%s_framesize_%s.csv" %(sensor_name,sensor_id,n_components, frame_size), "wb"), delimiter=';')
            for i in range(len(D[0])):
                csvfile.writerow((D[:,i]))
    
    ###############################################################################
    
    
    ###############################################################################
    #Plot the initialization dictionary
        if (plot_dict):
            if D_0 is not None:
                fi4=pl.figure(4, figsize=(4.2, 4))
                fi4.suptitle('Initialization dictionary', fontsize=18)
                pd.plot_dic(D_0)
    ###############################################################################
    
    ###############################################################################
    #Plot the representation of the initial signal in the new basis
        if (plot_dict):
            fi5=pl.figure(5, figsize=(4.2, 4))
            fi5.suptitle("Coefficients of the signal in the new basis", fontsize=18)
            pd.plot_dic(x_1.T)
    
    ###############################################################################
    
    ###############################################################################
    #Plot an histogram of the appearance of the basic functions
        if (plot):
            x_s=np.nonzero(x_1)
            fi6=pl.figure(6)
            pl.title('Number of appearances of each basic function')
            pl.hist(x_s[1], bins=n_components+1, range=(0, n_components))
            fi7=pl.figure(7)
            pl.title('Number of non-zero coefficients for each segment')
            pl.hist(x_s[0], bins=n_segm, range=(0, n_segm))
            ################################################################################
            fi8=pl.figure(8)
            pl.title('First pattern of the dictionary')
            pl.plot(D[0])
    
        if (plot and comparewithothermethods):
            fi9=pl.figure(9)
            fi9.suptitle('Comparing two methods', fontsize=18)        
        
        
            pl.subplot(1,2,1)
            pl.plot(y_b, ls='dotted', label='Original signal')
            maxmin_error = tk_err.rrmse(y_b, z_dict, norm='maxmin')
            pl.plot(z_dict, label='%s: %.2f reduction ratio,\n%.2f error'
                            % ('DL', rate_dict, maxmin_error)) 
            pl.legend()                    
            pl.subplot(1,2,2)
            pl.plot(y_b, ls='dotted', label='Original signal')
            maxmin_error = tk_err.rrmse(y_b, z_dct, norm='maxmin')
            pl.plot(z_dct, label='%s: %.2f reduction ratio,\n%.2f error'
                                % ('DCT', rate_dct, maxmin_error)) 
            pl.legend()
    
        if (plot):
            pl.show()
        
        
    
