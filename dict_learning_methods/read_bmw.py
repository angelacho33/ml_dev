# -*- coding: utf-8 -*-
"""
Created on Fri Jul 01 10:04:57 2016

@author: Etienne
"""

import numpy as np
from tk_gdfr import get_ts_value_from_client_sensor_file #reading routine for alstom datasets



def rightlane_from_file(filename):
        D = get_ts_value_from_client_sensor_file(filename, ',', 'cid:0', 
                                             'sin', 0, [7])
        values = D['cid:0']['sin']['values']
        z_c=np.concatenate(values)
        y=[]
        for i in range(len(z_c)-1):
            if (z_c[i+1]!=' null'):
                y.append(np.float(z_c[i+1]))
        #        else:
        #            y.append(0)            #if we want to replace the nulls with 0
        return(np.array(y))
        
def leftlane_from_file(filename):
        D = get_ts_value_from_client_sensor_file(filename, ',', 'cid:0', 
                                             'sin', 0, [5])
        values = D['cid:0']['sin']['values']
        z_c=np.concatenate(values)
        y=[]
        for i in range(len(z_c)-1):
            if (z_c[i+1]!=' null'):
                y.append(np.float(z_c[i+1]))
        #        else:
        #            y.append(0)            #if we want to replace the nulls with 0
        return(np.array(y))