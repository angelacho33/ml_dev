# -*- coding: utf-8 -*-
"""
Created on Tue Aug  9 11:30:33 2016

@author: root

This routine allows to write the print calls in a txt file instead of the terminal.
"""

import subprocess

if __name__ == '__main__':
    prog = 'encode_decode_example_Airbus'
    output = 'Airbus_tol001_framesize50'    
    
    cmd = ['python', '../Used Scripts/'+prog+'.py']
    
    with open('../Output from terminal/'+output+ '.txt', 'w') as out:
        return_code = subprocess.call(cmd, stdout=out)
        print prog + ' output saved in ' + output
