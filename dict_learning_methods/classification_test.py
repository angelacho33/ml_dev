# -*- coding: utf-8 -*-


"""
===========================================
Dictionary learning
===========================================

Trying to detect when the signal is changing.
At one point, the signal is no more Certuss Data, but a sines generated signal.
"""


##############################################################################
#Importation

import time
from ksvd import KSVD
import numpy as np
import matplotlib.pylab as pl
from sklearn.decomposition import SparseCoder
from sklearn.decomposition import DictionaryLearning
import plot_dictionary as pd
import csv
from dict_analysis import dict_read
import dictionaries_generation as gen
import Fourier_filter as ffl
import os
import tk_err
import tk_gdfr
from tk_databank import DataBank
from tk_plot import Plot
from compression import compr_float
from label_classification import complete_signal 

#def wavelet_matrix(width, resolution, n_components):
#    """Dictionary of selected wavelets"""
#    D=gen.sawtooth_matrix(width, resolution, n_components)
#    return D
#    

def averaging(x, frame=10):
    res=np.zeros(pd.sup_int(len(x)/frame))
    for i in range(len(res)):
        res[i]=np.mean(x[frame*i:frame*(i+1)])
    return res
    
if __name__ == '__main__':
    
    Dat = DataBank()  #call it Dat because D represents the Dictionary in what follows
    P = Plot(Dat)
     
    
    frame=[40]
    comp=[40]
    timelist=np.zeros((len(frame), len(comp)))
    thresholdlist=np.zeros((len(frame), len(comp)))
    convergencelist=np.zeros((len(frame), len(comp)))
    i_frame=0
    
    ##############################################################################
    #Generation of the signal
    print(__doc__)
    #Generates a signal
    value_names = 'temperature'
    file = '../Data/0013__certuss_Speisung__Speisewassertemperatur Kesseleintritt.csv'
    file2= '../Data/0012__certuss_Speisung__Sollwert Speisepumpe.csv'
    
    sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ';', 'pos', 'pos', 0, [1])
    sensor_data2 = tk_gdfr.get_ts_value_from_file(file2, ';', 'pos', 'pos', 0, [1])
    values2=np.concatenate(sensor_data2['pos']['values'])
    values2=values2[:100000]
   
   #    Options
    plot=True
    write=False
    restart=True
    notrainrestart=True
    svd_init=True
    class_regression=True
    corr_classification=True
  
    sensor_id=0
    ##Add data to a databank
    S1 = Dat.add_sensor_data(sensor_data1['pos']['values'][1:],
                           sensor_data1['pos']['tss'][1:], sensor_id, "velocity", "pos",value_names=value_names)
					  
#    Dat.sensor_raw_data_clean(sensor_id, 20)
    
    #D.write_sensor_data_files(sensor_id)
    
    
    ####################   Interpolation Daniel implemented
    #Raw data info
    raw_data=Dat.get_sensor_raw_data(sensor_id)
    tss=np.asarray(raw_data['tss'])
    values=np.asarray(raw_data['values'])
    npoints=raw_data['count']
    
    
    data_tmp = values
    #print data_tmp
    data_tmp = [x[0] for x in data_tmp]
#    print data_tmp
    data_tmp = np.asarray(data_tmp)
    filename = "../Generated signals/generated_sin_SNR_20_period_50.0_M_10_k_7_ch_0.5.csv" #smallest file
    sensor_name = 'sin'
    H = tk_gdfr.get_ts_value_from_client_sensor_file(filename, ';', 'cid:0', 
                                             sensor_name, 0, [1])
    values = H['cid:0'][sensor_name]['values']
    y=data_tmp
    y_b=y[len(y)/2:]#testing dataset
    y=-15*np.concatenate(values)
    y+=800
    y=np.concatenate((y, 2*np.concatenate(values)+800)) 
    
    y_t=complete_signal(y_b, frame[0])
    y_c=complete_signal(y, frame[0])
    
    print ('Length of testing signal : '+str(len(y_t)))
    print ('Length of comparison signal : '+str(len(y_c)))
    
    data_tmp=np.concatenate((y_t, y_c))
    
    #data_tmp = data_tmp[0:,1]#.tolist()

    #data_tmp = []
    #for row in data.value:
    #    data_tmp.append(row)
    ##    data_codename_all.append(row)
    ##        if row not in CodeNames_All:
    ##           CodeNames_All.append(row)
    
#    print "This is the new dataset",data_tmp
    print "This is the dataset length",len(data_tmp)
    
#    y=data_tmp
    
    
    for frame_size in frame:
        
        i_comp=0
        
        for n_components in comp:
            
            print '-'*40
            print 'New iteration'
            
            
            y=data_tmp
            y_b=y#testing dataset
            y_1=y[0:len(y)/2] #training dataset
            

                
            ##############################################################################
            #Different parameters
            
        #    Options
            plot=True
            plot_dict=False
            multiple_plot=False
            write=False
            restart=True
            notrainrestart=True
            svd_init=True
            comparewithothermethods=False
            
            #adjustable parameters
    #        frame_size=75
#            n_components = 100 #number of basic functions in the dictionary
            tol=np.sqrt(0.01)
            sparse_tol=np.sqrt(0.01)
            compare_tol=np.sqrt(0.01)
            sparsity_controlling_parameter=1.
            num_error=-1
            max_thresh=100.0
            min_thresh=0.0
            max_deviation=100
            tol_1train=max_deviation
            tol_1sparse=max_deviation
            fit_algo = 'omp'
            fit_thresh=5.
            n_iter=500
            n_nonzero_coefs_dict=None
            n_nonzero_coefs_sparse=None
            
            #Other 
            
            resolution = len(y_1);
            n_segm = pd.sup_int((resolution+0.0)/frame_size); #is the number of segments which decompose the original signal (which is here the concatenation of 2 signals)
            
            if (max_deviation==None):
                max_deviation=np.infty
            #avoid the edges problems
            
            if (resolution%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
                for j in range (frame_size-(resolution%frame_size)):
                    y_1=np.concatenate((y_1, [y_1[resolution-1]]))
                   
            
            variance_1=(np.mean(y_1**2)-np.mean(y_1)**2) 
            
            
             
            ##############################################################################
            #Printing
                   
            print 'Frame size : ', frame_size
            print 'Number of segments : ', n_segm
            print 'Number of components : ', n_components
            print 'Training Tolerance : ', tol  #maximal authorized RMSE 
            print 'Sparse coding Tolerance : ', sparse_tol
            print 'Sparsity controlling parameter : ', sparsity_controlling_parameter
            print 'Numerical Error Tolerance : ', num_error
            print 'Maximal number of iterations : ', n_iter
            print 'Maximum Threshold (in percentage) : %s' % max_thresh
            print 'Minimum Threshold (in percentage) : %s' % min_thresh
            print 'Allowed testing deviation : %s (omp) & %s (thresholding)' %(tol_1sparse, max_deviation)
            print 'Allowed training deviation : %s' %(tol_1train)
            print 'Non zero coefficients allowed during the training phase : ', n_nonzero_coefs_dict
            print 'Non zero coefficients allowed during the sparse coding phase: ', n_nonzero_coefs_sparse
            resolution = len(y_1);          
            t0 = time.time()
            
            ##############################################################################
            #Defines the useful functions/methods
             
            #Filters the signal
            y=y_1
             
            #a list of the different methods we can choose (omp seems to be the best at the moment)
            estimators = [('OMP', 'omp', tol*tol*variance_1*resolution/n_segm, n_nonzero_coefs_sparse), 
                          ('LASSO CD', 'lasso_cd', 1., None)]
            
                    
                    
            h=np.zeros((n_segm, int(len(y)/n_segm))) #will represent the different segments of y, is initialized further
            for l in range(n_segm):
                h[l]=(y[l*resolution/n_segm:(l+1)*resolution/n_segm]) #h represents now the n_segm segments dividing the y signal
            
            
            
            
            
            
            ##############################################################################
            #Dictionary initialization step
            
            if (restart):
                #last result
                title, algo, alpha, n_nonzero = estimators[0]  
                filename_2='../Dictionaries/dictionary_steam1_%s_%s_%s.csv' % (fit_algo, n_components, frame_size)
                try:
                    D_0=dict_read(filename_2)
                    coder_0 = SparseCoder(dictionary=D_0, transform_n_nonzero_coefs=n_nonzero,
                                    transform_alpha=alpha, transform_algorithm=algo, tol_1=tol_1train)
                    c_0=coder_0.transform(h) 
                    print 'Restart initialization : ', filename_2
                except IOError as e:
                    print "Unable to open file" #Does not exist OR no read permissions
                    print 'Initialization : DCT'
                    D_0=np.array(gen.dct_iii(np.identity(frame_size)))
                    coder_0 = SparseCoder(dictionary=D_0, transform_n_nonzero_coefs=n_nonzero,
                                    transform_alpha=alpha, transform_algorithm=algo, tol_1=tol_1train)
                    c_0=coder_0.transform(h)
                    restart=False
                    notrainrestart=False
            elif(svd_init):
                print 'Initialization : svd'
                D_0,c_0=KSVD(h, n_components, frame_size/3, 5, enable_threading=False)
            else:
                #No initialization
                print 'Initialization : None'
                D_0=None
                c_0=None
            
            ##############################################################################
            
            
            
            
                     
            #Setting the final parameters
            title, algo, alpha, n_nonzero = estimators[0]   
            
            if (fit_algo=='omp'):
                fit_alpha=alpha
            elif(fit_algo=='threshold'): 
                fit_alpha=fit_thresh
            else: 
                fit_alpha=sparsity_controlling_parameter*np.sqrt(variance_1)
            
            print 'Fit algorithm : ', fit_algo   
            print title 
            
            if (restart and notrainrestart):
                D=D_0
                x=c_0
                print '-'*40
                print 'Training phase : loaded the previous dictionary'
                print 'Variance : ', variance_1 
                
            else:         
                #Dictionary Learning step
                print '-'*40
                print 'Training phase :'
                print "Resolution : ", resolution
                print 'Variance : ', variance_1 
                dictio = DictionaryLearning(n_components=n_components, alpha=sparsity_controlling_parameter*np.sqrt(variance_1), 
                                            max_iter= n_iter, tol=num_error,
                                            fit_algorithm=fit_algo, transform_algorithm=algo, transform_n_nonzero_coefs=n_nonzero, transform_alpha=alpha, n_jobs=1, 
                                            code_init=c_0, dict_init=D_0, verbose=False, 
                                            split_sign=False, random_state=None, tol_2=alpha*10, tol_1=tol_1train, n_nonzero_coefs=n_nonzero_coefs_dict)
                                            
                                            #On this step is the dictionary learned, and x is then transformed in the new basis
                x=dictio.fit_transform(h)
                        
                #Analyses the results        
                D=dictio.components_ #here is the final dictionary
                
                fit_algo='omp'
                
                dictio = DictionaryLearning(n_components=n_components, alpha=sparsity_controlling_parameter*np.sqrt(variance_1), 
                                            max_iter= n_iter, tol=num_error,
                                            fit_algorithm=fit_algo, transform_algorithm=algo, transform_n_nonzero_coefs=n_nonzero, transform_alpha=alpha, n_jobs=1, 
                                            code_init=x, dict_init=D, verbose=False, 
                                            split_sign=False, random_state=None, tol_2=alpha, tol_1=tol_1train, n_nonzero_coefs=n_nonzero_coefs_dict)
                                    
                x=dictio.fit_transform(h)
                D=dictio.components_
                
                
            density = len(np.flatnonzero(x)) #the number of non zero coeffs    
            z = np.ravel(np.dot(x, D)) #x is then the reconstructed signal in the original basis
            rsquared_error = tk_err.rrmse(y, z, norm='std') #RRMSE
            maxmin_error = tk_err.rrmse(y, z, norm='maxmin') 
            
            #Printing
            print 'RRMSE : ', rsquared_error
            print 'Maxmin RRMSE : ', maxmin_error
            print 'Non zero coeffs : ', density
            
                      
            dt = time.time() - t0
            print('done in %.2fs.' % dt)
            #plots the original and reconstructed signal
          
            
            
            ###############################################################################
            # Plot the generated dictionary

            
            ###############################################################################
            
            ##############################################################################
            #Generates the signal to test
            
            t1=time.time()
            
            
            resolution = len(y_b);
            n_segm=pd.sup_int((resolution+0.0)/frame_size); #is the number of segments which decompose the original signal
            if (resolution%frame_size>0):
                for j in range (frame_size-(resolution%frame_size)):
                    y_b=np.concatenate((y_b, [y_b[resolution-1]]))
                    
            resolution = len(y_b); 
            
            h_b=np.zeros((n_segm, frame_size))
            h=np.zeros((n_segm, frame_size))
            
            
            
            for l in range(n_segm):
                h_b[l]=(y_b[l*frame_size:(l+1)*frame_size])
            
            
            variance_b=(np.mean(y_b**2)-np.mean(y_b)**2)
            
            ##############################################################################
            #Start of the testing phase
            
            #Defines the method
            coder = SparseCoder(dictionary=D, transform_n_nonzero_coefs=n_nonzero,
                                transform_alpha=sparse_tol*sparse_tol*variance_b*resolution/n_segm, transform_algorithm=algo, tol_1=tol_1sparse)
                                                    
             
            print '-'*40
            print 'Testing phase :'        
            print 'Variance : ', variance_b
            
            if (plot):
                fi3=pl.figure(3, figsize=(13, 6))
                pl.title('Testing dataset')
                print "Resolution : ", resolution
                pl.plot(y_b, label='Original signal')
            
            
            #on this line, x is the determined to minimize ||h-Dx|| with the condition that
            #every array of x has less than n_z non zero coefficients (for the omp method)
            t2=time.time()
            x_1=coder.transform(h_b) 
            
            #Thresholding phase
            t3 = time.time()
            #Intialize
            maxi=np.max(np.abs(x_1))
            z_1=np.ravel(np.dot(x_1,D))
            rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
            deviation=np.max(np.abs(y_b-z_1))
            thresh=min_thresh
            x_2=np.copy(x_1)
            step=1.0
            #Find the right threshold to set
            while ((rsquared_error<=100*sparse_tol) and (thresh < max_thresh) and deviation<=max_deviation):
                thresh+=step
                x_2[np.abs(x_2)<thresh/100*maxi]=0
                z_2=np.ravel(np.dot(x_2,D))
                rsquared_error = tk_err.rrmse(y_b, z_2, norm='std') #RMSE
                deviation=np.max(np.abs(y_b-z_2))
            
            while ((rsquared_error>100*sparse_tol or deviation>max_deviation) and (thresh>0) ):
                x_2=np.copy(x_1)
                thresh-=step
                x_2[np.abs(x_2)<thresh/100*maxi]=0
                z_2=np.ravel(np.dot(x_2,D))
                rsquared_error = tk_err.rrmse(y_b, z_2, norm='std')
                deviation=np.max(np.abs(y_b-z_2))
                
            #Set the low coefficients to 0 with the right threshold
            x_1[np.abs(x_1)<thresh/100*maxi]=0
#            for i in range(len(x_1)):
#                for j in range(len(x_1[i])):
#                    x_1[i,j]=np.int(x_1[i,j])
#            
            t4=time.time()-t3 
            t5=time.time()-t2
            print('Thresholding phase done in %.2fs.' % t4)        
            print ('Sparsecoding phase done in %.2fs.' % t5)
            
            ##############################################################################
                
            
            #Analyses the results
            density=len(np.flatnonzero(x_1)) #number of non zero coefficients
            z_1=np.ravel(np.dot(x_1,D))
            rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
            maxmin_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
            print 'Actual threshold (in percentage) : ', thresh
            thresholdlist[i_frame,i_comp]= thresh
            print 'RRMSE : ', rsquared_error
            print 'Maxmin_RRMSE : ', maxmin_error
            print 'Infinity norm : ', np.max(np.abs(y_b-z_1))
            timelist[i_frame,i_comp]=t5
            convergencelist[i_frame,i_comp]=(np.max(np.abs(y_b-z_1))<=max_deviation) and (rsquared_error<=100*sparse_tol)
            print '(%s, %s)' %(i_frame,i_comp)
            print 'Non zero coeffs : ', density
            print 'Compression ratio : ', 100*(1-(density+0.0)/resolution)
            
            
        
            #plots the original and reconstructed signal
            if (plot):
                pl.plot(z_1, ls='dashed', label='%s: %.1f reduction ratio,\n%.2f error'
                            % (title, 100*(1-(density+0.0)/resolution), maxmin_error))
                   
        ##############################################################################
        
               
            y_d=y_b
            z_dict=np.copy(z_1)           
            if (comparewithothermethods):
                
                
                z_dict=np.copy(z_1)
                rate_dict=100*(1-(density+0.0)/resolution)
                
                
                #Fixed dictionaries
            
                #DCT
                D_dct = np.array(gen.dct_iii(np.identity(frame_size)))
                
                #Sawtooth
                factor=1e-5 #width factor
                subsampling=300
                n_comp=resolution/subsampling
                D_sawtooth = np.r_[tuple(wavelet_matrix(width=w, resolution=resolution/n_segm,
                                                n_components=np.floor(n_comp / 5))
                            for w in (factor*10, factor*50, factor*250, factor*5*250, factor*25*250))]        
                  
                  
                print '-'*40
                print 'Comparison'
                for (D_1,title) in zip((D_dct, D_sawtooth), ('DCT', 'Sawtooth')):
                    coder = SparseCoder(dictionary=D_1, transform_n_nonzero_coefs=n_nonzero,
                        transform_alpha=compare_tol*compare_tol*variance_b*resolution/n_segm, transform_algorithm=algo, tol_1=tol_1sparse)
                    print '%s : ' %title
                    print '%s x %s' %(len(D_1), len(D_1[0]))
                    x_3=coder.transform(h_b)   
                    
                    maxi=np.max(np.abs(x_3))
                    z_1=np.ravel(np.dot(x_3,D_1))
                    rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
                    deviation=np.max(np.abs(y_b-z_1))
                    thresh=min_thresh
                    x_2=np.copy(x_3)
                    step=1.0
                    
                    #Find the right threshold to set
                    while ((rsquared_error<=100*compare_tol) and (thresh < max_thresh) and deviation<=max_deviation):
                        thresh+=step
                        x_2[np.abs(x_2)<thresh/100*maxi]=0
                        z_2=np.ravel(np.dot(x_2,D_1))
                        rsquared_error = tk_err.rrmse(y_b, z_2, norm='std') #RMSE
                        deviation=np.max(np.abs(y_b-z_2))
                    
                    while ((rsquared_error>100*compare_tol or deviation>max_deviation) and (thresh>0) ):
                        x_2=np.copy(x_3)
                        thresh-=step
                        x_2[np.abs(x_2)<thresh/100*maxi]=0
                        z_2=np.ravel(np.dot(x_2,D_1))
                        rsquared_error = tk_err.rrmse(y_b, z_2, norm='std')
                        deviation=np.max(np.abs(y_b-z_2))
                
                        
                    #Set the low coefficients to 0 with the right threshold
                    x_3[np.abs(x_3)<thresh/100*maxi]=0    
                        
                        #Analyses the results
                    density=len(np.flatnonzero(x_3)) #number of non zero coefficients
                    z_1=np.ravel(np.dot(x_3,D_1))
                    rsquared_error = tk_err.rrmse(y_b, z_1, norm='std') #RMSE
                    maxmin_error = tk_err.rrmse(y_b, z_1, norm='maxmin')
                    print 'Actual threshold (in percentage) : ', thresh
                    print 'RRMSE : ', rsquared_error
                    print 'Maxmin_RRMSE : ', maxmin_error
                    print 'Infinity norm : ', np.max(np.abs(y_b-z_1))
                    print 'Non zero coeffs : ', density
                    print 'Compression ratio : ', 100*(1-(density+0.0)/resolution)
                    print '-'*40
                    if (plot):
                        pl.plot(z_1, label='%s: %.1f reduction ratio,\n%.2f error'
                                    % (title, 100*(1-(density+0.0)/resolution), maxmin_error)) 
                    
                    if (title=='DCT'):
                        z_dct=np.copy(z_1)
                        rate_dct=100*(1-(density+0.0)/resolution)
                    elif (title=='Sawtooth'):
                        z_sawtooth=np.copy(z_1)
                        rate_sawtooth=100*(1-(density+0.0)/resolution)                    
        

            
            ###############################################################################
            
            
            #Writes the dictionary in a csv file
            if (write):
                folder_write='../Dictionaries'
                file_path = folder_write+"/dictionary_steam1_%s_%s_%s.csv" %(fit_algo, n_components, frame_size)
                if not os.path.exists(folder_write):
                   os.makedirs(folder_write)#
                csvfile = csv.writer(open(file_path, "wb"), delimiter=';')
                for i in range(len(D[0])):
                    csvfile.writerow((D[:,i]))
            
                folder_write='../Reconstructed signals'
                file_path = folder_write+"/reconstructed_signal_steam1_%s_%s_%s.csv" %(fit_algo, n_components, frame_size)
                if not os.path.exists(folder_write):
                   os.makedirs(folder_write)#
                csvfile = csv.writer(open(file_path, "wb"), delimiter=';')
                for i in range(len(z_dict)):
                    csvfile.writerow(['%.2f' % z_dict[i]])
            
            
            if (plot):
                x_s=np.nonzero(x_1)
                fi7=pl.figure(7)
                pl.title('Number of non-zero coefficients for each segment')
                pl.hist(x_s[0], bins=n_segm, range=(0, n_segm))
            averaged_frame=10
            if corr_classification:
               
                from label_classification import correlation
                corr=correlation(x_1, numberofmax=3)
                moy_corr=averaging(corr, frame=averaged_frame)
                classif=-np.ones(moy_corr.shape)
                classif[moy_corr>=0.8]=1                
                theoric_curve=np.concatenate((np.ones(len(y_t)/(averaged_frame*frame_size)), -np.ones(len(y_c)/(averaged_frame*frame_size))))
                score_corr=np.mean(classif==theoric_curve)
                
            if plot:
                fi4=pl.figure()
                pl.title('Correlation classification, score ' + str(score_corr))
                pl.plot(moy_corr, label='Predicted labels')
                pl.plot(classif, label='Classification')
                pl.plot(theoric_curve, label='Theoric labels')
                pl.ylim(ymin=-1.2, ymax=1.2)
                pl.legend()
                
        if class_regression:
            t9=time.time()
#            from label_classification import training_signal_ard
            from label_classification import training_signal_logistic
            print 'Supervised regression ...'
            x_t=x_1[:len(y_t)/frame_size]
            x_c=x_1[len(y_t)/frame_size:]
#            coef, alpha, lam, sigma, pred, sc=training_signal_ard(x_t, x_c, score=True)
            y_t=y_t.reshape(len(y_t)/frame_size, frame_size)
            y_c=y_c.reshape(len(y_c)/frame_size, frame_size)
            w_0, w, pred, sc=training_signal_logistic(x_t, x_c, score=True, n_iter=1000, tol=1e-9, solver='lbfgs')
            
            predict=averaging(pred, frame=averaged_frame)
            pl.figure()
            pl.title('Classification Prediction, score ' + str(sc))
            pl.plot(predict, label='Predicted labels')
            pl.plot(theoric_curve, label='Theoric labels')
            pl.ylim(ymin=-1.2, ymax=1.2)
            pl.legend()
            print 'Done in %.2fs' %(time.time()-t9)
           
            if plot:
                pl.show()

            i_comp+=1
            
        i_frame+=1

    print 'Convergence list :'
    print convergencelist
    print 'Time list :'
    print timelist        
    print 'Threshold list :'
    print thresholdlist
