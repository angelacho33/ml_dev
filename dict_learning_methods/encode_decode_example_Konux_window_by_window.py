# -*- coding: utf-8 -*-
"""
"""

from dict_learning_methods.tk_dictionary_encoder import SignalDictionaryEncoder
from dict_learning_methods.tk_dictionary_decoder import SignalDictionaryDecoder
import tk_gdfr
from tk_databank import DataBank
import dict_learning_methods.plot_dictionary as pd
import matplotlib.pylab as pl
from tk_plot import Plot
import numpy as np
from dict_learning_methods.dict_analysis import dict_read 
import sys,os
from dict_learning_methods.Differences_plots import diff_hist
import gc

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in xrange(0, len(l), n):
        yield l[i:i + n]

if __name__ == '__main__':
	
    import glob
    import os
    import csv
    import matplotlib
    #matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.
    
   
    plot_dict=False
    
    
    #file = 'Data/BMW/%s_data_%s.csv' %(value_names, numberofdata)
    
    #file = '../Data/BMW_Velocity/806c4190-98b5-4812-bd47-365f3bdca01e-velocity.csv'
    #folder='/home/angelh/Documents/DataScienceRelated/Teraki/AirBus_new_data/Files/'
    #folder='../Data/Airbus/'
    #folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/'
    #filenamelist = sorted(glob.glob(folder+'/*long_slice.csv'),key=os.path.getsize,reverse=True)
    
    file='/home/angelh/Documents/DataScienceRelated/Teraki/Konux/Files/train_sample_4.csv'

    #axis=3
    #print filenamelist
    #file = filenamelist[0]
    #sensor_id = 0
    
    reduction_list = []
    sigma=[]    
    write = True
    
    all_sensors = ['Axis_0',
                   'Axis_1',
                   'Axis_2',
                   'Axis_3',
               ]
    ##For over Axis and then chunks to add data to a DataBank
    for index,sensor in enumerate(all_sensors):
        sensor_id=index
        real_sensor_id=sensor_id
        sensor_name=sensor
        value_names= [sensor_name]
        if sensor_name!='Axis_3': ##Just add Axis3 data
            continue
        sensor_data = tk_gdfr.get_ts_value_from_file(file, ',', sensor_name, sensor_name, 0, [index])
        #values=sensor_data[sensor_name]['values'][:13000] #108000
        values=sensor_data[sensor_name]['values']
        all_data_values=values
        #Note: The current file has no time stamps at column zero as asumed in get_ts_value so the next line is commented
        #And timestamps are created after
        #timestamps=sensor_data[sensor_name]['tss'] 
        len_data=len(values)
        #timestamps=np.arange(0,len_data)*1000 
        timestamps=np.arange(0,len_data)
        
        number_of_points_per_segment=2000 ##Number of points in each data chunk/segment in which the all data is segmented.
        chunks_of_values=list(chunks(values,number_of_points_per_segment))
        chunks_of_timestamps=list(chunks(timestamps,number_of_points_per_segment))
        
        #Add each chunk to a DataBank
        Dat=DataBank()
        P=Plot(Dat)
        print "The data is being segmented in chunks of size: %d"%number_of_points_per_segment
        for chunk_index,chunk in enumerate(chunks_of_values):
            print "Adding chunk: ", chunk_index, 'to a DataBank' 
            sensor_id=chunk_index 
            Dat.add_sensor_data(chunk,chunks_of_timestamps[chunk_index], sensor_id, sensor_name, sensor_name,value_names=value_names)
            #P.plot_raw_inspection(sensor_id, output='png', fft_yscale='linear', dt_format='%H:%M:%S', sw_rmse=False)
            
            
    #sensor_list = [0,1,2,3] ##Each sensor is an Axis...
    #sensor_list = [3] ##Just Axis 3.
    
    all_reductions=[]
    all_rrmses=[]
    red_points=[]
    npoints_in_segment=[]
    all_recon_arrays=[]
    
    #for sensor_id in sensor_list: 
    for sensor in  Dat.get_sensor_list(): ##Loop over data_chunks added to a databank above..each chunk has a sensor_id=chuck_index
        index=sensor['index']
        dof=sensor['dof']
        sensor_id=sensor['sensor_id'] ##The sensor_id in this loop is the chunk_id not the sensor_id of the real data
        sensor_name=sensor['sensor_name']
        
        #Dat = DataBank()  #call it Dat because D represents the Dictionary in what follows
        #P = Plot(Dat)
        #print '-'*40
        print 'Sensor_id : ', sensor_id
        frame_size=50
        n_components=frame_size
        #print file
        

        #sensor_id=sensor_id + 1
        #sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [sensor_id])
        #all_values=sensor_data[sensor_name]['values']
        
        #npoints_chunk=13000 #0:13000 is the first segment for Axis3 with strong signal...

        ##0:len_data(sensor_data1): All data
        #npoints_chunk_init=0
        #npoints_chunk_end=len(sensor_data1['pos']['values'])
        
        ##0:13000 ~first segment with "strong" signals
        #npoints_chunk_init=0
        #npoints_chunk_end=13000
        
        ##13000:23000 ~segment Konux mail plot
        #npoints_chunk_init=13000
        #npoints_chunk_end=23000
        
        
        #data_chunk=sensor_data1['pos']['values'][npoints_chunk_init:npoints_chunk_end]
        ##Fake time stamps in ms assuming a sampling rate of 1000 Hz
        #len_data=len(sensor_data1['pos']['values'])
        #len_data=len(data_chunk)
        #timestamps=np.arange(0,len_data)*1000 
        #timestamps=np.arange(0,len_data) #An array of timestamps each ms.
        
        #hh=sensor_data1['pos']['values']
        
        #       
        #value_names=sensor_data1['pos']['values'][0]
        #S1 = Dat.add_sensor_data(sensor_data1['pos']['values'][2:],
        #                       clean_tss, sensor_id, "velocity", "pos",value_names=value_names)
        
        #value_names = ['Axis']
        #S1 = Dat.add_sensor_data(sensor_data1['pos']['values'],
        #                         timestamps, sensor_id, "Axis %d"%sensor_id, "pos",value_names=value_names)
        #S1 = Dat.add_sensor_data(data_chunk,
        #                         timestamps, sensor_id, "Axis %d"%sensor_id, "pos",value_names=value_names)
        
        
        #Dat.sensor_raw_data_clean(sensor_id, 20)
        #D.write_sensor_data_files(sensor_id)
        #Raw data info
        raw_data=Dat.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        
        maxmin=np.max(values)-np.min(values) ##Amplitude of each chunk
        
        print '********************************************'
        print "sensor_id:%d , maxmin=%f"%(sensor_id,maxmin)
        print '********************************************'
        
        npoints=raw_data['count']
        
        ###From here exactly the same code from the original:encode_decode_example_Airbus.py i.e DL generation, encoding and decoding...
    
        data_tmp = values
        #print data_tmp
        data_tmp = [x[0] for x in data_tmp]
        #        print data_tmp
        data_tmp = np.asarray(data_tmp)
        #        print data_tmp
    
        #data_tmp = data_tmp[0:,1]#.tolist()

        #data_tmp = []
        #for row in data.value:
        #    data_tmp.append(row)
        ##    data_codename_all.append(row)
        ##        if row not in CodeNames_All:
        ##           CodeNames_All.append(row)
        
        
        ######Note!Is the next really needed????? There is already a function inside DictLearning routines to "complete" the data...hmm
#       print "This is the new dataset",data_tmp
        print "This is the dataset length",len(data_tmp)
        len_recon=len(data_tmp)     
        if (len_recon%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
            for j in range (frame_size-(len_recon%frame_size)):
                data_tmp=np.concatenate((data_tmp, [data_tmp[len_recon-1]]))
       
        #loadthedictionary
        if not os.path.exists('./Dictionaries'):
            os.makedirs('./Dictionaries')
        ##Note: real_sensor_id is the id of a given sesor all data, not the sensor_id of the chunk of data 
        file_dict='./Dictionaries/Dictionary_'+'konux'+'_sensorid'+str(real_sensor_id)+'_omp_%s_%s.csv' % (n_components, frame_size)
        try:
            D=dict_read(file_dict)
            print 'Previous trained Dictionary found...using it...'
        except IOError as e:
            ####################################################################################################
            ####################################################################################################
            #############NOTE!!!! If a Dictionary does not exist the next lines inside the except will create a Dict based in all_data_values
            ####################################################################################################
            ####################################################################################################
            print "Unable to open file" #Does not exist OR no read permissions
            
            print 'Training the dictionary ...'
            config_train={
                'engine_id': "encoding_0",
                'engine_type': "encoder",
                'engine_layer': "signal",
                'did': 1,
                'f' : frame_size,
                'tol_2': 0.005,
                #'tol_1': 0.01*maxmin,
                'tol_1': None,
                'tol' : -1,
                'n_iter' : 50,
                'n_components' : frame_size,
                'factor' : 10,
                'n_nonzero': None,
                'init' : 'svd'}                 
            from dict_learning_methods.tk_dictionary_learning import DictionaryTraining
            dictio=DictionaryTraining(config_train)
            ##all_data_values was created in the first loop above when data is segmented and added to a DataBank
            all_data=np.concatenate(all_data_values) 
            y_1=all_data[:len(all_data)/2]            
            D=dictio.fit(y_1)
            print 'Dictionary trained.'
    
            if (plot_dict):
                if D is not None:
                    fi4=pl.figure(4, figsize=(4.2, 4))
                    fi4.suptitle('Initialization dictionary', fontsize=18)
                    pd.plot_dic(D)
	       #Writes the dictionary in a csv file
            if (write):
                #file_dict='../Dictionaries/dictionary_sensorid'+str(sensor_id)+'_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
                with open(file_dict, "wb") as f:
                    csvfile = csv.writer(f, delimiter=';')
                    for i in range(len(D[0])):
                        csvfile.writerow((D[:,i]))
                f.close()    
        
        config_enc={'engine_id': "encoding_0",
                    'engine_type': "encoder",
                    'engine_layer': "signal",
                    'did': 1,
                    'f': frame_size,
                    'd': D,
                    'tol_2': 0.01,
                    #'tol_1': 0.01*maxmin,
                    'tol_1': None,
                    'n_nonzero': None}    
        encoder=SignalDictionaryEncoder(config_enc)
    
        red,_=encoder.single_encoding(data_tmp)
#       red=np.array(red)
    
#        print "This is the dataset length",len(data_tmp)
#        print "This is the reduced data shape", red.shape
        reduction_ratio = (1-red.shape[0]/float(len(data_tmp)))*100
        #print "Reduction ratio", (1-red.shape[0]/float(len(data_tmp)))*100
	   
        all_reductions.append(reduction_ratio)
        red_points.append(red.shape[0])
        npoints_in_segment.append(len(data_tmp))#each entry of this array should be equal to number_of_points_per_segment
        
        print "This is the data",data_tmp
        print "This is the reduced data",red.shape
        
        reduction_list.append(reduction_ratio)
        print "Mean reduction",np.mean([reduction_list])
    
    
        config_dec={'engine_id': "encoding_0",
                    'engine_type': "encoder",
                    'engine_layer': "signal",
                    'did': 1,
                    'f': frame_size,
                    'd': D}
        decoder=SignalDictionaryDecoder(config_dec)
        recon,_=decoder.single_decoding(red, length=len(data_tmp))
        recon=recon[:len_recon]
        
        print "*********recon***********",recon

        process_id=Dat.add_processed_sensor_data(sensor_id, recon, tss, datatype='recon_data', meta={'reduction' : reduction_ratio,'framesize' : frame_size})
        all_recon_arrays.append(recon)
        deviations=Dat.get_deviations(sensor_id=sensor_id, process_id=process_id)   
        
        variance_b=np.mean(data_tmp**2)-np.mean(data_tmp)**2
    
        h=data_tmp.reshape(len(data_tmp)/frame_size, frame_size)
        
        variance_seg=(np.mean(h**2, axis=1)-np.mean(h, axis=1)**2)
        variance_seg=np.mean(variance_seg)
    
        maxmin_b=np.max(data_tmp)-np.min(data_tmp)  
        
        sigma0=variance_b/(maxmin_b**2)
        
#        _,_,sigma0=diff_hist(data_tmp, filename='../figures/Airbus/diff_hist_senorid_'+str(sensor_id)+'.png', title='Difference hist, sensor id ' + str(sensor_id)+', red '+ str(reduction_ratio),
#                  relative=True, bins=500)
#                  
        sigma.append(sigma0)
        #Dat.save("Konux_%s_%s.dbk" %(value_names[0], sensor_id))   
        
        #print 'Databank has been saved.'
        print
        print 'Sensor_name:',sensor_name
        print 'Data Segment:',sensor_id
        print 'This is the reduction ratio : ', reduction_ratio
        rrmse=deviations['RRMSE%'][0]        
        print 'This is the RRMSE (%) : ', rrmse        
        print
        
        all_rrmses.append(rrmse)
#        red_dct, rmse_dct=encoder.dct_compare_single_signal(data_tmp)        
#        print 'Comparison with DCT'
#        print '    Reduction ratio : ', red_dct
#        print '    RRMSE (%) : ', rmse_dct
#        print
#        
#        red_saw, rmse_saw=encoder.sawtooth_compare_single_signal(data_tmp)        
#        print 'Comparison with sawtooth'
#        print '    Reduction ratio : ', red_saw
#        print '    RRMSE (%) : ', rmse_saw
#        print
###        
#        red_haar, rmse_haar=encoder.wavelet_compare_single_signal(data_tmp)        
#        print 'Comparison with haar wavelets'
#        print '    Reduction ratio : ', red_haar
#        print '    RRMSE (%) : ', rmse_haar
#        print
#        
        #P.plot(sensor_id, process_id=process_id, deviation='difference', output='png', 
        #       suptitle='Sensorid_'+str(sensor_id)+'_reduction_'+str(reduction_ratio)+'%_framesize_'+str(frame_size), 
        #       path='../figures/Airbus/', dt_format='%M:%S',)
        P.plot(sensor_id, process_id=process_id, deviation='difference', output='png', suptitle=sensor_name+' ,data_chunk:'+str(sensor_id) +' ,Reduction: '+str(round(reduction_ratio,3))+'% ,rrmse: '+str(round(rrmse,3))+'%' , dt_format='%M:%S')
        #P.plot(sensor_id, process_id=process_id, deviation='difference', output='screen', suptitle='Axis_'+str(sensor_id)+' ,Reduction: '+str(round(reduction_ratio,3))+'% ,rrmse: '+str(round(rrmse,3))+'%' , dt_format='%S.%f')
        
        ##Clean matplotlib!!
        pl.close('all')
        gc.collect()
        
    
    
    ###
    print '**all_reductions'
    print all_reductions
    fig,ax=pl.subplots(1)
    ax.plot(all_reductions)
    ax.set_xlabel("chunk_id")
    ax.set_ylabel("reduction per chunk (%)")
    fig.savefig("reductions.png")
    ###
    
    print '**all_rrmses'
    print all_rrmses
    
    print '**red_points'
    print red_points
    
    print '**Sum of red points'
    all_red_points=sum(red_points)
    print all_red_points
    
    print '**All number of points'
    print len_data
    
    print '***Over All reduction***'
    overall_reduction=(1-all_red_points/float(len_data))*100
    print overall_reduction,"%"
    
    #Dat.save("Konux_%s_%s.dbk" %(value_names[0], sensor_id))   
    Dat.save("Konux_%s_by_chunks.dbk"%sensor_name)   
    print 'Databank with each sensor corresponding to a data chunk has been saved.'
    
    #######################################################################################
    ###Create DataBank and plot concatenating chunks
    
    Dat_concatenated_data=DataBank()
    P_concatenated_data=Plot(Dat_concatenated_data)
    
    raw_data=sensor_data[sensor_name]['values']
    Dat_concatenated_data.add_sensor_data(raw_data,timestamps, real_sensor_id, sensor_name, sensor_name,value_names=value_names)
    
    recon_data=np.concatenate(all_recon_arrays) ##concatenate recon_data per chunk
    recon_data=recon_data.reshape(len(recon_data), 1) ##Put [1,2,3...] in format [[1],[2],[3]...]
    
    process_id=Dat_concatenated_data.add_processed_sensor_data(real_sensor_id, recon_data, timestamps, datatype='recon_data', meta={'reduction' : overall_reduction,'framesize' : frame_size})
    
    deviations=Dat_concatenated_data.get_deviations(sensor_id=real_sensor_id, process_id=process_id)
    rrmse=deviations['RRMSE%'][0]
    
    ##Rename the previous folder containing the plots for each segment..why? the concatenated_data with sensor_id=real_sensor_id will be stored in a folder named difference_plots...:P
    os.rename('difference_plots','difference_plots_per_data_segment')
    
    P_concatenated_data.plot(sensor_id=real_sensor_id, process_id=process_id, deviation='difference', output='png', suptitle=sensor_name+' ,Reduction: '+str(round(overall_reduction,3))+'% ,rrmse: '+str(round(rrmse,3))+'%' , dt_format='%M:%S')
    P_concatenated_data.plot(sensor_id=real_sensor_id, process_id=process_id, deviation='difference', output='screen', suptitle=sensor_name+' ,Reduction: '+str(round(overall_reduction,3))+'% ,rrmse: '+str(round(rrmse,3))+'%' , dt_format='%M:%S')
    
    raw_data=Dat_concatenated_data.get_sensor_raw_data(real_sensor_id)
    raw_values = np.asarray(raw_data['values'])
    np.savetxt('./raw_data_Axis_%d.txt'%real_sensor_id,raw_values)
    
    recon_data=Dat_concatenated_data.get_sensor_recon_data(real_sensor_id,process_id)
    recon_values = np.asarray(recon_data['values'])
    np.savetxt('./recon_data_Axis_%d_reduction_%f.txt'%(real_sensor_id,overall_reduction),recon_values)
    
    #######

    
    ##AngelH: What was the next for?
    if (write):
        #file_dict='../Dictionaries/dictionary_sensorid'+str(sensor_id)+'_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
        #with open('../Datas/Airbus/Curve_of_redratio/variance.csv', "wb") as f:
        with open('variance.csv', "wb") as f:
            csvfile = csv.writer(f, delimiter=';')
            for j in range(len(reduction_list)):
                csvfile.writerow((reduction_list[j], sigma[j]))
        f.close()  
    #pl.figure()
    #from Differences_plots import plot_tuples    
    #plot_tuples(reduction_list, sigma)
    #pl.title('Variance according to reduction ratio')
    #pl.show()
    
