# -*- coding: utf-8 -*-



 
    
def compr_float3(x):
    """write the float with 3 decimals, or none if the float is 0"""
    if (x==0): 
        return 0
    else:
        return '%.3f' % x
        
def compr_float(x, dig):
    """write the float with 3 decimals, or none if the float is 0"""
    if (x==0): 
        return 0
    else:
        return ('%.'+str(dig)+'f') % x
        
def compr_float2(x):
    """write the float with 3 decimals, or just 0 if the float is 0"""
    if (x==0): 
        return 0
    else:
        return '%.2f' % x
        
def compr_int(n):
    if (n==0): 
        return 0
    else:
        return '%.0f' % n
        
