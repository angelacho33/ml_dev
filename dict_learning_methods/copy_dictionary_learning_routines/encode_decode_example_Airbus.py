# -*- coding: utf-8 -*-
"""
Created on Thu Aug 04 10:20:59 2016

@author: Etienne
"""

from tk_dictionary_encoder import SignalDictionaryEncoder
from tk_dictionary_decoder import SignalDictionaryDecoder
import tk_gdfr
from tk_databank import DataBank
import plot_dictionary as pd
import matplotlib.pylab as pl
from tk_plot import Plot
import numpy as np
from dict_analysis import dict_read 
import sys
from Differences_plots import diff_hist

if __name__ == '__main__':
	
    import glob
    import os
    import csv
    import matplotlib
    #matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.
    
   
    value_names = 'airbus'
    

    
    plot_dict=False
    
    
    #file = 'Data/BMW/%s_data_%s.csv' %(value_names, numberofdata)
    
    #file = '../Data/BMW_Velocity/806c4190-98b5-4812-bd47-365f3bdca01e-velocity.csv'
    
    folder='../Data/Airbus/'
    #folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/'
    filenamelist = sorted(glob.glob(folder+'/*long_slice.csv'),key=os.path.getsize,reverse=True)
    
    print filenamelist
    
    file = filenamelist[0]
    sensor_id = 0
    
    reduction_list = []
    sigma=[]    
    
    write = True
    
    #for file in filenamelist:
    sensor_list = range(1,13)
    for sensor_id in sensor_list: 
        Dat = DataBank()  #call it Dat because D represents the Dictionary in what follows
        P = Plot(Dat)
        print '-'*40
        print 'Sensor_id : ', sensor_id
        frame_size=50
        n_components=frame_size
        #print file
        

        
        #sensor_id=sensor_id + 1
        sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [sensor_id])
	   
        #print sensor_data1['pos']['tss'][1:]
       

        clean_tss=sensor_data1['pos']['tss'][2:]
        clean_tss=np.linspace(0, len(clean_tss)*1000./128, len(clean_tss))  
    

        hh=sensor_data1['pos']['values'][2:]
        maxmin=np.max(hh)-np.min(hh)
#        print clean_tss
        #sensor_id=sensor_id + 1
        ##Add data to a databank
        #S1 = Dat.add_sensor_data(sensor_data1['pos']['values'][1:],
        #                       sensor_data1['pos']['tss'][1:], sensor_id, "velocity", "pos",value_names=value_names)
#        start=len(clean_tss)/4
#        n_chunk=90*128
        value_names=sensor_data1['pos']['values'][0]
        S1 = Dat.add_sensor_data(sensor_data1['pos']['values'][2:],
                               clean_tss, sensor_id, "velocity", "pos",value_names=value_names)
					  
        #Dat.sensor_raw_data_clean(sensor_id, 20)
    
        #D.write_sensor_data_files(sensor_id)
    
    
        ####################   Interpolation Daniel implemented
        #Raw data info
        raw_data=Dat.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        npoints=raw_data['count']
    
    
        data_tmp = values
        #print data_tmp
        data_tmp = [x[0] for x in data_tmp]
#        print data_tmp
        data_tmp = np.asarray(data_tmp)
	   
#        print data_tmp
    
        #data_tmp = data_tmp[0:,1]#.tolist()

        #data_tmp = []
        #for row in data.value:
        #    data_tmp.append(row)
        ##    data_codename_all.append(row)
        ##        if row not in CodeNames_All:
        ##           CodeNames_All.append(row)
    
#       print "This is the new dataset",data_tmp
        print "This is the dataset length",len(data_tmp)
        len_recon=len(data_tmp)     
        if (len_recon%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
            for j in range (frame_size-(len_recon%frame_size)):
                data_tmp=np.concatenate((data_tmp, [data_tmp[len_recon-1]]))
       
        #loadthedictionary
        file_dict='../Dictionaries/Airbus/Dictionary_'+'airbus'+'_sensorid'+str(sensor_id)+'_omp_%s_%s.csv' % (n_components, frame_size)
        try:
            D=dict_read(file_dict)
        except IOError as e:
            print "Unable to open file" #Does not exist OR no read permissions
            
            
            config_train={
                'engine_id': "encoding_0",
                'engine_type': "encoder",
                'engine_layer': "signal",
                'did': 1,
                'f' : frame_size,
                'tol_2': 1,
                'tol_1': 0.01*maxmin,
                'tol' : -1,
                'n_iter' : 50,
                'n_components' : frame_size,
                'factor' : 5,
                'n_nonzero': None,
                'init' : 'svd'} 
                
                
            print 'Training the dictionary ...'
            y_1=data_tmp[:len(data_tmp)/2]
            
#            from ksvd import KSVD
#            D_0,c_0=KSVD(y_1, n_components, frame_size/3, 5, enable_threading=False)
#            D_0[0]=(np.ones(frame_size))/np.sqrt(frame_size)           
#            D_0=None
#            c_0=None
            from tk_dictionary_learning import DictionaryTraining
            dictio=DictionaryTraining(config_train)
            D=dictio.fit(y_1)
            print 'Dictionary trained.'
    
            if (plot_dict):
                if D is not None:
                    fi4=pl.figure(4, figsize=(4.2, 4))
                    fi4.suptitle('Initialization dictionary', fontsize=18)
                    pd.plot_dic(D)
	       #Writes the dictionary in a csv file
            if (write):
                #file_dict='../Dictionaries/dictionary_sensorid'+str(sensor_id)+'_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
                with open(file_dict, "wb") as f:
                    csvfile = csv.writer(f, delimiter=';')
                    for i in range(len(D[0])):
                        csvfile.writerow((D[:,i]))
                f.close()    
        
        config_enc={'engine_id': "encoding_0",
                    'engine_type': "encoder",
                    'engine_layer': "signal",
                    'did': 1,
                    'f': frame_size,
                    'd': D,
                    'tol_2': 1,
                    'tol_1': 0.01*maxmin,
                    'n_nonzero': None}    
        encoder=SignalDictionaryEncoder(config_enc)
    
        red,_=encoder.single_encoding(data_tmp)
#       red=np.array(red)
    
#        print "This is the dataset length",len(data_tmp)
#        print "This is the reduced data shape", red.shape
        reduction_ratio = (1-red.shape[0]/float(len(data_tmp)))*100
        #print "Reduction ratio", (1-red.shape[0]/float(len(data_tmp)))*100
	   
        print "This is the data",data_tmp
        print "This is the reduced data",red.shape
        
        reduction_list.append(reduction_ratio)
        print "Mean reduction",np.mean([reduction_list])
    
    
        config_dec={'engine_id': "encoding_0",
                    'engine_type': "encoder",
                    'engine_layer': "signal",
                    'did': 1,
                    'f': frame_size,
                    'd': D}
        decoder=SignalDictionaryDecoder(config_dec)
        recon,_=decoder.single_decoding(red, length=len(data_tmp))
        recon=recon[:len_recon]
    
        process_id=Dat.add_processed_sensor_data(sensor_id, recon, tss, datatype='recon_data', meta={'reduction' : reduction_ratio,
                                                                                                         'framesize' : frame_size})
                                                                                    
        deviations=Dat.get_deviations(sensor_id=sensor_id, process_id=process_id)   
        
        variance_b=np.mean(data_tmp**2)-np.mean(data_tmp)**2
    
        h=data_tmp.reshape(len(data_tmp)/frame_size, frame_size)
        
        variance_seg=(np.mean(h**2, axis=1)-np.mean(h, axis=1)**2)
        variance_seg=np.mean(variance_seg)
    
        maxmin_b=np.max(data_tmp)-np.min(data_tmp)  
        
        sigma0=variance_b/(maxmin_b**2)
        
#        _,_,sigma0=diff_hist(data_tmp, filename='../figures/Airbus/diff_hist_senorid_'+str(sensor_id)+'.png', title='Difference hist, sensor id ' + str(sensor_id)+', red '+ str(reduction_ratio),
#                  relative=True, bins=500)
#                  
        sigma.append(sigma0)
        Dat.save("../Saved_Databank/Airbus_%s_%s.dbk" %(value_names, sensor_id))   
        
        print 'Databank has been saved.'
        print
        print 'This is the reduction ratio : ', reduction_ratio
        print 'This is the RRMSE (%) : ', deviations['RRMSE%'][0]        
        print
        
#        red_dct, rmse_dct=encoder.dct_compare_single_signal(data_tmp)        
#        print 'Comparison with DCT'
#        print '    Reduction ratio : ', red_dct
#        print '    RRMSE (%) : ', rmse_dct
#        print
#        
#        red_saw, rmse_saw=encoder.sawtooth_compare_single_signal(data_tmp)        
#        print 'Comparison with sawtooth'
#        print '    Reduction ratio : ', red_saw
#        print '    RRMSE (%) : ', rmse_saw
#        print
###        
#        red_haar, rmse_haar=encoder.wavelet_compare_single_signal(data_tmp)        
#        print 'Comparison with haar wavelets'
#        print '    Reduction ratio : ', red_haar
#        print '    RRMSE (%) : ', rmse_haar
#        print
#        
        P.plot(sensor_id, process_id=process_id, deviation='difference', output='png', 
               suptitle='Sensorid_'+str(sensor_id)+'_reduction_'+str(reduction_ratio)+'%_framesize_'+str(frame_size), 
               path='../figures/Airbus/', dt_format='%M:%S',)
    
    
    if (write):
        #file_dict='../Dictionaries/dictionary_sensorid'+str(sensor_id)+'_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
        with open('../Datas/Airbus/Curve_of_redratio/variance.csv', "wb") as f:
            csvfile = csv.writer(f, delimiter=';')
            for j in range(len(reduction_list)):
                csvfile.writerow((reduction_list[j], sigma[j]))
        f.close()  
    pl.figure()
    from Differences_plots import plot_tuples    
    plot_tuples(reduction_list, sigma)
    pl.title('Variance according to reduction ratio')
    pl.show()
    