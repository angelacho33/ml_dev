"""
Teraki Data Process Package
Author: Etienne
Class for DictionaryTraining
"""


import numpy as np
from sklearn.decomposition import DictionaryLearning

def dct_iii(X):
    X = np.array(X)
    PI = np.pi
    N = X.shape[0]
    x = [None] * N
    for n in range(N):
            summation = 0.
            for k in range(N):
                    s = 0.5**0.5 if k==0 else 1.0
                    summation += s * X[k] * np.cos(PI * (n + 0.5) * k / N)
                    x[n] = summation * (2.0 / N)**0.5
    x_reform = [list(a) for a in zip(*x)]
    return x_reform  
    
def sawtooth(length, invwidth):
    """Discrete sub-sampled sawtooth wavelet"""
    import scipy.signal as scs
    x = np.linspace(0, length - 1, length)
    x = scs.sawtooth(2*np.pi*x*(invwidth), width=0.5);
    return (x+1)

def complete_signal(data_tmp, frame_size=None):
        if frame_size is None:
            print 'Frame_size has to be defined'
            return data_tmp
        len_recon=len(data_tmp)     
        if (len_recon%frame_size>0): 
            for j in range (frame_size-(len_recon%frame_size)):
                data_tmp=np.concatenate((data_tmp, [data_tmp[len_recon-1]]))
        return data_tmp


class DictionaryTraining():
    """Class for DictionaryTraining
        Stores all the parameters to train the dictionary, but does not store the dictionary.

    Public Methods:
        fit():  Train the dictionary on an input dataset
        fit_transform():  Train the dictionary and transform the input dataset
    """
    def __init__(self, config_para={}):
        """initiate Engine instance

        Args: 
            config_para: configuration for the Engine is it a dictionary of following:
            {
                'engine_id':  <engine_id>,
                'engine_type':  <engine_type>,
                'engine_layer': <engine_layer>,
                'f': <framesize>,
                'n_components' : <n_components>,
                'n_iter' : <numberofiterations>, maximal number of iterations before stopping the learning phase
                'tol' :<numerical error to stop the loop>, set it to -1 if you just want to reach n_iter iterations in the learning phase
                'sparsity' :<sparsity_controlling_parameter>, parameter in the cost function (usually set to 1)
                'did': <dictionary_id>, 
                'init' : <initialization>, the way to initialize : 'svd', None, or directly set a dictionary D to do a warm restart
                'tol_2': <toleranceRRMSE for the learning phase>, tolerance for the RRMSE. Not in percentage, so 0.02 means 2% of RRMSE
                                                                  set it to 1 if you don't want to use it
                'tol_1': <tolerancedeviation for the learning phase>, tolerance for the maximal deviation with the original signal (not relative)
                                                                  set it to None if you don't want to use it
                'n_nonzero' : <maximal number of nonzero per frame size>, set it to None if you don't want to use it
                'factor' : <factor for the first learning step> (set it to None if only want one phase)
            }

            <engine_name>, <engine_type> and <engine_layer> are string, <dictionary_id> is the id
            of the <dictionary> and <dictionary> is the dictionary. the format of <dictionary> is
            to be discussed.

            Here is an example of input config_para:
            {
                'engine_id': "encoding_0",
                'engine_type': "encoder",
                'engine_layer': "signal",
                'did': 1,
                'f': 5,
                'n_components': 5,
                'sparsity' : 1.,
                'n_iter' : 50,
                'tol' : -1,
                'init' : 'svd',
                'tol_2':0.03,
                'tol_1':None,
                'n_nonzero':None,
                'factor' : 5
            }

        Returns: 
            None
        """ 
        self.engine_id=config_para['engine_id']
        self.engine_type=config_para['engine_type']
        self.engine_layer=config_para['engine_layer']
        self.f=config_para['f']
        self.did=config_para['did']
        
        try:
            self.init=config_para['init']
        except KeyError:
            self.init='svd'
        
        try:
            if config_para['tol_2'] is None :
                self.tol_2=1
            else :
                self.tol_2=config_para['tol_2']
        except KeyError:
            print 'Undefined tol_2. tol_2 set to 100 %'
            self.tol_2 is 1
        try:
            self.tol_1=config_para['tol_1']
        except KeyError:
            self.tol_1 is None
            
        try:
            self.n_nonzero=config_para['n_nonzero']
        except KeyError:
            self.n_nonzero=None
            
        try:
            self.factor=config_para['factor']
        except KeyError:
            self.factor=None
        
        try:
            self.n_components=config_para['n_components']
        except KeyError:
            self.n_components=self.f
        
        try:
            self.sparsity=config_para['sparsity']
        except KeyError:
            self.sparsity=1.
            
        try:
            self.n_iter=config_para['n_iter']
        except KeyError:
            self.n_iter=50
            
        try:
            self.tol=config_para['tol']
        except KeyError:
            self.tol=1e-9
        
        return
            
            
    def fit(self, value, para=None):
        """ Single signal encoding
        Args: 
            value : training datasets (can be a single signal or a list of signal)
            para: None
        Returns:
            ret_d : trained dictionary
        """
        if self.f is None :
            print 'Has to define a frame size'
            return None
            
      
        y_1=np.concatenate(np.array(value))
        
#        if (len(np.array(value).shape)==2):
#            dim=(np.array(value).shape)[1]
#            self.f*=dim
        
        maxmin=np.max(y_1)-np.min(y_1)
        
        y_1=complete_signal(y_1, frame_size=self.f)
        
        variance_1=np.mean(y_1**2)-np.mean(y_1)**2
        
        y_1=y_1.reshape(len(y_1)/self.f, self.f)
        
        
        if (self.init=='svd'):
            from dict_init import svd_init
            D_0, c_0=svd_init(y_1)
        elif (self.init is None):
            D_0, c_0=None, None
        else:
            print 'Warm restart'
            D_0=self.init
            from sklearn.decomposition import SparseCoder
            coder_0 = SparseCoder(dictionary=D_0, transform_n_nonzero_coefs=self.n_nonzero,
                                    transform_alpha=(maxmin*self.tol_2)**2*self.f, transform_algorithm='omp', tol_1=self.tol_1)
            c_0=coder_0.transform(y_1)
        
        if self.factor is not None:
             dictio = DictionaryLearning(n_components=self.n_components, alpha=self.sparsity*np.sqrt(variance_1), 
                                        max_iter=self.n_iter, tol=self.tol,
                                        fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=self.n_nonzero, 
                                        transform_alpha=(self.tol_2*maxmin)**2*self.f*self.factor, n_jobs=1, 
                                        code_init=c_0, dict_init=D_0, verbose=False, 
                                        split_sign=False, random_state=None, tol_2=self.f*(self.tol_2*maxmin)**2*self.factor, 
                                        tol_1=self.tol_1*self.factor if self.tol_1 is not None else None, n_nonzero_coefs=self.n_nonzero)
                                    
                                    
            #On this step is the dictionary learned, and x is then transformed in the new basis
             c_0=dictio.fit_transform(y_1)
        
             D_0=dictio.components_ #here is the final dictionary    
        
       
        dictio = DictionaryLearning(n_components=self.n_components, alpha=self.sparsity*np.sqrt(variance_1), 
                                        max_iter=self.n_iter, tol=self.tol,
                                        fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=self.n_nonzero, 
                                        transform_alpha=(self.tol_2*maxmin)**2*self.f, n_jobs=1, 
                                        code_init=c_0, dict_init=D_0, verbose=False, 
                                        split_sign=False, random_state=None, tol_2=self.f*(self.tol_2*maxmin)**2, 
                                        tol_1=self.tol_1, n_nonzero_coefs=self.n_nonzero)
                                    					
                                    
                                    
            #On this step is the dictionary learned, and x is then transformed in the new basis
        dictio.fit(y_1)
            #Analyses the results        
        ret_d=dictio.components_ #here is the final dictionary
        return ret_d
        
    def fit_transform(self, value, para=None):
        """ Single signal encoding
        Args: 
            value : training datasets (can be a signle signal or a list of signal)
            para: None
        Returns:
            ret_d : trained dictionary
            x : transformed data
            
        """
        if self.f is None :
            print 'Has to define a frame size'
            return None
        y_1=np.ravel(np.array(value))
        
        maxmin=np.max(y_1)-np.min(y_1)
        
        y_1=complete_signal(y_1, frame_size=self.f)
        
        variance_1=np.mean(y_1**2)-np.mean(y_1)**2
        
        y_1=y_1.reshape(len(y_1)/self.f, self.f)
        
        
        if (self.init=='svd'):
            from dict_init import svd_init
            D_0, c_0=svd_init(y_1)
        elif (self.init is None):
            D_0, c_0=None, None
        else:
            print 'Warm restart'
            D_0=self.init
            from sklearn.decomposition import SparseCoder
            coder_0 = SparseCoder(dictionary=D_0, transform_n_nonzero_coefs=self.n_nonzero,
                                    transform_alpha=(maxmin*self.tol_2)**2*self.f, transform_algorithm='omp', tol_1=self.tol_1)
            c_0=coder_0.transform(y_1)
        
        if self.factor is not None:
             dictio = DictionaryLearning(n_components=self.n_components, alpha=self.sparsity*np.sqrt(variance_1), 
                                        max_iter=self.n_iter, tol=self.tol,
                                        fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=self.n_nonzero, 
                                        transform_alpha=(self.tol_2*maxmin)**2*self.f*self.factor, n_jobs=1, 
                                        code_init=c_0, dict_init=D_0, verbose=False, 
                                        split_sign=False, random_state=None, tol_2=self.f*(self.tol_2*maxmin)**2*self.factor, 
                                        tol_1=self.tol_1*self.factor if self.tol_1 is not None else None, n_nonzero_coefs=self.n_nonzero)
                                    
                                    
            #On this step is the dictionary learned, and x is then transformed in the new basis
             c_0=dictio.fit_transform(y_1)
        
             D_0=dictio.components_ #here is the final dictionary    
        
       
        dictio = DictionaryLearning(n_components=self.n_components, alpha=self.sparsity*np.sqrt(variance_1), 
                                        max_iter=self.n_iter, tol=self.tol,
                                        fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=self.n_nonzero, 
                                        transform_alpha=(self.tol_2*maxmin)**2*self.f, n_jobs=1, 
                                        code_init=c_0, dict_init=D_0, verbose=False, 
                                        split_sign=False, random_state=None, tol_2=self.f*(self.tol_2*maxmin)**2, 
                                        tol_1=self.tol_1, n_nonzero_coefs=self.n_nonzero)
                                    					
                                    
                                    
            #On this step is the dictionary learned, and x is then transformed in the new basis
        x=dictio.fit_transform(y_1)
            #Analyses the results        
        ret_d=dictio.components_ #here is the final dictionary
        return ret_d, x
  
    
    def DCT_dict(self, y_b, para=None, frame_size=None):
        """
        Returns a DCT dictionary. This is a square matrix.
        """
        if frame_size==None:
            frame_size=self.f
            
        D_dct = np.array(dct_iii(np.identity(frame_size)))
        return D_dct
      
    def sawtooth_dict(self, y_b, para=None, frame_size=None, n_components=None):
        """
        Retruns a sawtooth dictionary. The number of n_components can be chosen.
        """
        if frame_size==None:
            frame_size=self.f
        if n_components is None:
            n_components=frame_size
        D_saw=np.zeros((n_components, frame_size))
        for i in range(n_components):
            D_saw[i]=sawtooth(frame_size, (i+0.0)/(2*frame_size))-1
            D_saw[i]/=np.sqrt(np.sum(D_saw[i]**2))
        return D_saw
        
    def wavelet_dict(self, y_b, para=None, frame_size=None, n_components=None, wavelet='haar'):
        """
        Returns a wavelet dictionary.
        You can chose your wavelet among the pywt package list.
        """
        if frame_size==None:
            frame_size=self.f
        if frame_size%2!=0:
            print 'Frame size has to be even'
            return None
        if n_components is None:
            n_components=frame_size
        import pywt
        D_wav=np.identity(frame_size/2)
        D_wav=np.concatenate((pywt.idwt(D_wav, None, wavelet), pywt.idwt(None, D_wav, wavelet)))
        if D_wav.shape!=(frame_size, n_components):
            print 'Problem with pywt, wrong shape of the wavelet dictionary'
            return None
        for i in range(n_components):
            D_wav[i]/=np.sqrt(np.sum(D_wav[i]**2))
        return D_wav
        
    
    
 