# -*- coding: utf-8 -*-
"""
Created on Thu Aug 04 10:20:59 2016

@author: Etienne
"""

from tk_dictionary_encoder import SignalDictionaryEncoder
from tk_dictionary_decoder import SignalDictionaryDecoder
import tk_gdfr
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
from dict_analysis import dict_read
from Differences_plots import diff_hist

if __name__ == '__main__':
	
    import glob
    import os
    import csv
    import matplotlib
    #matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.
   
    value_names = 'velocity'
    scenario = 'overland'
    numberofdata=2
    frame_size=15
    n_components=frame_size
    #file = 'Data/BMW/%s_data_%s.csv' %(value_names, numberofdata)
    
    #file = '../Data/BMW_Velocity/806c4190-98b5-4812-bd47-365f3bdca01e-velocity.csv'
    
    folder='../Data/BMW/BMW_velocity/'+str(scenario)
    #folder='/home/angelh/Documents/DataScienceRelated/Teraki/Files/BMW/probes/'
    filenamelist = sorted(glob.glob(folder+'/*velocity.csv'),key=os.path.getsize,reverse=True)
    sensor_id = 0
    
    reduction_list = []
    
    write = True
    
    for file in filenamelist: 
        #print file
        sensor_data1 = tk_gdfr.get_ts_value_from_file(file, ',', 'pos', 'pos', 0, [1])
        sensor_id=sensor_id + 1
             
        Dat = DataBank()  #call it Dat because D represents the Dictionary in what follows
        P = Plot(Dat)
        ##Add data to a databank
        S1 = Dat.add_sensor_data(sensor_data1['pos']['values'][1:],
                               sensor_data1['pos']['tss'][1:], sensor_id, "velocity", "pos",value_names=value_names)
					  
        Dat.sensor_raw_data_clean(sensor_id, 20)
    
        #D.write_sensor_data_files(sensor_id)
    
    
        ####################   Interpolation Daniel implemented
        #Raw data info
        raw_data=Dat.get_sensor_raw_data(sensor_id)
        tss=np.asarray(raw_data['tss'])
        values=np.asarray(raw_data['values'])
        npoints=raw_data['count']
    
    
        data_tmp = values
        #print data_tmp
        data_tmp = [x[0] for x in data_tmp]
#        print data_tmp
        data_tmp = np.asarray(data_tmp)
    
        #data_tmp = data_tmp[0:,1]#.tolist()

        #data_tmp = []
        #for row in data.value:
        #    data_tmp.append(row)
        ##    data_codename_all.append(row)
        ##        if row not in CodeNames_All:
        ##           CodeNames_All.append(row)
    
#       print "This is the new dataset",data_tmp
        print "This is the dataset length",len(data_tmp)
        len_recon=len(data_tmp)     
        if (len_recon%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
            for j in range (frame_size-(len_recon%frame_size)):
                data_tmp=np.concatenate((data_tmp, [data_tmp[len_recon-1]]))
  
        config_train={'tol_2': 0.02,
                'tol_1': 0.01,
                'tol' : -1,
                'n_iter' : 50,
                'n_components' : frame_size,
                'factor' : 10,
                'n_nonzero': None} 

        #loadthedictionary
        file_dict='../Dictionaries/BMW/Dictionary_type_'+str(value_names)+'_sensorid_'+str(sensor_id)+'_scenario_'+str(scenario)+'_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
        try:
            D=dict_read(file_dict)
        except IOError as e:
            print "Unable to open file" #Does not exist OR no read permissions
            print 'Training the dictionary ...'
            y_1=data_tmp[:len(data_tmp)/2]
            if (len(y_1)%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
                for j in range (frame_size-(len(y_1)%frame_size)):
                    y_1=np.concatenate((y_1, [y_1[len(y_1)-1]]))
            variance_1=(np.mean(y_1**2)-np.mean(y_1)**2) 
    
            y_1=y_1.reshape(len(y_1)/frame_size, frame_size)
            maxmin_1=np.max(y_1)-np.min(y_1)
            from sklearn.decomposition import DictionaryLearning
            from ksvd import KSVD
            D_0,c_0=KSVD(y_1, n_components, frame_size/3, 5, enable_threading=False)
#            D_0=None
#            c_0=None
            dictio = DictionaryLearning(n_components=config_train['n_components'], alpha=1.0*np.sqrt(variance_1), 
                                        max_iter=config_train['n_iter'], tol=config_train['tol'],
                                        fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=config_train['n_nonzero'], transform_alpha=(config_train['tol_2']*maxmin_1)**2 if config_train['tol_2'] is not None else None, n_jobs=1, 
                                        code_init=c_0, dict_init=D_0, verbose=False, 
                                        split_sign=False, random_state=None, tol_2=(config_train['tol_2']*maxmin_1)**2*config_train['factor'] if config_train['tol_2'] is not None else None, 
                                        tol_1=config_train['tol_1']*config_train['factor'] if config_train['tol_1'] is not None else None, n_nonzero_coefs=config_train['n_nonzero'])
                                    
                                    
            #On this step is the dictionary learned, and x is then transformed in the new basis
            x=dictio.fit_transform(y_1)
        
            D=dictio.components_ #here is the final dictionary    
        
            dictio = DictionaryLearning(n_components=config_train['n_components'], alpha=1.0*np.sqrt(variance_1), 
                                        max_iter=config_train['n_iter'], tol=config_train['tol'],
                                        fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=config_train['n_nonzero'], transform_alpha=(config_train['tol_2']*maxmin_1)**2 if config_train['tol_2'] is not None else None, n_jobs=1, 
                                        code_init=x, dict_init=D, verbose=False, 
                                        split_sign=False, random_state=None, tol_2=(config_train['tol_2']*maxmin_1)**2 if config_train['tol_2'] is not None else None, 
                                        tol_1=config_train['tol_1'], n_nonzero_coefs=config_train['n_nonzero'])
            						         
            #On this step is the dictionary learned, and x is then transformed in the new basis
            x=dictio.fit_transform(y_1)
            #Analyses the results        
            D=dictio.components_ #here is the final dictionary
            print 'Dictionary trained.'
    
	       #Writes the dictionary in a csv file
            if (write):
                #file_dict='../Dictionaries/dictionary_sensorid'+str(sensor_id)+'_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
               with open(file_dict, "wb") as f:
                   csvfile = csv.writer(f, delimiter=';')
                   for i in range(len(D[0])):
                       csvfile.writerow((D[:,i]))
               f.close()
        config_enc={'engine_id': "encoding_0",
                    'engine_type': "encoder",
                    'engine_layer': "signal",
                    'did': 1,
                    'f': frame_size,
                    'd': D,
                    'tol_2': 0.06,
                    'tol_1': 0.01,
                    'n_nonzero': None}    
        encoder=SignalDictionaryEncoder(config_enc)
    
        red,_=encoder.single_encoding(data_tmp)
#       red=np.array(red)
    
        print "This is the dataset length",len(data_tmp)
        print "This is the reduced data shape", red.shape
        reduction_ratio = (1-red.shape[0]/float(len(data_tmp)))*100
        #print "Reduction ratio", (1-red.shape[0]/float(len(data_tmp)))*100
	   
        print "This is the data",data_tmp
        print "This is the reduced data",red.shape

        reduction_list.append(reduction_ratio)
	   
        #np.savetxt('Data_reduced_velocity_BMW_Values_'+str(value_names)+'_scenario_'+str(scenario)+'.csv',reduction_list)
        print "Mean reduction",np.mean([reduction_list])
    
    
        config_dec={'engine_id': "encoding_0",
                    'engine_type': "encoder",
                    'engine_layer': "signal",
                    'did': 1,
                    'f': frame_size,
                    'd': D}
        decoder=SignalDictionaryDecoder(config_dec)
        recon,_=decoder.single_decoding(red, length=len(data_tmp))
        recon=recon[:len_recon]
    
        process_id=Dat.add_processed_sensor_data(sensor_id, recon, tss, datatype='recon_data', meta={'reduction' : reduction_ratio,
                                                                                                         'framesize' : frame_size})
      
      
        diff_hist(data_tmp, filename='../figures/BMW/'+str(value_names)+'/'+str(scenario)+'/diff_hist_senorid_'+str(sensor_id)+'.png', title='Difference hist, sensor id ' + str(sensor_id)+', red '+ str(reduction_ratio))
       
        Dat.save("../Saved_Databank/BMW_%s_%s.dbk" %(value_names, numberofdata))   
    
        print 'Databank has been saved.'
        P.plot(sensor_id, process_id=process_id, deviation='difference', output='png', suptitle='Type_'+str(value_names)+'_scenario_'+str(scenario)+'_sensorid_'+str(sensor_id)+'_reduction_'+str(reduction_ratio)+'%', path='../figures/BMW/'+str(value_names)+'/'+str(scenario)+'/')
    np.savetxt('Data_reduced_velocity_BMW_Values_'+str(value_names)+'_scenario_'+str(scenario)+'.csv',reduction_list)