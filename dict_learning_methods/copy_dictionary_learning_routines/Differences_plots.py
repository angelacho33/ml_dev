# -*- coding: utf-8 -*-
"""
Created on Mon Aug 15 10:00:54 2016

@author: Etienne
"""
import numpy as np
import matplotlib.pylab as pl
from scipy.stats import norm
import matplotlib.mlab as mlab

def diff(y):
    dif=np.zeros(len(y)-1)
    for i in range(len(dif)):
        dif[i]=y[i+1]-y[i]
    return dif

def diff_hist(y, filename=None, png=True, bins=50, title='Difference hist', relative=False, removezeros=True):
    dif=diff(y)
    if removezeros:
        dif=dif[np.flatnonzero(dif)]
    if relative:
        maxmin=np.max(y)-np.min(y)
        if maxmin!=0:
            dif/=maxmin
        title='Relative '+title
    if ((filename is None) and png):
        print 'Need a filename to save the plot'
        return dif
    (mu, sigma) = norm.fit(dif)
    fi=pl.figure()
    n, bi, patches=pl.hist(dif, bins=bins, normed=True, facecolor='green', alpha=0.75)
    z = mlab.normpdf( bi, mu, sigma)
    l = pl.plot(bi, z, 'r--', linewidth=2)
    pl.xlabel('Relative difference')
    pl.ylabel('Probability')
    pl.title(title + (r'$\mu=%.3f,\ \sigma=%.3f$' %(mu, sigma)))
    pl.grid(True)
    if png:
        pl.savefig(filename, bbox_inches='tight')
        pl.close()
    else:
        pl.show()
    return dif, mu, sigma
    
def plot_tuples(x, y):
    import math
    if (len(x)!=len(y)):
        print 'x and y must have the same length'
        return None
    x=np.array(x)
    y=np.array(y)
    x_i=np.sort(x, axis=0)
    xs=[]
    for i in range(len(x_i)):
        if (not math.isnan(x_i[i])):
            xs.append(x_i[i])
    ys=np.zeros(len(xs))
    for i in range(len(xs)):
        j=-1
        found=False
        while (not found):
            j+=1
            found=(xs[i]==x[j])
        ys[i]=y[j]
    pl.plot(xs, ys)
    return xs, ys