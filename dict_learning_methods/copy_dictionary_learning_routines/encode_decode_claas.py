# -*- coding: utf-8 -*-
"""
Created on Thu Aug 04 10:20:59 2016

@author: Etienne
"""

from tk_dictionary_encoder import SignalDictionaryEncoder
from tk_dictionary_decoder import SignalDictionaryDecoder
#import tk_gdfr
from claas_db import ClaasDataBank
#import plot_dictionary as pd
import matplotlib.pylab as pl
from tk_plot import Plot
import numpy as np
from dict_analysis import dict_read 
import re
from analyze import load_and_plot
import sys
from Differences_plots import diff_hist

if __name__ == '__main__':
	
    import glob
    import os
    import csv
    import matplotlib
    import time
    #matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.
    
    t00=time.time()

    value_names = 'claas'
    
#    f_txt = open("Claas_dl.txt", 'w')
#    sys.stdout = f_txt
    
    
    folder='../Data/Claas/Teraki/'
    
    #file = 'Data/BMW/%s_data_%s.csv' %(value_names, numberofdata)
    
    #file = '../Data/BMW_Velocity/806c4190-98b5-4812-bd47-365f3bdca01e-velocity.csv'
    
    

#    filenames = glob.glob(folder+'/*.csv')
    filenamelist = sorted(glob.glob(folder+'*.csv'),key=os.path.getsize,reverse=False) ##Order by size..from lightest to heaviest
  
    print filenamelist
    
    sensorlist=range(1, 29)
    
    reduction_list = []
    
    write = True
    
    #for file in filenamelist:
    frame_list=[15]
    sigma_list=[]
    for frame_size in frame_list:
        n_components=frame_size
#        for non, filename in enumerate(filenamelist):
        for sensor_id_extracted in sensorlist:
            Dat = ClaasDataBank()
            P = Plot(Dat)
#            print '-'*40
            
#            sensor_id_extracted=map(int, re.findall('\d+', filename))[0] ##New way using map and regularexpression package
            filename=folder+'sig_'+str(sensor_id_extracted)+'.csv'
             
#            print 'Reading file:', non, ' sensor_id:' , sensor_id_extracted, filename
            #D.read_sensor_file(filename, sensor_id=i)
            Dat.read_sensor_file(filename, sensor_id=sensor_id_extracted)
            
            
            print
            print
            print
            print '-'*40
            print 'Frame size : ', frame_size
            print '-'*40
    
            #print file
            
            for sensor in Dat.get_sensor_list():
                print '-'*40
                index=sensor['index']
                dof=sensor['dof']
                sensor_id=sensor['sensor_id']
                sensor_name=sensor['sensor_name']
                            
                raw_data=Dat.get_sensor_raw_data(sensor_id) 
                tss=np.asarray(raw_data['tss'])
                values=np.asarray(raw_data['values'])
                npoints=raw_data['count']
                
                reconlist=np.zeros(values.shape)
                red_list=np.zeros(dof)          
                for i in range(dof):
                    
                    print "Processing sensor:", sensor_id, sensor_name
                    print 'Current dimension : %s of %s' %(i, dof)
                    print 
                    #print "'"+sensor_name+"'"+':[1000  ,0.90       ,[1,1]       ,[0,0]            ,[default_delta,default_delta]],'
                    #if sensor_name!='FeedRakeRpmSignalActVal':
                    #    continue
                    #if sensor_id!=5:
                    #if sensor_id in [10,9,28,23,13,14,6,27,25,16,12,22,11,4,8,7,3]: ##Not to repeat the results I already got in test3
                    #if sensor_id!=10:
                    #    continue
                    #if sensor_id not in [2,6,13,15,16]:
                    #    continue
        
            
                
                
                    data_tmp = values
                    #print data_tmp
                    data_tmp = [x[i] for x in data_tmp]
            #        print data_tmp
                    data_tmp = np.asarray(data_tmp)
            	   
            #        print data_tmp
                
                    #data_tmp = data_tmp[0:,1]#.tolist()
            
                    #data_tmp = []
                    #for row in data.value:
                    #    data_tmp.append(row)
                    ##    data_codename_all.append(row)
                    ##        if row not in CodeNames_All:
                    ##           CodeNames_All.append(row)
                
            #       print "This is the new dataset",data_tmp
                    print "This is the dataset length",len(data_tmp)
                    print
                    maxmin=np.max(data_tmp)-np.min(data_tmp)
                    len_recon=len(data_tmp)     
                    if (len_recon%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
                        for j in range (frame_size-(len_recon%frame_size)):
                            data_tmp=np.concatenate((data_tmp, [data_tmp[len_recon-1]]))
              
                
            
                    #loadthedictionary
                    file_dict='../Dictionaries/Claas/Dictionary_tol001_'+str(sensor_name)+'_sensorid'+str(sensor_id)+'_dimension'+str(i)+'_%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
                    try:
                        D=dict_read(file_dict)
                    except IOError as e:
                        print "Unable to open file" #Does not exist OR no read permissions
                        
                        
                        config_train={'tol_2': 1,
                            'tol_1':0.01*maxmin,
                            'tol' : -1,
                            'n_iter' : 50,
                            'n_components' : frame_size,
                            'factor' : 3,
                            'n_nonzero': None} 
                            
                            
                        print 'Training the dictionary ...'
                        y_1=data_tmp[:len(data_tmp)/2]
                        if (len(y_1)%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
                            for j in range (frame_size-(len(y_1)%frame_size)):
                                y_1=np.concatenate((y_1, [y_1[len(y_1)-1]]))
                        variance_1=np.mean(y_1**2)-np.mean(y_1)**2
                        maxmin_1=np.max(y_1)-np.min(y_1)
                
                        y_1=y_1.reshape(len(y_1)/frame_size, frame_size)
                        from sklearn.decomposition import DictionaryLearning
#                        from ksvd import KSVD
#                        D_0,c_0=KSVD(y_1, n_components, frame_size/3, 5, enable_threading=False)
#                        D_0[0]=1./np.sqrt(frame_size)
                        from dict_init import svd_init
                        D_0, c_0=svd_init(y_1)
            #            D_0=None
            #            c_0=None
                        dictio = DictionaryLearning(n_components=config_train['n_components'], alpha=1.0*np.sqrt(variance_1), 
                                                    max_iter=config_train['n_iter'], tol=config_train['tol'],
                                                    fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=config_train['n_nonzero'], transform_alpha=(config_train['tol_2']*maxmin_1)**2, n_jobs=1, 
                                                    code_init=c_0, dict_init=D_0, verbose=False, 
                                                    split_sign=False, random_state=None, tol_2=frame_size*(config_train['tol_2']*maxmin_1)**2*config_train['factor'], tol_1=config_train['tol_1']*config_train['factor'] if config_train['tol_1'] is not None else None, n_nonzero_coefs=config_train['n_nonzero'])
                                                
                                                
                        #On this step is the dictionary learned, and x is then transformed in the new basis
                        x=dictio.fit_transform(y_1)
                    
                        D=dictio.components_ #here is the final dictionary    
                    
                        dictio = DictionaryLearning(n_components=config_train['n_components'], alpha=1.0*np.sqrt(variance_1), 
                                                    max_iter=config_train['n_iter'], tol=config_train['tol'],
                                                    fit_algorithm='omp', transform_algorithm='omp', transform_n_nonzero_coefs=config_train['n_nonzero'], transform_alpha=(config_train['tol_2']*maxmin_1)**2, n_jobs=1, 
                                                    code_init=x, dict_init=D, verbose=False, 
                                                    split_sign=False, random_state=None, tol_2=frame_size*(config_train['tol_2']*maxmin_1)**2, tol_1=config_train['tol_1'], n_nonzero_coefs=config_train['n_nonzero'])
                        						
                                                
                                                
                        #On this step is the dictionary learned, and x is then transformed in the new basis
                        x=dictio.fit_transform(y_1)
                        #Analyses the results        
                        D=dictio.components_ #here is the final dictionary
                        print 'Dictionary trained.'
                        print
                
                       	       #Writes the dictionary in a csv file
                        if (write):
                            #file_dict='../Dictionaries/dictionary_sensorid'+str(sensor_id)+'_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
                            with open(file_dict, "wb") as f:
                                csvfile = csv.writer(f, delimiter=';')
                                for j in range(len(D[0])):
                                    csvfile.writerow((D[:,j]))
                            f.close()    
                
                    config_enc={'engine_id': "encoding_0",
                                'engine_type': "encoder",
                                'engine_layer': "signal",
                                'did': 1,
                                'f': frame_size,
                                'd': D,
                                'tol_2': 1,
                                'tol_1': 0.01*maxmin,
                                'n_nonzero': None}    
                    encoder=SignalDictionaryEncoder(config_enc)
                
                    red,_=encoder.single_encoding(data_tmp)
            #       red=np.array(red)
                
            #        print "This is the dataset length",len(data_tmp)
            #        print "This is the reduced data shape", red.shape
                    if red.shape==():
                        reduction_ratio = (1-1./float(len(data_tmp)))*100
                    else:
                        reduction_ratio = (1-red.shape[0]/float(len(data_tmp)))*100
                    #print "Reduction ratio", (1-red.shape[0]/float(len(data_tmp)))*100
            	   
                    if reduction_ratio<30:
                        print 'This is tol_1 :' +str(config_enc['tol_1'])
                    print "This is the data",data_tmp
                    print "This is the reduced data",red.shape
                    
                    variance_b=np.mean(data_tmp**2)-np.mean(data_tmp)**2
                
                    h=data_tmp.reshape(len(data_tmp)/frame_size, frame_size)
                    
                    variance_seg=(np.mean(h**2, axis=1)-np.mean(h, axis=1)**2)
                    variance_seg=np.mean(variance_seg)
                
                    maxmin_b=np.max(data_tmp)-np.min(data_tmp)                    
                    
                    reduction_list.append(reduction_ratio)
                    red_list[i]=reduction_ratio
                    print "Mean reduction",np.mean([reduction_list])
                
                
                    config_dec={'engine_id': "encoding_0",
                                'engine_type': "encoder",
                                'engine_layer': "signal",
                                'did': 1,
                                'f': frame_size,
                                'd': D}
                    decoder=SignalDictionaryDecoder(config_dec)
                    recon,_=decoder.single_decoding(red, length=len(data_tmp))
                    recon=recon[:len_recon]
                    reconlist[:, i] =recon.reshape(len(recon))
                    
                    sigma0=variance_seg/variance_b
                    sigma_list.append(sigma0)
                
                process_id=Dat.add_processed_sensor_data(sensor_id, reconlist, tss, datatype='recon_data', meta={'reduction' : red_list,
                                                                                                                 'framesize' : frame_size})
                                                                                            
                deviations=Dat.get_deviations(sensor_id=sensor_id, process_id=process_id)   
                
                bank_name="../Saved_Databank/Claas/Claas_%s_%s.dbk" %(sensor_name, sensor_id) 
                Dat.save(bank_name)  
                
        #        load_and_plot(bank_name, sensor_id=sensor_id, suptitle=('Claas sensor %s, dimension %s' % (sensor_id, i)))
                
                
                
                print 'Databank has been saved.'
                print
                    
                for i in range(dof):
                    print '-'*10
                    print 'Dimension : %s' % i
                    print 'This is the reduction ratio : ', red_list[i]
                    print 'This is the RRMSE (%) : ', deviations['RRMSE%'][i]        
                    
                    data_tmp = values
                    #print data_tmp
                    data_tmp = [h[i] for h in data_tmp]
            #        print data_tmp
                    data_tmp = np.asarray(data_tmp)
                    len_recon=len(data_tmp)     
                    if (len_recon%frame_size>0): #this step is to make sure that we can divide the length of the signal by the number of segments we set
                        for j in range (frame_size-(len_recon%frame_size)):
                            data_tmp=np.concatenate((data_tmp, [data_tmp[len_recon-1]]))        
                    
#                    red_dct, rmse_dct=encoder.dct_compare_single_signal(data_tmp)        
#                    print 'Comparison with DCT'
#                    print '    Reduction ratio : ', red_dct
#                    print '    RRMSE (%) : ', rmse_dct
#                    
#                    red_saw, rmse_saw=encoder.sawtooth_compare_single_signal(data_tmp)        
#                    print 'Comparison with sawtooth'
#                    print '    Reduction ratio : ', red_saw
#                    print '    RRMSE (%) : ', rmse_saw
#                            
                print    
                P.plot(sensor_id, process_id=process_id, dt_format='%H:%M:%S', deviation='difference', output='png', suptitle='Sensorid_'+str(sensor_id)+'_reduction_'+str(reduction_ratio)+'%_framesize_'+str(frame_size), path='../figures/Claas/')
          
    	       #Writes the dictionary in a csv file
        if (write):
            #file_dict='../Dictionaries/dictionary_sensorid'+str(sensor_id)+'_bmw%s_omp_%s_%s.csv' % (value_names, n_components, frame_size)
            with open('../Datas/Claas/Curve_of_redratio/varianceperseg_varscale.csv', "wb") as f:
                csvfile = csv.writer(f, delimiter=';')
                for j in range(len(reduction_list)):
                    csvfile.writerow((reduction_list[j], sigma_list[j]))
            f.close()    
        pl.figure()
        from Differences_plots import plot_tuples    
        plot_tuples(reduction_list, sigma_list)
        pl.title('Variance per segment according to reduction ratio')
        pl.show()
        t05=time.time()-t00
        print ('Done in %.2fs.' % t05)
             
            #    f_txt.close()