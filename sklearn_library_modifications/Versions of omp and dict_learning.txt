OMP:
05-07 : taking tol_1 and tol in consideration
20-07 : tol does not override n_nonzero anymore, both are now compatible

dict_learning : 
14-07 : taking tol_1 and tol in consideration
19-07 : update of the current cost. The current cost is now correct for the omp method too (cost for stopping criteria)
20-07 : non zero coeffs modification. It is now possible to impose a maximal number of non zero per segment during the training phase (omp method)