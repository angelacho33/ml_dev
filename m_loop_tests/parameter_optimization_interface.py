import numpy as np
import scipy.io 
import os
import time
import collections
import sys
timeDelay = 1
InputFileName='ExpInput.txt'
#default_exception=1e15

class mloop_interface():
    """
    Interface class to read parameters proposed by M-LOOP in ExpInput.txt 
    and write results to ExpOutput.mat
    """
    def __init__(self,boundaries_for_parameters_to_optimize=[],number_of_training_runs=1,number_of_optimization_runs=1,leash=None):
        """Initiate mloop interface
        Args: 
        
        -boundaries_for_parameters_to_optimize=[('parameter_name',[min_boundary,max_boundary])]
        -number_of_training_runs
        -number_of_optimization_runs
        -leash
        
        Returns:
        None
        """
        self.parameter_values = []
        self.number_of_processed_runs=0
        ##Default values for parameters
        #self.framesize=100
        #self.core_reduction=0.7
        #self.min_delta=0.0
        #self.delta=default_exception
        #self.log10multiple=0
        #self.multiple=np.power(10,self.log10multiple)
        #self.block=15
        ##############################
        
        dict=collections.OrderedDict(boundaries_for_parameters_to_optimize)
        self.boundaries_for_parameters_to_optimize = dict
        self.parameter_values_dict=collections.OrderedDict()
        
        self.min_max_bounds={}
        min_bounds_for_mloop=[]
        max_bounds_for_mloop=[]
        for key in dict.keys():
            if key=='cost':
                continue
            self.min_max_bounds[key]=[dict[key][0],dict[key][1]]
            min_bounds_for_mloop.append(dict[key][0])
            max_bounds_for_mloop.append(dict[key][1])

        try:
            min_cost=dict['cost'][0]
            max_cost=dict['cost'][1]
            self.cost_boundaries=[min_cost,max_cost]
        except:
            print "\n"
            print '***************** WARNING **********************'
            print 'Min. Max Boundaries for the cost function were not specified. Setting them to [0,1]...\nIf you want other boundary values add an entry of the form (\'cost\',[min_cost,max_cost]) at the initialization dictionary'
            print '***********************************************\n'
            min_cost=0.0
            max_cost=1.0
            self.cost_boundaries=[min_cost,max_cost]
            #sys.exit(1)
            
        self.number_of_training_runs=number_of_training_runs
        self.number_of_optimization_runs=number_of_optimization_runs
        
        ##Write Configuration File M-LOOP needs...
        text_file=open("ExpConfig.txt", "w")
        text_file.write("ControllerType='GlobalLearner'\n")
        if 'cost' in dict.keys():
            text_file.write("NumberOfParameters=%d\n"%(len(dict)-1)) ##-1 because the cost function in the dict is not a parameter to be optimized
        else:
            text_file.write("NumberOfParameters=%d\n"%(len(dict)))
        text_file.write("MinimumBoundaries="+str(min_bounds_for_mloop)+"\n")
        text_file.write("MaximumBoundaries="+str(max_bounds_for_mloop)+"\n")
        #text_file.write("MinimumCost = 0.0\n")
        #text_file.write("MaximumCost = 1.0\n")
        text_file.write("MinimumCost = %f\n"%min_cost)
        text_file.write("MaximumCost = %f\n"%max_cost)
        if number_of_training_runs==0:
            text_file.write("InitialTrainingSource='FromFile'\n") ##This assumes the pkl file already exists from a previous training
        else:
            text_file.write("InitialTrainingSource='Random'\n")
            text_file.write("NumberOfTrainingRuns=%d\n"%number_of_training_runs)
        text_file.write("NumberOfRuns = %d\n"%number_of_optimization_runs)
        if leash!=None:
            ##The default leash inside M-LOOP is 1.0
            text_file.write("LeashSize=%f\n"%leash)
            
        text_file.close()
        ####
        
    def read_parameters_from_mloop_file(self,InputFileName):
        ##The order or the parameters is the same as specified in the
        ##Boundaries array in the ExpConfig.txt file.
        with open(InputFileName,'r') as f:
            for line in f:
                text = line.rstrip() ##Removes white spaces to the right
                items = text.split(" ") ##Separates by space
        return items ##Returns the last line of the file as an array
                
    def get_parameter_values(self):
        """
        Returns an array with the parameters proposed by mloop in the ExpInput.txt file
        The order of the parameters is the same as specified in the "boundaries_for_parameters_to_optimize" input tuple
        """
        if self.number_of_processed_runs==0:
            print("Waiting for m-loop parameters for next run...")
            print("run (in python3): python $YOUR_PATH_TO_MLOOP/run_Controller.py")
        else:
            print("Waiting for m-loop parameters for next run...")
        
        expInFileName = InputFileName
        readFileFlag = False
        while not readFileFlag:
            while not os.path.isfile(expInFileName):
                #Give a time delay in seconds before we check the for output file again
                time.sleep(timeDelay)
                
            #Give some time to make sure the file is written to disk
            time.sleep(timeDelay)
        
            try:
                self.parameter_values=self.read_parameters_from_mloop_file(expInFileName)
                os.remove(expInFileName)
                readFileFlag = True
            except: 
                print("FileNotFoundError thrown when trying to read ExpInput.txt Trying again...")
        ####
        ##Put the suggested paramters in an ordered dict according to the order set in the boudaries dict
        for index,key in enumerate(self.boundaries_for_parameters_to_optimize.keys()):
            if key=='cost':
                continue
            #print index,key
            if key in ["framesize","block"]:
                self.parameter_values_dict[key]=int(float(self.parameter_values[index]))
            else:
                self.parameter_values_dict[key]=float(self.parameter_values[index])
        print self.parameter_values_dict
        ####
        #return parameter_values
        return self.parameter_values_dict
            
    def write_output_file(self, cost, uncer, bad_run_status):
        """
        Write the results of the processing in a .mat file that will be read by M-LOOP
        """
        self.number_of_processed_runs=self.number_of_processed_runs+1
        cost=cost 
        uncer=uncer
        bad_run_status=bad_run_status
        dict={'cost':cost,
              'uncer':uncer, 
              'bad': bad_run_status}
        print(dict)
        scipy.io.savemat('ExpOutput.mat',dict)
        
        






