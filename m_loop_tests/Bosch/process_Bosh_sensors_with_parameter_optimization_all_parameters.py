import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import matplotlib.pyplot as plt 

import os, sys, time
from tk_databank import DataBank
from tk_plot import Plot
import numpy as np
import glob
import csv
import gc
import numpy as np
from bosch_overview import * ##File to read bosch data....
import matplotlib.pyplot as plt

#########################################################################
##M-LOOP interface
import parameter_optimization_interface as poi
boundaries_for_parameters_to_optimize=[('framesize',[100,900]),
                                       ('log10multiple',[0,6]),
                                       ('min_delta',[1,20]),
                                       ('delta',[1,100]),
                                       ('block',[1,30]),
                                       ('core_reduction',[0.1,0.95])
]                                       
number_of_training_runs=10
number_of_optimization_runs=200
##The process of a sensor below (process_cases) will be called number_of_training_runs+number_of_optimization_runs times
##Produce the ExpConfig.txt required by M-LOOP
mloop_interface=poi.mloop_interface(boundaries_for_parameters_to_optimize,
                                    number_of_training_runs,
                                    number_of_optimization_runs,
                                    leash=0.1)
#########################################################################
total_number_of_runs=number_of_training_runs+number_of_optimization_runs
process_cases=range(0,total_number_of_runs+1) 

file_id=0
folder='/home/angel/Bosch/'
filenamelist=sorted(glob.glob(folder+'*.CSV'),key=os.path.getsize,reverse=True)

filenamelist=[filenamelist[file_id]] 
print filenamelist

##Sensor names copied from the header of the Bosh file...
all_sensors = ["Longitude","Latitude","IMEI","ASMod_dvolPFltEG",
               "Exh_pAdapPPFltDiff","ASMod_tPFltSurfSim","PFltLd_stSimOn",
               "GlbDa_lTotDst","PFltLd_resFlwFlt","PFltLd_mSotMeasBas_mp",
               "PFltLd_mSotCor","PFltLd_mSotMeasRaw","PFltLd_mSotMeas",
               "PFltLd_mSotMlg","VehV_v","PFltPOp_stVehPOp",
               "Exh_tAdapTPFltUs","PFltLd_resFlw","Exh_pDiffOfsValAct",
               "Exh_pDiffOfsValOld","PFltLd_mSotSimNoCont",
               "PFltRgn_lSnceRgn","Epm_nEng","InjCtl_qSetUnBal",
               "DFES_numDFC_[0]","DFES_numDFC_[1]","DFES_numDFC_[2]",
               "DFES_stEntry_[0]","DFES_stEntry_[1]","DFES_stEntry_[2]",
               "CEngDsT_t","PthSet_trqInrSet","APP_r",
               "Tra_numGear","AirCtl_mDesVal","AFS_mAirPerCylFlt",
               "AirCtl_stMon","EGRVlv_r","EGRVlv_rAct",
               "ThrVlv_rAct","TrbCh_rPs","EnvT_t",
               "LSU_rLam_[0]","PFltLd_dmO2Aprx","PFltLd_dvolPFltEGRstnRed_mp",
               "PFltLd_mSotSim","FMO_qEmiCtlCor","EnvP_p",
               "Exh_tAdapTOxiCatUs","CoEng_st","CoEOM_stOpModeAc"]

default_exception=1e15 ##If I do not want to use exception detection as suggested by Qing I just set a very large number....

###Default parameters
                                            ##min delta is the factor multiplying the amplitude for each sensor
                                           #:[framesize,core_reduction, multiple, min_delta, delta] 
all_sensors_parameters = {"Longitude":[500,0.85,1,0,10],
                          "Latitude":[500,0.85,1,0,10],
                          "IMEI":[500,0.85,1,0,  default_exception],
                          "ASMod_dvolPFltEG":[500,0.7,1,5,20], #######This is the sensor I am using for the first tests of M-LOOP
                          "Exh_pAdapPPFltDiff":[500,0.75,1,1,2.5],
                          "ASMod_tPFltSurfSim":[500,0.85,1,0,25],
                          "PFltLd_stSimOn":[500,0.95,1,0,0.5],
                          "GlbDa_lTotDst":[500,0.9,1,0,  default_exception], 
                          "PFltLd_resFlwFlt":[500,0.7,1,0,0.005],
                          "PFltLd_mSotMeasBas_mp":[500,0.8,1,0,  default_exception], 
                          "PFltLd_mSotCor":[500,0.9,1,0,  default_exception], 
                          "PFltLd_mSotMeasRaw":[500,0.9,1,0,  default_exception], 
                          "PFltLd_mSotMeas":[500,0.7,1,0,  0.005], 
                          "PFltLd_mSotMlg":[500,0.6,1,0,  0.005], 
                          "VehV_v":[1000,0.75,1,0,25],
                          "PFltPOp_stVehPOp":[500,0.7,1,0,25],
                          "Exh_tAdapTPFltUs":[500,0.8,1,0,20],
                          "PFltLd_resFlw":[500,0.99,1,0,0.005],
                          "Exh_pDiffOfsValAct":[500,0.7,1,0,0.2],
                          "Exh_pDiffOfsValOld":[500,0.7,1,0,0.2],
                          "PFltLd_mSotSimNoCont":[500,0.6,1,0,  0.005], 
                          "PFltRgn_lSnceRgn":[500,0.85,1,0,  default_exception], 
                          "Epm_nEng":[500,0.85,1,0,100],
                          "InjCtl_qSetUnBal":[250,0.8,1,0,2],
                          "DFES_numDFC_[0]":[500,0.9,1,0,  default_exception], 
                          "DFES_numDFC_[1]":[500,0.9,1,0,  default_exception], 
                          "DFES_numDFC_[2]":[500,0.9,1,0,  default_exception], 
                          "DFES_stEntry_[0]":[500,0.9,1,0,  default_exception], 
                          "DFES_stEntry_[1]":[500,0.9,1,0,  default_exception], 
                          "DFES_stEntry_[2]":[500,0.9,1,0,  default_exception], 
                          "CEngDsT_t":[500,0.85,1,0,10],
                          "PthSet_trqInrSet":[500,0.80,1,1,20],
                          "APP_r":[500,0.6,1,1,10],
                          "Tra_numGear":[500,0.8,1,0,0.5],
                          "AirCtl_mDesVal":[500,0.8,1,0,100],
                          "AFS_mAirPerCylFlt":[500,0.95,1,0,25],
                          "AirCtl_stMon":[500,0.85,1,0,0.05],
                          "EGRVlv_r":[500,0.6,1,0,25],
                          "EGRVlv_rAct":[500,0.4,1,0,25],
                          "ThrVlv_rAct":[500,0.4,1,0,25],
                          "TrbCh_rPs":[500,0.9,1,0,  default_exception], 
                          "EnvT_t":[500,0.99,1,0,0.05],
                          "LSU_rLam_[0]":[500,0.85,1,0.1,1],
                          "PFltLd_dmO2Aprx":[500,0.7,1,0,5],
                          "PFltLd_dvolPFltEGRstnRed_mp":[500,0.7,1,1,25],
                          "PFltLd_mSotSim":[500,0.7,1,0,  0.005], 
                          "FMO_qEmiCtlCor":[500,0.9,1,0,0.25],
                          "EnvP_p":[500,0.1,1,0,0],
                          "Exh_tAdapTOxiCatUs":[1000,0.85,1,0,25],
                          "CoEng_st":[500,0.7,1,0,0.5],
                          "CoEOM_stOpModeAct":[500,0.9,1,0,  default_exception]
                      }

print 'len(all_sensors)', len(all_sensors)
print 'len(all_sensors_parameters)', len(all_sensors_parameters)


######### Pre and post process function
def set_pre_process(multiple,min_delta,delta):
    pre_process = {
        "multiple": [multiple],
        #'integer': [1],
        "min_delta":[min_delta*multiple],
        "value_exceptions": [
            [
                {
                    "e_name": "red_peak_points",
                    "e_id":  4,
                    "v_types": ["red"],
                    "condition": "(abs(delta_v)>"+str(delta*multiple)+")",
                    "para": "delta_v"
                },
                {
                    "e_name": "rest_peak_points",
                    "e_id":  5,
                    "v_types": ["rest"],
                    "condition": "(abs(delta_v)>"+str(delta*multiple)+")",
                    "para": "str(delta_v)+'_'+str(ts)+'_'+str(v)"
                }
            ]
        ]
    }
    post_process = {
        "divide": [float(multiple)]
    }
    return pre_process, post_process

###############################################################    
output_file='summary_file_id_'+str(file_id)+'_output_recon.csv'

with open(output_file, 'w',0) as f_recon:
    #Output file
    writer_recon = csv.writer(f_recon,quoting=csv.QUOTE_NONNUMERIC)
    writer_recon.writerow(('file_id','npoints','sensor_id', 'sensor_name','value_name','process_id' ,'framesize','core_reduction',
                           'multiple','min_delta','delta', 'block',
                           'tss_recon_min','tss_recon_max',
                           'raw_min_value','values_recon_min',
                           'raw_max_value','values_recon_max', 
                           'values_recon.mean', 
                           'zlib_ratio', 'count_red','eff. reduction',
                           'dev_max','dev_mean','dev_min','amplitude',
                           'relerr','mape','smape','wpe','wape','rrmse','relative_to_the_max_error','mae',
                           'file_name') )
    
    
    
    for file in filenamelist:    
        D = DataBank()
        ##Next "for" loop was taken from the bosch overview Cao sent around with some modifications I import these functions above...
        for i in range(4,get_csv_list_len(file)):
            #if i!=5:
            #    continue
            sensor_id=i-4
            ###########################################################################################################
            ###########################################################################################################
            if sensor_id!=3: ##The tests I am doing are for sensor_id=3: ASMod_dvolPFltEG so only add this sensor to DataBank
                continue
            ###########################################################################################################
            ###########################################################################################################
            sensor_data = get_single_sensor_data(file,i)
            sensor_name = get_sensor_name(file,i)
            value_names= [sensor_name]
            print 'sensor_id', sensor_id , ' , sensor_name:',sensor_name
            
            #D.add_sensor_data(sensor_data[sensor_name]['values'],sensor_data[sensor_name]['tss'], i-4, output_name, sensor_name)
            timestamps= sensor_data[sensor_name]['tss']
            #print timestamps
            timestamps_in_miliseconds=[ (time.mktime(dt.timetuple()) + (dt.microsecond / 1000000.0))*1000 for dt in timestamps]
            if sensor_name=='IMEI': ##This sensor has some code entries with letters...so not a number...
                continue
                
            number_of_points=1000 ##consider only the first 1000 data points of the sensor
            D.add_sensor_data(sensor_data[sensor_name]['values'][:number_of_points],timestamps_in_miliseconds[:number_of_points], sensor_id, sensor_name, sensor_name,value_names=value_names)
            
            
        print '************************************************************************************'
        print '*********** There are ', len(D.get_sensor_list()), ' sensors in DataBank ***********'
        print '************************************************************************************'
       
        ##Print the sensor_summary i.e #sensors cumulative functions and sampling rate...x
        P=Plot(D)
        P.print_sensor_summary()
        
        
        ##################### Loop to process each sensor added to the DataBank...
        for sensor in D.get_sensor_list():
            index=sensor['index']
            dof=sensor['dof']
            sensor_id=sensor['sensor_id']
            sensor_name=sensor['sensor_name']
            
            #Raw data info
            raw_data=D.get_sensor_raw_data(sensor_id)
            tss=np.asarray(raw_data['tss'])
            values=np.asarray(raw_data['values'])
            npoints=raw_data['count']
            
            ##The next works if there dof=1
            max=raw_data['max_v'][0]
            min=raw_data['min_v'][0]
            
            for item in process_cases:  
                ##Get the parameters proposed by M-LOOP
                ##The lenght of the the process_parameter array returned next depends on the lenght of the boundaries_for_parameters_to_optimize
                ##specified above
                parameter_values_dict=mloop_interface.get_parameter_values()
                
                framesize=parameter_values_dict['framesize']
                ############ If more parameters are added in the parameters to optimization uncomment the next lines
                core_reduction=parameter_values_dict['core_reduction']
                #core_reduction=0.7
                multiple=np.power(10,parameter_values_dict['log10multiple'])
                min_delta=parameter_values_dict['min_delta']
                delta=parameter_values_dict['delta']
                block=parameter_values_dict['block']
                
                ##If not included in the optimization list The rest of the parameters are set from the all_sensors_paramaters dictionary above...
                #framesize=all_sensors_parameters[sensor_name][0]
                #core_reduction=all_sensors_parameters[sensor_name][1]
                #multiple=all_sensors_parameters[sensor_name][2]
                #min_delta=all_sensors_parameters[sensor_name][3]
                #delta=all_sensors_parameters[sensor_name][4]
                #block=15
                
                pre_process,post_process=set_pre_process(multiple=multiple,min_delta=min_delta,delta=delta)
                
                print '**********************************************************************'
                print 'Processing sensor, ',sensor_id, ' ,', sensor_name
                print 'framesize', framesize
                print 'core_reduction', core_reduction
                #print 'npoints', npoints
                print 'block:' , block
                print 'pre_process:', pre_process
                print 'post_process:', post_process
                
                
                
                #####Process Data
                D.sensor_data_process(sensor_id, framesize, core_reduction,'cii_dct_v011', pre_process=pre_process, post_process=post_process,block=block, raw_tss=True)
                process_id=len(D.get_sensor_info(sensor_id)['processed_data'])-1
                print 'Processed sensor, ',sensor_id, ' ,', sensor_name, ', framesize: ', framesize, ' ,core_reduction: ', core_reduction
                print '***********************************'
                print 'process_id=', process_id
                print '***********************************'
                                
                #Recon data info
                recon_data=D.get_sensor_recon_data(sensor_id,process_id)
                tss_recon=np.asarray(recon_data['tss'])
                values_recon=np.asarray(recon_data['values'])
                zlib_ratio=D.get_zip_reduction_ratio(sensor_id,framesize)
                
                #Reduce info
                reduce_info=D.get_sensor_process_info(sensor_id, process_id)['reduce_info']
                count_red=reduce_info['count_red']
                red_info_ratio=reduce_info['ratio']
                
                #Deviation_info
                deviations=D.get_deviations(sensor_id,process_id)
            
                for i in range(dof):
                    value_name=sensor['value_names'][i]
                    values_comp=values_recon.T[i]
                    dev_max=deviations['E'][i]['max']
                    dev_mean=deviations['E'][i]['mean']
                    dev_min=deviations['E'][i]['min']
                
                    relerr=deviations['relerr'][i]
                    mae=deviations['MAE'][i]
                    mape=deviations['MAPE%'][i]
                    smape=deviations['SMAPE%'][i]
                    rrmse=deviations['RRMSE%'][i]
                    wape=deviations['WAPE%'][i]
                    wpe=deviations['WPE%'][i]
                    
                    ########################################################################
                    ##Write results of processing to the mat file that will be read by M-LOOP
                    eff_reduction=red_info_ratio*100
                    weight_rrmse=1
                    weight_eff_red=0.1
                    cost=weight_rrmse*rrmse + weight_eff_red*(100-eff_reduction)
                    cost=cost/(weight_rrmse+weight_eff_red) ##Normalize to the sum of weights
                    
                    uncer=0.5 ##Estimates the uncertainty in the cost function. 0.5 comes from calculating the standard deviation
                              ## of the rrmses obtained by running 10 times for a given framesize using engineA
                    bad_run_status=False
                    mloop_interface.write_output_file(cost=cost,uncer=uncer,bad_run_status=bad_run_status)
                    #######################################################################################
                    print '******************************************'
                    print '******************************************'
                    print "effective_reduction: ", eff_reduction
                    print "rrmse: ", rrmse
                    print "cost: ", cost
                    print '******************************************'
                    print '******************************************'
                    ####### Plots##################
                    P=Plot(D)
                    
                    suptitle='sid_'+str(sensor_id)+'_'+sensor_name+'_pid'+str(process_id)+'_f'+str(framesize)+'_corered'+str(round(core_reduction*100,2))+'_red'+str(round(red_info_ratio*100,2))+'_rrmse'+str(round(rrmse,2))+'_mindelta'+str(round(min_delta,2))+'_delta'+str(round(delta,2))

                    P.plot(sensor_id,process_id,deviation='difference',dt_format='%H:%M:%S',output='png',suptitle=suptitle) 
                    
                    #P.plot_rec_inspection(sensor_id, process_id, output='png', fft_yscale='log', dt_format='%M:%S')
                    ##Close plt figures to avoid a massive memory leak...
                    plt.close('all')
                    gc.collect()
                    #################################

                    ##A relative error measurement they want to see
                    amplitude=np.abs(max-min)
                    if np.abs(dev_min)>np.abs(dev_max):
                        relative_error=np.abs(dev_min/amplitude)*100 
                    else:
                        relative_error=np.abs(dev_max/amplitude)*100 
                    
                    #Write output
                    writer_recon.writerow((file_id,npoints,sensor_id,sensor_name, value_name,process_id ,framesize,core_reduction, 
                                           multiple, min_delta,delta,block,
                                           tss_recon.min(),tss_recon.max(),min,float("%.3f"%values_comp.min()),max, float("%.3f"%values_comp.max()), 
                                           float("%.3f"%values_comp.mean()), float("%.3f"%zlib_ratio), count_red,float("%.3f"%red_info_ratio), 
                                           dev_max,dev_mean,dev_min,amplitude,
                                           relerr,mape,smape,wpe,wape,rrmse,relative_error,mae,
                                           file))
                    
                    #process_id=process_id+1
        print '** Goes in file: ',file_id, ' ', file
        name='output_file_id_'+str(file_id)+'.dbk'
        D.save(name)
        file_id=file_id+1
            
