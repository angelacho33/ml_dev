#Note: This sript only works with python3 since it reads a pickle file
#produced with python3...
#The names of the variables in this example are framesize and block
#which are set as labels..would have to change accordingly...

import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import pickle ##In python2 pickle is already the faster cPickle
#import cPickle as pickle 

import matplotlib.pyplot as plt
import numpy as np


results_mloop = pickle.load( open( "ControllerArchive.pkl", "rb" ) )

realBest=results_mloop.realBest
realBestCostFunc=results_mloop.realCostFunc(results_mloop.bestCost)
print('Real Best Params:', realBest) ##realBest are the parameters that gave the best Cost
#print('Best Cost:', results_mloop.bestCost)   ##BestCost is the normalized cost
print('Real Best Cost:',realBestCostFunc)  ##Using this function I get the real cost

bestMinima=results_mloop.bestMinima
bestMinCost=results_mloop.bestMinCost

print('bestMimima:', bestMinima) ##realBest are the parameters that gave the best Cost
bestParams=results_mloop.bestParams

real_parameters=[results_mloop.realParamsFunc(x) for x in results_mloop.allParams]
real_cost_function=[results_mloop.realCostFunc(x) for x in results_mloop.allCosts]

#print(real_cost_function)
framesizes_used=[]
blocks_used=[]
for entry in real_parameters:
    framesize=entry[0]
    framesizes_used.append(framesize)
    
    block=entry[1]
    blocks_used.append(block)

fig2,ax2=plt.subplots(2)
plt.subplots_adjust(left=0.125, right=0.9, bottom=0.1, top=0.85, wspace=0.8, hspace=0.8)

########## framesize plot
realBest_framesize=realBest[0]
ax2[0].plot(framesizes_used,real_cost_function,'bo',label='Data points')
ax2[0].plot(realBest_framesize,results_mloop.realCostFunc(results_mloop.bestCost) ,'rv',markersize=10, 
            label='realBest (framesize):' +str(round(realBest_framesize,3))+' , bestCost:'+str(round(realBestCostFunc,3)))
ax2[0].axhline(y=realBestCostFunc,color='red',lw=1,ls="dashed")
ax2[0].set_xlabel('realParamsFunc (Framesize)')
ax2[0].set_ylabel('realCostFunc (rrmse)')
#ax2[0].legend(loc="best",numpoints=1,fontsize=12)
ax2[0].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
              ncol=2, mode="expand", borderaxespad=0.,numpoints=1,fontsize=12)
ax2[0].set_title("Framesize",y=1.2)

########## block plot
realBest_block=realBest[1]
ax2[1].plot(blocks_used,real_cost_function,'bo',label='Data points')
ax2[1].plot(realBest_block,results_mloop.realCostFunc(results_mloop.bestCost) ,'rv',markersize=10, 
            label='realBest (block):' +str(round(realBest_block,3))+' , bestCost:'+str(round(realBestCostFunc,3)))
ax2[1].axhline(y=realBestCostFunc,color='red',lw=1,ls="dashed")
ax2[1].set_xlabel('realParamsFunc (block)')
ax2[1].set_ylabel('realCostFunc (rrmse)')
#ax2[1].legend(loc="best",numpoints=1,fontsize=12)
ax2[1].legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
              ncol=2, mode="expand", borderaxespad=0.,numpoints=1,fontsize=12)
ax2[1].set_title("block",y=1.2)
plt.show()
#fig.show() Will open a window but closes after the script ran.
fig2.savefig('results_mloop_realParams_vs_realCostFunction_with_Minimum.png')



###############
#scatter plot
fig3,ax3=plt.subplots()
#cm = plt.cm.get_cmap('OrRd') #Another colormap 
#cm = plt.cm.get_cmap('autumn') #Another colormap 
cm = plt.cm.get_cmap('jet') #Another colormap 
#sc=ax3.scatter(blocks_used,framesizes_used,c=real_cost_function,s=100,cmap=cm)
#sc=ax3.scatter(blocks_used,framesizes_used,c=np.log10(real_cost_function),s=100)
sc=ax3.scatter(blocks_used,framesizes_used,c=real_cost_function,s=100,cmap=cm)
ax3.plot(realBest_block,realBest_framesize, marker='+',color='red',mew=5,markersize=20,label='RealBest:(%.2f,%.2f)'%(realBest_block,realBest_framesize))
ax3.legend(loc='best',numpoints=1)
ax3.set_xlabel('block')
ax3.set_ylabel('framesize')
colorbar=plt.colorbar(sc)
#colorbar.set_label('log10(rrmse (%))')
colorbar.set_label('rrmse (%)')
plt.show()
fig3.savefig('Scatter_plot.png')

#contour plot
fig4,ax4=plt.subplots()
#cm = plt.cm.get_cmap('PuBu') #Another colormap 
#sc=ax3.scatter(blocks_used,framesizes_used,c=real_cost_function,s=100,cmap=cm)
#sc=ax3.scatter(blocks_used,framesizes_used,c=np.log10(real_cost_function),s=100)
#sc=ax4.scatter(blocks_used,framesizes_used,c=real_cost_function,s=100)
#h =ax4.contour(blocks_used,framesizes_used, real_cost_function)

xi = np.linspace(1    ,50  ,100)
yi = np.linspace(50  ,990  ,100)
from matplotlib.mlab import griddata
zi = griddata(blocks_used,framesizes_used , real_cost_function, xi, yi, interp='linear')

#from scipy.interpolate import griddata
#zi = griddata([blocks_used,framesizes_used] , real_cost_function, (xi, yi), method='linear')

#zi = griddata(blocks_used,framesizes_used , real_cost_function, xi, yi, interp='nn')
CS = ax4.contour(xi, yi, zi, 20, linewidths=0.5, colors='k')
#CS = ax4.contourf(xi, yi, zi, 30, cmap=plt.cm.rainbow,vmax=abs(zi).max(), vmin=-abs(zi).max())
#CS = ax4.contourf(xi, yi, zi, 30,cmap=plt.cm.Blues,vmax=abs(zi).max(), vmin=-abs(zi).max())
CS = ax4.contourf(xi, yi, zi, 20,cmap=cm)

ax4.plot(realBest_block,realBest_framesize, marker='+',color='red',mew=5,markersize=20,label='RealBest:(%.2f,%.2f)'%(realBest_block,realBest_framesize))
ax4.legend(loc='best',numpoints=1)
ax4.set_xlabel('block')
ax4.set_ylabel('framesize')
colorbar=plt.colorbar(CS)
#colorbar.set_label('log10(rrmse (%))')
colorbar.set_label('rrmse (%)')
plt.show()
fig4.savefig('Contour_plot_interpolated.png')

########################################
#2d Model Plot
fig5,ax5=plt.subplots()
xi = np.linspace(0    ,1  ,100)
yi = np.linspace(0    ,1  ,100)
X,Y = np.meshgrid(xi,yi)
#XY=np.array([X.flatten(),Y.flatten()]).T
xx = np.vstack([X.reshape(X.size), Y.reshape(Y.size)]).T
gp=results_mloop.gaussProcessParticles[0] 
#z_pred= gp.predict(XY)
z_pred= gp.predict(xx)
z_pred = z_pred.reshape((100, 100))

CS = ax5.contour(Y, X, z_pred, [1.25,1.5,2.0], linewidths=0.5, colors='k',ls='dashed')
plt.clabel(CS, fontsize=11)

CS = ax5.contourf(Y, X, z_pred, 40,cmap=cm)

ax5.plot(bestMinima[1],bestMinima[0], marker='*',color='green',mew=1,markersize=20,label='BestMinima=(%.2f,%.2f)'%(bestMinima[1],bestMinima[0]))
#ax5.plot(bestParams[1],bestParams[0], marker='*',color='white',mew=1,markersize=20,label='BestParams')
colorbar=plt.colorbar(CS)
colorbar.set_label('Normalized Cost')
#ax5.legend(loc='best',numpoints=1)
ax5.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
              ncol=2, mode="expand", borderaxespad=0.,numpoints=1,fontsize=12)

plt.show()

fig5.savefig('Contour_plot_Model.png')
