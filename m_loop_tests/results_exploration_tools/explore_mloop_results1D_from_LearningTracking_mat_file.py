import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import scipy.io
import matplotlib.pyplot as plt
import numpy as np

params_mloop=scipy.io.loadmat("LearnerTracking.mat")
#print params_mloop

x=params_mloop['allParams'].T[0]
y=params_mloop['allCosts'][0]

xvec=params_mloop['crossXvec'][0]
yvec=params_mloop['crossCostVecs'][0]

plt.plot(x, y,marker='o',ls='',label='allParams')

plt.plot(xvec,yvec,label='crossCostVecs',lw=2)

bestParams=params_mloop['bestParams'][0]
bestCost=params_mloop['bestCost'][0]

bestMinima=params_mloop['bestMinima'][0]
bestMinCost=params_mloop['bestMinCost'][0]


plt.plot(bestParams,bestCost,marker='*',markersize=20,color="blue",label='bestParams:'+str(bestParams))
plt.axhline(y=bestCost,color='blue',lw=1,ls="dashed")

plt.plot(bestMinima,bestMinCost,marker='*',markersize=20,color="green",label='bestMin:'+str(bestMinima))
plt.axhline(y=bestMinCost,color='green',lw=1,ls="dashed")

plt.xlabel("normalized (Framesize)")
plt.ylabel("normalized (core reduction)")
sigma=params_mloop['crossUncerVecs'][0]

plt.ylim(1.2,1.7)

##extracted from:http://scikit-learn.org/stable/auto_examples/gaussian_process/plot_gp_regression.html
plt.fill(np.concatenate([xvec, xvec[::-1]]),
        np.concatenate([yvec - 1.9600 * sigma,
                        (yvec + 1.9600 * sigma)[::-1]]),
        alpha=.5, fc='g', ec='None', label='95% confidence interval')



plt.legend(loc="best",numpoints=1)
plt.savefig('parameters_from_Learner_Tracking.png')
plt.show()







