##Note: This script only runs with python3 since the pkl file is written by m-loop in python3
import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import pickle ##In python3 pickle is already the faster cPickle
#import cPickle as pickle 

import matplotlib.pyplot as plt
import numpy as np

results_mloop = pickle.load( open( "ControllerArchive.pkl", "rb" ) )

realBest=results_mloop.realBest[0]
realBestCost=results_mloop.realCostFunc(results_mloop.bestCost)

realParams=[results_mloop.realParamsFunc(x) for x in results_mloop.allParams]
realCosts=[results_mloop.realCostFunc(x) for x in results_mloop.allCosts]

bestRealMinima=results_mloop.bestRealMinima[0]
bestRealMinCost=results_mloop.realCostFunc(results_mloop.bestMinCost)

##Next function is the inverse of the scaling for the uncertainties used mloop internally
minCost=results_mloop.minCost
maxCost=results_mloop.maxCost
diffCost=maxCost-minCost
realUncerFunc=lambda u: float(abs(u)*diffCost/9.0) 
realUncers=[realUncerFunc(x) for x in results_mloop.allUncers]
###


print('Real Best Params:',realBest) ##realBest are the parameters that gave the best Cost
print('Real Best Cost:'  ,realBestCost)  ##Using this function I get the real cost
print('bestRealMinima:' ,bestRealMinima) ##realBest are the parameters that gave the best Cost
print('bestRealMinCost:',bestRealMinCost)  ##Using this function I get the real cost

##########################################################
##Plot DataPoints plus Model

fig2,ax2=plt.subplots()
plt.subplots_adjust(top=0.85)

### Data Points 
#print(realParams) 
#ax2.plot(realParams,realCosts,'bo',label='Data points') 
ax2.errorbar(realParams,realCosts,realUncers,fmt='b.',markersize=10,label='Data points')

## realBest Point 
ax2.plot(realBest,realBestCost,'b*',markersize=15, 
         label='realBest:('+str(round(realBest,3))+','+str(round(realBestCost,3))+')')
ax2.axhline(y=realBestCost,color='blue',lw=1,ls="dashed")

############ Model 
x = np.atleast_2d(np.linspace(0, 1, 100)).T  ## mesh of points in the normalized parameter space
gp=results_mloop.gaussProcessParticles[0]    ## Particles is an array I take the first component...Is this correct?
y_pred, MSE = gp.predict(x, eval_MSE=True)   ## MSE=Mean Squared Error
sigma = np.sqrt(MSE)

real_x=[results_mloop.realParamsFunc(x_) for x_ in x]
real_cost=[results_mloop.realCostFunc(y) for y in y_pred]

real_sigma=np.array([realUncerFunc(s) for s in sigma])
ax2.plot(real_x,real_cost,label='Model',color='green')

## 95%Confidence band for the model: ##Taken from the scikit gaussian process example
ax2.fill(np.concatenate([real_x, real_x[::-1]]),
        np.concatenate([real_cost - 1.9600 * real_sigma,
                       (real_cost + 1.9600 * real_sigma)[::-1]]),
        alpha=.5, fc='green', ec='None', label='95% confidence interval')

#####################################

## bestRealMinima 
ax2.plot(bestRealMinima,bestRealMinCost,'g*',markersize=15, 
         label='bestRealMinima:(' +str(round(bestRealMinima,3))+','+str(round(bestRealMinCost,3))+')')
ax2.axhline(y=bestRealMinCost,color='green',lw=1,ls="dashed")

####Plot details
ax2.set_xlabel('realParamsFunc')
ax2.set_ylabel('realCostFunc')
#ax2.legend(loc="best",numpoints=1,fontsize=12)
ax2.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.,numpoints=1,fontsize=11)
plt.show()
#fig.show() Will open a window but closes after the script ran.
fig2.savefig('results_mloop_realParams_vs_realCostFunction_with_Minimum.png')
