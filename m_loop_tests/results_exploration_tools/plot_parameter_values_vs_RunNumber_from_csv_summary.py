#!/usr/bin/env python
import matplotlib
matplotlib.use('Agg') ### Force matplotlib to not use any Xwindows backend.

import matplotlib.pyplot as plt
import sys
import pandas as pd
import pickle
import numpy as np
import os

results_mloop = pickle.load( open( "ControllerArchive.pkl", "rb" ) )

#if len(sys.argv)<4:
#    sys.exit("Error!: Missing input parameters, Usage: pandas_extraction.py <File> <scenario> <number_files> <quantity>")

file='summary_file_id_0_output_recon.csv'
dfa=pd.read_csv(file,sep=',')
#dfa_no_bads=dfa[dfa['rrmse']<4.0]
#values=dfa_no_bads['framesize'].values

optimized_parameters=['framesize','multiple','min_delta','delta','block','core_reduction']
#optimized_parameters=['framesize','multiple','min_delta','delta(excp.)','block']
other_relevant_parameters=['eff. reduction', 'rrmse']

for index,parameter in enumerate(optimized_parameters):
#for index,parameter in enumerate(['framesize','multiple']):
    fig, ax=plt.subplots()
    plt.subplots_adjust(left=0.125, right=0.9, bottom=0.1, top=0.8, wspace=0.8, hspace=0.8)
    values=dfa[parameter].values
    ax.plot(values,marker='o',color='blue')
    ax.set_xlabel('Run Number')
    ax.set_ylabel(parameter)
    
    if parameter=='multiple':
        ax.set_yscale('log')
    MinBoundary=results_mloop.realParamsFunc(results_mloop.currMinBoundary)[index]
    if parameter=='multiple':
        MinBoundary=np.power(10,MinBoundary)
    ax.axhline(y=MinBoundary,color='black',lw=3,ls="dashed",label='MinimumBoundary')

    MaxBoundary=results_mloop.realParamsFunc(results_mloop.currMaxBoundary)[index]
    if parameter=='multiple':
        MaxBoundary=np.power(10,MaxBoundary)
    ax.axhline(y=MaxBoundary,color='black',lw=3,ls="dashed",label='MaximumBoundary')
    
    realBest=int(results_mloop.realBest[index])
    if parameter=='multiple':
        realBest=np.power(10,realBest)
    ax.axhline(y=realBest,color='blue',lw=3,ls="dashed",label='realBest:' +str(int(realBest)))
    bestRealMin=int(results_mloop.bestRealMinima[index])
    if parameter=='multiple':
        bestRealMin=np.power(10,bestRealMin)
    ax.axhline(y=bestRealMin,color='green',lw=3,ls="dashed",label='bestRealMin:' +str(int(bestRealMin)))

    ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
              ncol=2, mode="expand", borderaxespad=0.,numpoints=1,fontsize=15)
    #ax.legend(loc='best',title='Framesize')    
    
    
    title=parameter
    fig.suptitle(title,fontsize=15,y=0.98)
    
    print('Generated png for: '+parameter) 
    
    folder_name='./scanned_parameters_vs_RunNumber_plots/'
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    
    plt.savefig(folder_name+'scanned_parameters_'+parameter+'.png')
    plt.show()
    plt.close()

########################################################################################
########################################################################################
fig, ax=plt.subplots(2)
plt.subplots_adjust(left=0.125, right=0.9, bottom=0.1, top=0.95, wspace=0.4, hspace=0.4)
for index,parameter in enumerate(other_relevant_parameters):
#for index,parameter in enumerate(['framesize','multiple']):
    values=dfa[parameter].values
    ylabel_unit="%"
    if parameter=='eff. reduction':
        values=values*100
    ax[index].plot(values,marker='o',color='blue')
    ax[index].set_xlabel('Run Number')
    ax[index].set_ylabel(parameter+' '+ylabel_unit)
    ax[index].set_title(parameter)
    ax[index].yaxis.grid(True, which='major')
    
print('Generated png for: eff. reduction and rrmse') 
plt.savefig(folder_name+'scanned_parameters_eff_reduction_and_rrmse.png')
plt.show()
plt.close()
print("****")
print('The plots were stored in: '+folder_name) 
print("****")

    
    




